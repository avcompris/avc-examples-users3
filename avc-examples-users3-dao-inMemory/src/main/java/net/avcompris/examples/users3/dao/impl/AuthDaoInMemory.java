package net.avcompris.examples.users3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.impl.FilteringsFactory.match;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.avcompris.commons3.api.UserSessionFiltering;
import net.avcompris.commons3.dao.impl.AbstractDao;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UserSessionDto;
import net.avcompris.examples.users3.dao.UserSessionsDto;
import net.avcompris.examples.users3.dao.UserSessionsDtoQuery;
import net.avcompris.examples.users3.dao.UserSessionsDtoQuery.SortBy;

@Component
public final class AuthDaoInMemory extends AbstractDao implements AuthDao {

	private static final int SESSION_TIMEOUT_MINUTES = 60;

	private final Map<String, String> passwords = new ConcurrentHashMap<>();
	private final Map<String, MutableUserSessionDto> sessions = new ConcurrentHashMap<>();

	@Autowired
	public AuthDaoInMemory(final Clock clock) {

		super(clock);
	}

	@Override
	public void setUserPassword(final String username, final String password) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(password, "password");

		passwords.put(username, password);
	}

	@Override
	public void removeUserPassword(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		passwords.remove(username);
	}

	@Override
	@Nullable
	public String getUsernameByAuthorization(final String authorization, final DateTime updatedAt)
			throws SQLException, IOException {

		checkNotNull(authorization, "authorization");
		checkNotNull(updatedAt, "updatedAt");

		// throw new NotImplementedException("");

		return null;
	}

	@Override
	@Nullable
	public String getUsernameBySessionId(final String userSessionId, final DateTime updatedAt)
			throws SQLException, IOException {

		checkNotNull(userSessionId, "userSessionId");
		checkNotNull(updatedAt, "updatedAt");

		final MutableUserSessionDto session = sessions.get(userSessionId);

		if (session == null || session.getExpiredAt() != null) {

			return null;
		}

		session.setUpdatedAt(updatedAt);

		final DateTime expiresAt = session.getExpiresAt();

		if (updatedAt.isAfter(expiresAt)) {

			session.setExpiredAt(updatedAt);

			return null;
		}

		session.setExpiresAt(updatedAt.plusMinutes(SESSION_TIMEOUT_MINUTES));

		return session.getUsername();
	}

	@Override
	public boolean isValidUserPassword(final String username, final String password) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(password, "password");

		return passwords.containsKey(username) //
				&& password.contentEquals(passwords.get(username));
	}

	@Override
	public UserSessionDto newUserSession(final String username, final DateTime createdAt)
			throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(createdAt, "createdAt");

		final MutableUserSessionDto dto = instantiate(MutableUserSessionDto.class) //
				.setUsername(username) //
				.setCreatedAt(createdAt) //
				.setUpdatedAt(createdAt) //
				.setExpiresAt(createdAt.plusMinutes(SESSION_TIMEOUT_MINUTES));

		return retryUntil(4_000, 0, () -> {

			final String userSessionId = "S-" //
					+ System.currentTimeMillis() + "-" //
					+ randomAlphanumeric(20);

			dto.setUserSessionId(userSessionId);

			final UserSessionDto old = sessions.putIfAbsent(userSessionId, dto);

			return old == null ? dto : null;
		});
	}

	@Override
	@Nullable
	public UserSessionDto getUserSession(final String userSessionId, final DateTime updatedAt)
			throws SQLException, IOException {

		checkNotNull(userSessionId, "userSessionId");
		checkNotNull(updatedAt, "updatedAt");

		final MutableUserSessionDto session = sessions.get(userSessionId);

		if (session == null) {

			return null;
		}

		if (session.getExpiredAt() == null) {

			session.setUpdatedAt(updatedAt);

			final DateTime expiresAt = session.getExpiresAt();

			if (updatedAt.isAfter(expiresAt)) {

				session.setExpiredAt(updatedAt);

			} else {

				session.setExpiresAt(updatedAt.plusMinutes(SESSION_TIMEOUT_MINUTES));
			}
		}

		return session;
	}

	@Override
	public void terminateSession(final String userSessionId, @Nullable final DateTime updatedAt,
			final DateTime expiredAt) throws SQLException, IOException {

		checkNotNull(userSessionId, "userSessionId");
		checkNotNull(expiredAt, "expiredAt");

		final MutableUserSessionDto session = sessions.get(userSessionId);

		if (session == null || session.getExpiredAt() != null) {

			return;
		}

		if (updatedAt != null) {

			session.setUpdatedAt(updatedAt);
		}

		session.setExpiredAt(expiredAt);
	}

	@Override
	@Nullable
	public DateTime getLastActiveAt(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		DateTime maxLastActiveAt = null;

		for (final UserSessionDto session : sessions.values()) {

			if (!username.contentEquals(session.getUsername())) {
				continue;
			}

			final DateTime lastActiveAt = session.getUpdatedAt();

			if (maxLastActiveAt == null || lastActiveAt.isAfter(maxLastActiveAt)) {
				maxLastActiveAt = lastActiveAt;
			}
		}

		return maxLastActiveAt;
	}

	@Override
	public UserSessionsDto getUserSessions(final UserSessionsDtoQuery query) throws SQLException, IOException {

		checkNotNull(query, "query");

		final UserSessionFiltering filtering = query.getFiltering();
		final SortBy[] sortBys = query.getSortBys();
		final int start = query.getStart();
		final int limit = query.getLimit();

		final List<UserSessionDto> extract = new ArrayList<>();

		for (final MutableUserSessionDto session : sessions.values()) {

			if (filtering == null || match(session, filtering)) {

				extract.add(session);
			}
		}

		final MutableUserSessionsDto sessions = instantiate(MutableUserSessionsDto.class) //
				.setTotal(extract.size());

		final Comparator<? super UserSessionDto> comparator = Comparators.get(sortBys);

		extract.sort(comparator);

		int offset = 0;

		for (final UserSessionDto session : extract) {

			if (offset >= start + limit) {

				break;

			} else if (offset >= start) {

				sessions.addToResults(session);
			}

			++offset;
		}

		return sessions;
	}
}
