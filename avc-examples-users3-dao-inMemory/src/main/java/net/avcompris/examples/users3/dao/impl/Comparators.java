package net.avcompris.examples.users3.dao.impl;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.compareNullable_ASC;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.compareNullable_DESC;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.compare_ASC;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.compare_DESC;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.getComparator_CREATED_AT;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.getComparator_CREATED_AT_DESC;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.getComparator_REVISION;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.getComparator_REVISION_DESC;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.getComparator_UPDATED_AT;
import static net.avcompris.commons3.dao.impl.ComparatorUtils.getComparator_UPDATED_AT_DESC;

import java.util.Comparator;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;

import net.avcompris.examples.users3.dao.UserDto;
import net.avcompris.examples.users3.dao.UserSessionDto;
import net.avcompris.examples.users3.dao.UserSessionsDtoQuery;
import net.avcompris.examples.users3.dao.UsersDtoQuery;

abstract class Comparators {

	public static Comparator<? super UserDto> get(final UsersDtoQuery.SortBy sortBy) {

		checkNotNull(sortBy, "sortBy");

		switch (sortBy) {

		case SORT_BY_USERNAME:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compare_ASC(u1.getUsername(), u2.getUsername());
				}
			};

		case SORT_BY_USERNAME_DESC:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compare_DESC(u1.getUsername(), u2.getUsername());
				}
			};

		case SORT_BY_ROLENAME:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compare_ASC(u1.getRolename(), u2.getRolename());
				}
			};

		case SORT_BY_ROLENAME_DESC:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compare_DESC(u1.getRolename(), u2.getRolename());
				}
			};

		case SORT_BY_ENABLED:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compare_ASC(u1.isEnabled(), u2.isEnabled());
				}
			};

		case SORT_BY_ENABLED_DESC:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compare_DESC(u1.isEnabled(), u2.isEnabled());
				}
			};

		case SORT_BY_CREATED_AT:

			return getComparator_CREATED_AT();

		case SORT_BY_CREATED_AT_DESC:

			return getComparator_CREATED_AT_DESC();

		case SORT_BY_UPDATED_AT:

			return getComparator_UPDATED_AT();

		case SORT_BY_UPDATED_AT_DESC:

			return getComparator_UPDATED_AT_DESC();

		case SORT_BY_LAST_ACTIVE_AT:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compareNullable_ASC(u1.getLastActiveAt(), u2.getLastActiveAt());
				}
			};

		case SORT_BY_LAST_ACTIVE_AT_DESC:

			return new Comparator<UserDto>() {

				@Override
				public int compare(final UserDto u1, final UserDto u2) {
					return compareNullable_DESC(u1.getLastActiveAt(), u2.getLastActiveAt());
				}
			};

		case SORT_BY_REVISION:

			return getComparator_REVISION();

		case SORT_BY_REVISION_DESC:

			return getComparator_REVISION_DESC();

		default:

			throw new NotImplementedException("sortBy: " + sortBy);
		}
	}

	public static Comparator<? super UserDto> get(final UsersDtoQuery.SortBy... sortBys) {

		checkArgument(sortBys != null && sortBys.length != 0, "sortBys should not be empty");

		return new Comparator<UserDto>() {

			@Override
			public int compare(final UserDto u1, final UserDto u2) {

				for (final UsersDtoQuery.SortBy sortBy : sortBys) {

					final Comparator<? super UserDto> comparator = get(sortBy);

					final int compare = comparator.compare(u1, u2);

					if (compare != 0) {

						return compare;
					}
				}

				return 0;
			}

		};
	}

	public static Comparator<? super UserSessionDto> get(final UserSessionsDtoQuery.SortBy sortBy) {

		checkNotNull(sortBy, "sortBy");

		switch (sortBy) {

		case SORT_BY_USERNAME:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return compare_ASC(s1.getUsername(), s2.getUsername());
				}
			};

		case SORT_BY_USERNAME_DESC:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return compare_DESC(s1.getUsername(), s2.getUsername());
				}
			};

		case SORT_BY_USER_SESSION_ID:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return compare_ASC(s1.getUserSessionId(), s2.getUserSessionId());
				}
			};

		case SORT_BY_USER_SESSION_ID_DESC:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return compare_DESC(s1.getUserSessionId(), s2.getUserSessionId());
				}
			};

		case SORT_BY_CREATED_AT:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return s1.getCreatedAt().compareTo(s2.getCreatedAt());
				}
			};

		case SORT_BY_CREATED_AT_DESC:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return s2.getCreatedAt().compareTo(s1.getCreatedAt());
				}
			};

		case SORT_BY_UPDATED_AT:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return s1.getUpdatedAt().compareTo(s2.getUpdatedAt());
				}
			};

		case SORT_BY_UPDATED_AT_DESC:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return s2.getUpdatedAt().compareTo(s1.getUpdatedAt());
				}
			};

		case SORT_BY_EXPIRES_AT:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return s1.getExpiresAt().compareTo(s2.getExpiresAt());
				}
			};

		case SORT_BY_EXPIRES_AT_DESC:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					return s2.getExpiresAt().compareTo(s1.getExpiresAt());
				}
			};

		case SORT_BY_EXPIRED_AT:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					final DateTime expiredAt1 = s1.getExpiredAt();
					final DateTime expiredAt2 = s2.getExpiredAt();

					return expiredAt1 == null && expiredAt2 == null //
							? 0 //
							: expiredAt1 == null //
									? 1 //
									: expiredAt2 == null //
											? -1 //
											: expiredAt1.compareTo(expiredAt1);
				}
			};

		case SORT_BY_EXPIRED_AT_DESC:

			return new Comparator<UserSessionDto>() {

				@Override
				public int compare(final UserSessionDto s1, final UserSessionDto s2) {
					final DateTime expiredAt2 = s2.getExpiredAt();
					final DateTime expiredAt1 = s1.getExpiredAt();

					return expiredAt1 == null && expiredAt2 == null //
							? 0 //
							: expiredAt2 == null //
									? 1 //
									: expiredAt1 == null //
											? -1 //
											: expiredAt2.compareTo(expiredAt1);
				}
			};

		default:

			throw new NotImplementedException("sortBy: " + sortBy);
		}
	}

	public static Comparator<? super UserSessionDto> get(final UserSessionsDtoQuery.SortBy... sortBys) {

		checkArgument(sortBys != null && sortBys.length != 0, //
				"sortBys should not be empty");

		return new Comparator<UserSessionDto>() {

			@Override
			public int compare(final UserSessionDto s1, final UserSessionDto s2) {

				for (final UserSessionsDtoQuery.SortBy sortBy : sortBys) {

					final Comparator<? super UserSessionDto> comparator = get(sortBy);

					final int compare = comparator.compare(s1, s2);

					if (compare != 0) {

						return compare;
					}
				}

				return 0;
			}

		};
	}
}
