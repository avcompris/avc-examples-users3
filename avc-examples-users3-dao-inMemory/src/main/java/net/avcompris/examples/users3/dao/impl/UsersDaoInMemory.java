package net.avcompris.examples.users3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons.query.impl.FilteringsFactory.match;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Nullable;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.avcompris.commons3.dao.exception.DuplicateEntityException;
import net.avcompris.commons3.dao.impl.AbstractDao;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.dao.UserDto;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.UsersDto;
import net.avcompris.examples.users3.dao.UsersDtoQuery;
import net.avcompris.examples.users3.dao.UsersDtoQuery.SortBy;
import net.avcompris.examples.users3.dao.impl.MutableUserDto;
import net.avcompris.examples.users3.dao.impl.MutableUsersDto;
import net.avcompris.examples.users3.query.UserFiltering;

@Component
public final class UsersDaoInMemory extends AbstractDao implements UsersDao {

	private final Map<String, MutableUserDto> users = new ConcurrentHashMap<>();

	@Autowired
	public UsersDaoInMemory(final Clock clock) {

		super(clock);
	}

	@Override
	public UsersDto getUsers(final UsersDtoQuery query) throws SQLException, IOException {

		checkNotNull(query, "query");

		final UserFiltering filtering = query.getFiltering();
		final SortBy[] sortBys = query.getSortBys();
		final int start = query.getStart();
		final int limit = query.getLimit();

		final List<UserDto> extract = new ArrayList<>();

		for (final MutableUserDto user : users.values()) {

			if (filtering == null || match(user, filtering)) {

				extract.add(user);
			}
		}

		final MutableUsersDto users = instantiate(MutableUsersDto.class) //
				.setTotal(extract.size());

		final Comparator<? super UserDto> comparator = Comparators.get(sortBys);

		extract.sort(comparator);

		int offset = 0;

		for (final UserDto user : extract) {

			if (offset >= start + limit) {

				break;

			} else if (offset >= start) {

				users.addToResults(user);
			}

			++offset;
		}

		return users;
	}

	@Override
	public void createUser(final String username, //
			final String rolename, //
			@Nullable final String preferredLang, //
			@Nullable final String preferredTimeZone, //
			final boolean enabled //
	) throws SQLException, IOException, DuplicateEntityException {

		checkNotNull(username, "username");
		checkNotNull(rolename, "rolename");

		final DateTime now = clock.now();

		final MutableUserDto user = instantiate(MutableUserDto.class) //
				.setUsername(username) //
				.setRolename(rolename) //
				.setPreferredLang(preferredLang) //
				.setPreferredTimeZone(preferredTimeZone) //
				.setEnabled(enabled) //
				.setCreatedAt(now) //
				.setUpdatedAt(now) //
				.setRevision(1);

		final UserDto old = users.putIfAbsent(username, user);

		if (old != null) {
			throw new DuplicateEntityException("username: " + username);
		}
	}

	@Override
	@Nullable
	public UserDto getUser(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		final MutableUserDto userDto = users.get(username);

		if (userDto == null) {
			return null;
		}

		return userDto;
	}

	@Override
	public void setLastActiveAt(final String username, final DateTime lastActiveAt) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(lastActiveAt, "lastActiveAt");

		final MutableUserDto userDto = users.get(username);

		if (userDto == null) {
			return;
		}

		userDto.setLastActiveAt(lastActiveAt);
	}

	@Override
	public void updateUser(final String username, //
			final String rolename, //
			@Nullable final String preferredLang, //
			@Nullable final String preferredTimeZone, //
			final boolean enabled, //
			final int fromRevision //
	) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(rolename, "rolename");

		final MutableUserDto dto = users.get(username);

		if (dto == null) { // TODO What do we really expect here?
			return;
		}

		final int actualRevision = dto.getRevision();

		if (actualRevision != fromRevision) {
			throw new ConcurrentModificationException(
					"revision should be: " + fromRevision + ", but was: " + actualRevision);
		}

		final DateTime now = clock.now();

		dto.setUpdatedAt(now) //
				.setRolename(rolename) //
				.setEnabled(enabled) //
				.setRevision(fromRevision + 1);

		if (preferredLang != null) {
			dto.setPreferredLang(preferredLang);
		}

		if (preferredTimeZone != null) {
			dto.setPreferredTimeZone(preferredTimeZone);
		}
	}

	@Override
	public void deleteUser(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		users.remove(username);
	}
}
