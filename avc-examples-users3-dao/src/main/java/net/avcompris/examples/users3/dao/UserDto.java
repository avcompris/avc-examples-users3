package net.avcompris.examples.users3.dao;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.commons3.dao.EntityDto;

public interface UserDto extends EntityDto {

	String getUsername();

	String getRolename();

	@Nullable
	String getPreferredLang();

	@Nullable
	String getPreferredTimeZone();

	@Nullable
	DateTime getLastActiveAt();

	boolean isEnabled();
}
