package net.avcompris.examples.users3.dao;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.commons3.dao.exception.DuplicateEntityException;

public interface UsersDao {

	UsersDto getUsers(UsersDtoQuery query) throws SQLException, IOException;

	void createUser(String username, //
			String rolename, //
			@Nullable String preferredLang, //
			@Nullable String preferredTimeZone, //
			boolean enabled //
	) throws SQLException, IOException, DuplicateEntityException;

	@Nullable
	UserDto getUser(String username) throws SQLException, IOException;

	void updateUser(String username, //
			String rolename, //
			@Nullable String preferredLang, //
			@Nullable String preferredTimeZone, //
			boolean enabled, //
			int fromRevision //
	) throws SQLException, IOException;

	void deleteUser(String username) throws SQLException, IOException;

	void setLastActiveAt(String username, DateTime lastActiveAt) throws SQLException, IOException;
}
