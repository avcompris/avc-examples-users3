package net.avcompris.examples.users3.dao.impl;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.commons3.dao.impl.MutableEntityDto;
import net.avcompris.examples.users3.dao.UserDto;

public interface MutableUserDto extends MutableEntityDto, UserDto {

	MutableUserDto setUsername(String username);

	MutableUserDto setRolename(String rolename);

	MutableUserDto setPreferredLang(@Nullable String preferredLang);

	MutableUserDto setPreferredTimeZone(@Nullable String preferredTimeZone);

	MutableUserDto setLastActiveAt(@Nullable DateTime lastActiveAt);

	@Override
	MutableUserDto setCreatedAt(DateTime createdAt);

	@Override
	MutableUserDto setUpdatedAt(DateTime updatedAt);

	MutableUserDto setEnabled(boolean enabled);

	@Override
	MutableUserDto setRevision(int revision);
}
