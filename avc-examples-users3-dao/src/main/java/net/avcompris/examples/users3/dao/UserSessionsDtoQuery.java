package net.avcompris.examples.users3.dao;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.UserSessionFiltering;
import net.avcompris.commons3.dao.EntitiesDtoQuery;

public interface UserSessionsDtoQuery extends EntitiesDtoQuery<UserSessionFiltering, UserSessionFiltering.Field> {

	@Override
	@Nullable
	UserSessionFiltering getFiltering();

	enum SortBy implements EntitiesDtoQuery.SortBy {

		SORT_BY_USERNAME, SORT_BY_USERNAME_DESC, //
		SORT_BY_USER_SESSION_ID, SORT_BY_USER_SESSION_ID_DESC, //
		SORT_BY_CREATED_AT, SORT_BY_CREATED_AT_DESC, //
		SORT_BY_UPDATED_AT, SORT_BY_UPDATED_AT_DESC, //
		SORT_BY_EXPIRES_AT, SORT_BY_EXPIRES_AT_DESC, //
		SORT_BY_EXPIRED_AT, SORT_BY_EXPIRED_AT_DESC;

		/**
		 * For speed’s sake.
		 */
		private final String sqlField;

		/**
		 * For speed’s sake.
		 */
		private final boolean isDesc;

		SortBy() {

			this.sqlField = EntitiesDtoQuery.SortBy.toSqlField(name());
			this.isDesc = EntitiesDtoQuery.SortBy.isDesc(name());
		}

		@Override
		public String toSqlField() {

			return sqlField;
		}

		@Override
		public boolean isDesc() {

			return isDesc;
		}
	}

	enum Expand implements EntitiesDtoQuery.Expand {
		EXPAND_ALL, //
		EXPAND_USERNAME, //
		EXPAND_USER_SESSION_ID, //
		EXPAND_CREATED_AT, //
		EXPAND_UPDATED_AT, //
		EXPAND_EXPIRES_AT, //
		EXPAND_EXPIRED_AT, //
	}

	@Override
	SortBy[] getSortBys();

	@Override
	Expand[] getExpands();

	// @Override
	UserSessionsDtoQuery setFiltering(@Nullable UserSessionFiltering filtering);

	UserSessionsDtoQuery setSortBys(SortBy... sortBys);

	UserSessionsDtoQuery setExpands(Expand... expands);

	@Override
	UserSessionsDtoQuery setStart(int start);

	@Override
	UserSessionsDtoQuery setLimit(int limit);
}
