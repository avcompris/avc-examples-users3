package net.avcompris.examples.users3.dao;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

public interface UserSessionDto {

	String getUserSessionId();

	String getUsername();

	DateTime getCreatedAt();

	DateTime getUpdatedAt();

	DateTime getExpiresAt();

	@Nullable
	DateTime getExpiredAt();
}
