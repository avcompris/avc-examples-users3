package net.avcompris.examples.users3.dao;

import javax.annotation.Nullable;

import net.avcompris.commons3.dao.EntitiesDtoQuery;
import net.avcompris.examples.users3.query.UserFiltering;

public interface UsersDtoQuery extends EntitiesDtoQuery<UserFiltering, UserFiltering.Field> {

	@Override
	@Nullable
	UserFiltering getFiltering();

	enum SortBy implements EntitiesDtoQuery.SortBy {

		SORT_BY_USERNAME, SORT_BY_USERNAME_DESC, //
		SORT_BY_ROLENAME, SORT_BY_ROLENAME_DESC, //
		SORT_BY_ENABLED, SORT_BY_ENABLED_DESC, //
		SORT_BY_CREATED_AT, SORT_BY_CREATED_AT_DESC, //
		SORT_BY_UPDATED_AT, SORT_BY_UPDATED_AT_DESC, //
		SORT_BY_LAST_ACTIVE_AT, SORT_BY_LAST_ACTIVE_AT_DESC, //
		SORT_BY_REVISION, SORT_BY_REVISION_DESC; //

		/**
		 * For speed’s sake.
		 */
		private final String sqlField;

		/**
		 * For speed’s sake.
		 */
		private final boolean isDesc;

		SortBy() {

			this.sqlField = EntitiesDtoQuery.SortBy.toSqlField(name());
			this.isDesc = EntitiesDtoQuery.SortBy.isDesc(name());
		}

		@Override
		public String toSqlField() {

			return sqlField;
		}

		@Override
		public boolean isDesc() {

			return isDesc;
		}
	}

	enum Expand implements EntitiesDtoQuery.Expand {
		EXPAND_ALL, //
		EXPAND_USERNAME, //
		EXPAND_ROLENAME, //
		EXPAND_ENABLED, //
		EXPAND_CREATED_AT, //
		EXPAND_UPDATED_AT, //
		EXPAND_LAST_ACTIVE_AT, //
	}

	@Override
	SortBy[] getSortBys();

	@Override
	Expand[] getExpands();

	// @Override
	UsersDtoQuery setFiltering(@Nullable UserFiltering filtering);

	UsersDtoQuery setSortBys(SortBy... sortBys);

	UsersDtoQuery setExpands(Expand... expands);

	@Override
	UsersDtoQuery setStart(int start);

	@Override
	UsersDtoQuery setLimit(int limit);
}
