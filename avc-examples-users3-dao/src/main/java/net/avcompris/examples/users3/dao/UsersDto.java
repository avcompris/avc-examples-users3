package net.avcompris.examples.users3.dao;

import net.avcompris.commons3.dao.EntitiesDto;

public interface UsersDto extends EntitiesDto<UserDto> {

	@Override
	UserDto[] getResults();
}
