package net.avcompris.examples.users3.dao.impl;

import net.avcompris.examples.users3.dao.UserSessionDto;
import net.avcompris.examples.users3.dao.UserSessionsDto;

public interface MutableUserSessionsDto extends UserSessionsDto {

	MutableUserSessionsDto setTotal(int total);

	MutableUserSessionsDto addToResults(UserSessionDto session);

	MutableUserSessionsDto setSqlWhereClause(String sqlWhereClause);
}
