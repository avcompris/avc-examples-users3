package net.avcompris.examples.users3.dao;

import javax.annotation.Nullable;

public interface UserSessionsDto {

	UserSessionDto[] getResults();

	int getTotal();

	@Nullable
	String getSqlWhereClause();
}
