package net.avcompris.examples.users3.dao.impl;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

import net.avcompris.examples.users3.dao.UserSessionDto;

public interface MutableUserSessionDto extends UserSessionDto {

	MutableUserSessionDto setUserSessionId(String userSessionId);

	MutableUserSessionDto setUsername(String username);

	MutableUserSessionDto setCreatedAt(DateTime createdAt);

	MutableUserSessionDto setUpdatedAt(DateTime updatedAt);

	MutableUserSessionDto setExpiresAt(DateTime expiresAt);

	MutableUserSessionDto setExpiredAt(@Nullable DateTime expiredAt);

}
