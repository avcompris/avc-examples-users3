package net.avcompris.examples.users3.dao.impl;

import net.avcompris.commons3.dao.impl.MutableEntitiesDto;
import net.avcompris.examples.users3.dao.UserDto;
import net.avcompris.examples.users3.dao.UsersDto;

public interface MutableUsersDto extends MutableEntitiesDto<UserDto>, UsersDto {

	@Override
	MutableUsersDto setTotal(int total);

	@Override
	MutableUsersDto addToResults(UserDto user);

	@Override
	MutableUsersDto setSqlWhereClause(String sqlWhereClause);
}
