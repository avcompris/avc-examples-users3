package net.avcompris.examples.users3.dao;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Nullable;

import org.joda.time.DateTime;

public interface AuthDao {

	UserSessionsDto getUserSessions(UserSessionsDtoQuery query) throws SQLException, IOException;

	void setUserPassword(String username, String password) throws SQLException, IOException;

	void removeUserPassword(String username) throws SQLException, IOException;

	@Nullable
	String getUsernameByAuthorization(String authorization, DateTime updatedAt) throws SQLException, IOException;

	@Nullable
	String getUsernameBySessionId(String userSessionId, DateTime updatedAt) throws SQLException, IOException;

	boolean isValidUserPassword(String username, String password) throws SQLException, IOException;

	UserSessionDto newUserSession(String username, DateTime createdAt) throws SQLException, IOException;

	@Nullable
	UserSessionDto getUserSession(String userSessionId, DateTime updatedAt) throws SQLException, IOException;

	void terminateSession(String userSessionId, @Nullable DateTime updatedAt, DateTime expiredAt)
			throws SQLException, IOException;

	@Nullable
	DateTime getLastActiveAt(String username) throws SQLException, IOException;
}
