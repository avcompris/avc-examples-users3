package net.avcompris.examples.users3.core.tests;

import static net.avcompris.commons3.core.tests.CoreTestUtils.grantAll;
import static net.avcompris.examples.shared3.core.tests.MyCoreTestUtils.defaultUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.core.tests.AbstractServiceTest;
import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.shared3.core.impl.CorrelationServiceImpl;

public abstract class AbstractCorrelationServicePurgeTest extends AbstractServiceTest<CorrelationDao> {

	protected CorrelationService correlationService;
	protected Clock dummyClock;

	@BeforeEach
	public final void setUpBeans() throws Exception {

		dummyClock = new DummyClock(60);

		final CorrelationDao correlationDao = getBeans(dummyClock);

		correlationService = new CorrelationServiceImpl(grantAll(), dummyClock, correlationDao);

		LogFactory.resetCorrelationId();
	}

	@Test
	public final void test_purge() throws Exception {

		final String correlationId = correlationService.getCorrelationId(null, null);

		assertEquals(correlationId, correlationService.getCorrelationId(correlationId, null));
		assertEquals(correlationId, correlationService.getCorrelationId(null, correlationId));
		assertEquals(correlationId, correlationService.getCorrelationId(correlationId, correlationId));

		dummyClock.now();

		correlationService.purgeOlderThanSec(correlationId, defaultUser(), 200);

		// Should not be purged
		//
		assertEquals(correlationId, correlationService.getCorrelationId(correlationId, null));

		dummyClock.now();

		correlationService.purgeOlderThanSec(correlationId, defaultUser(), 200);

		// Should have been purged
		//
		assertNotEquals(correlationId, correlationService.getCorrelationId(correlationId, null));
	}
}
