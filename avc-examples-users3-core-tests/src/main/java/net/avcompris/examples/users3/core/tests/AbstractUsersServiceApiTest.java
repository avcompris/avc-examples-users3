package net.avcompris.examples.users3.core.tests;

import static net.avcompris.commons3.core.tests.CoreTestUtils.defaultClock;
import static org.apache.commons.lang3.CharEncoding.UTF_8;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;

import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.core.AuthService;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.core.tests.AbstractServiceApiTest;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.examples.users3.core.impl.AuthServiceImpl;
import net.avcompris.examples.users3.core.impl.UsersServiceImpl;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.web.HealthCheckController;
import net.avcompris.examples.users3.web.UsersController;

public abstract class AbstractUsersServiceApiTest extends AbstractServiceApiTest<Pair<UsersDao, AuthDao>> {

	@SuppressWarnings("unchecked")
	protected AbstractUsersServiceApiTest(final TestSpec spec) {

		super(spec, "tutu", //
				HealthCheckController.class, //
				UsersController.class);
	}

	@BeforeEach
	public final void setUpBeans() throws Exception {

		final Pair<UsersDao, AuthDao> beans = getBeans(defaultClock());

		final UsersDao usersDao = beans.getLeft();
		final AuthDao authDao = beans.getRight();

		final Permissions permissions = new PermissionsImpl();

		System.setProperty("superadmin.authorizationFile", "target/superadmin.Authorization");

		inject(AuthService.class, //
				new AuthServiceImpl(permissions, defaultClock(), usersDao, authDao));

		inject(UsersService.class, //
				new UsersServiceImpl(permissions, defaultClock(), usersDao, authDao));

		FileUtils.write(new File("target", "superadmin.Authorization"), "tutu", UTF_8);
	}
}
