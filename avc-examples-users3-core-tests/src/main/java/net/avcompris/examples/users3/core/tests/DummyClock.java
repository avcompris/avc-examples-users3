package net.avcompris.examples.users3.core.tests;

import org.joda.time.DateTime;

import net.avcompris.commons3.utils.Clock;

/**
 * Use this Clock to increase time by 10 sec. each time you call
 * {@link DummyClock#now()}
 *
 * @author dandriana
 */
final class DummyClock implements Clock {

	private final int advanceBySec;

	private DateTime nextNow = new DateTime();

	DummyClock(final int advanceBySec) {

		this.advanceBySec = advanceBySec;
	}

	@Override
	public synchronized DateTime now() {

		final DateTime now = nextNow;

		nextNow = now.plusSeconds(advanceBySec);

		return now;
	}

	@Override
	public long nowMs() {

		return now().getMillis();
	}
}
