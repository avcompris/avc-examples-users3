package net.avcompris.examples.users3.core.tests;

import static net.avcompris.commons3.core.tests.CoreTestUtils.defaultClock;
import static net.avcompris.commons3.core.tests.CoreTestUtils.grantAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.core.tests.AbstractServiceTest;
import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.examples.shared3.core.impl.CorrelationServiceImpl;

public abstract class AbstractCorrelationServiceTest extends AbstractServiceTest<CorrelationDao> {

	protected CorrelationService correlationService;

	@BeforeEach
	public final void setUpBeans() throws Exception {

		final CorrelationDao correlationDao = getBeans(defaultClock());

		correlationService = new CorrelationServiceImpl(grantAll(), defaultClock(), correlationDao);
	}

	@Test
	public final void test_correlationIdParam() throws Exception {

		final String correlationId = correlationService.getCorrelationId("xx", "yyy");

		assertNotNull(correlationId);

		assertEquals(correlationId, correlationService.getCorrelationId(correlationId, null));

		assertEquals(correlationId, correlationService.getCorrelationId(correlationId, "yyy"));
	}

	@Test
	public final void test_correlationIdHeader() throws Exception {

		final String correlationId = correlationService.getCorrelationId("xx", "yyy");

		assertNotNull(correlationId);

		assertEquals(correlationId, correlationService.getCorrelationId(null, correlationId));

		final String correlationId2 = correlationService.getCorrelationId("xx", correlationId);

		assertEquals(correlationId, correlationId2);
	}

	@Test
	public final void test_correlationId_null_null() throws Exception {

		final String correlationId = correlationService.getCorrelationId(null, null);

		assertNotNull(correlationId);
	}
}
