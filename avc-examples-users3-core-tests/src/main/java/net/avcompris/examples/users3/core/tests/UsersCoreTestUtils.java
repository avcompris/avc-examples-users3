package net.avcompris.examples.users3.core.tests;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static net.avcompris.commons3.core.tests.CoreTestUtils.random40;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.examples.shared3.core.tests.MyCoreTestUtils.defaultUser;
import static net.avcompris.examples.users3.query.UserFiltering.Field.ROLE;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.eq;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ArrayUtils;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UsernameNotFoundException;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.api.UsersQuery;
import net.avcompris.examples.users3.core.api.UsersService;

public abstract class UsersCoreTestUtils {

	private static String CORRELATION_ID;
	private static UsersService USERS_SERVICE;

	public static String setCorrelationId(final String correlationId) {

		CORRELATION_ID = checkNotNull(correlationId, "correlationId");

		return correlationId;
	}

	public static String getCorrelationId() {

		checkState(CORRELATION_ID != null, "CORRELATION_ID should have been set");

		return CORRELATION_ID;
	}

	public static void setUsersService(final UsersService usersService) {

		USERS_SERVICE = checkNotNull(usersService, "usersService");
	}

	public static UsersService getUsersService() {

		checkState(USERS_SERVICE != null, "USERS_SERVICE should have been set");

		return USERS_SERVICE;
	}

	public static User ensureAnyUser(final Role role, final String... usernamesToExclude) throws ServiceException {

		checkNotNull(role, "role");

		final UsersInfo users = getUsers(instantiate(UsersQuery.class) //
				.setFiltering(eq(ROLE, role)));

		for (final UserInfo userInfo : users.getResults()) {

			if (!ArrayUtils.contains(usernamesToExclude, userInfo.getUsername())) {

				return toUser(userInfo);
			}
		}

		final String username = random40("U-");

		return toUser(createUser(username, instantiate(UserCreate.class) //
				.setEnabled(true) //
				.setRole(role)));
	}

	public static UserInfo createUser(final String username, final UserCreate create) throws ServiceException {

		checkNotNull(username, "username");
		checkNotNull(create, "create");

		return getUsersService().createUser(getCorrelationId(), defaultUser(), username, create);
	}

	public static UsersInfo getUsers() throws ServiceException {

		return getUsers(null);
	}

	public static UsersInfo getUsers(@Nullable final UsersQuery query) throws ServiceException {

		return getUsersService().getUsers(getCorrelationId(), defaultUser(), query);
	}

	public static int getUserCount() throws Exception {

		return getUsersService().getUsers(getCorrelationId(), defaultUser(), null) //
				.getTotal();
	}

	public static UserInfo getUser(String username) throws ServiceException {

		return getUsersService().getUser(getCorrelationId(), defaultUser(), username);
	}

	public static String ensureUsername(final String username) throws ServiceException {

		try {

			getUsersService().getUser(getCorrelationId(), defaultUser(), username);

			return username;

		} catch (final UsernameNotFoundException e) {

			// do nothing
		}

		getUsersService().createUser(getCorrelationId(), defaultUser(), username, instantiate(UserCreate.class) //
				.setRole(Role.REGULAR) //
				.setEnabled(true));

		return username;
	}

	public static User toUser(final UserInfo userInfo) {

		return new User() {

			@Override
			public String getUsername() {
				return userInfo.getUsername();
			}

			@Override
			public Role getRole() {
				return userInfo.getRole();
			}
		};
	}
}
