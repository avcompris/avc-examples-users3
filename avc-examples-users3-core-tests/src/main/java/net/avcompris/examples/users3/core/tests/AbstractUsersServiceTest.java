package net.avcompris.examples.users3.core.tests;

import static java.util.Locale.ENGLISH;
import static net.avcompris.commons.query.impl.FilteringsFactory.and;
import static net.avcompris.commons.query.impl.FilteringsFactory.or;
import static net.avcompris.commons3.core.tests.CoreTestUtils.defaultClock;
import static net.avcompris.commons3.core.tests.CoreTestUtils.newCorrelationId;
import static net.avcompris.commons3.core.tests.CoreTestUtils.random40;
import static net.avcompris.commons3.core.tests.CoreTestUtils.random8;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.examples.shared3.core.tests.MyCoreTestUtils.defaultUser;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_CREATED_AT;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_CREATED_AT_DESC;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_LAST_ACTIVE_AT;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_LAST_ACTIVE_AT_DESC;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_ROLE;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_ROLE_DESC;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_UPDATED_AT;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_UPDATED_AT_DESC;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_USERNAME;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_USERNAME_DESC;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.ensureUsername;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.getUserCount;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.setCorrelationId;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.setUsersService;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.toUser;
import static net.avcompris.examples.users3.query.UserFiltering.Field.CREATED_AT;
import static net.avcompris.examples.users3.query.UserFiltering.Field.LAST_ACTIVE_AT;
import static net.avcompris.examples.users3.query.UserFiltering.Field.UPDATED_AT;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.gt;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.gte;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.lt;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

import org.apache.commons.lang3.tuple.Pair;
import org.joda.time.DateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.api.exception.IllegalStateUpdateException;
import net.avcompris.commons3.api.exception.InvalidQueryFilteringException;
import net.avcompris.commons3.api.exception.InvalidRoleException;
import net.avcompris.commons3.api.exception.ReservedUsernameException;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthenticatedException;
import net.avcompris.commons3.api.exception.UnauthorizedException;
import net.avcompris.commons3.api.exception.UsernameAlreadyExistsException;
import net.avcompris.commons3.api.exception.UsernameNotFoundException;
import net.avcompris.commons3.core.AuthService;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.core.tests.AbstractServiceTest;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UserUpdate;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.api.UsersQuery;
import net.avcompris.examples.users3.api.UsersQuery.SortBy;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.examples.users3.core.impl.AuthServiceImpl;
import net.avcompris.examples.users3.core.impl.UsersServiceImpl;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;

public abstract class AbstractUsersServiceTest extends AbstractServiceTest<Pair<UsersDao, AuthDao>> {

	protected AuthService authService;
	protected UsersService usersService;

	@BeforeEach
	public final void setUpBeans() throws Exception {

		final Pair<UsersDao, AuthDao> beans = getBeans(defaultClock());

		final UsersDao usersDao = beans.getLeft();
		final AuthDao authDao = beans.getRight();

		final Permissions permissions = new PermissionsImpl();

		authService = new AuthServiceImpl(permissions, defaultClock(), usersDao, authDao);
		usersService = new UsersServiceImpl(permissions, defaultClock(), usersDao, authDao);

		setUsersService(usersService);

		LogFactory.resetCorrelationId();
	}

	@Test
	public final void testGetUsers_null() throws Exception {

		final UsersInfo users = usersService.getUsers(newCorrelationId(), defaultUser(), null);

		assertNotNull(users);

		users.getStart();
		users.getLimit();
		users.getSize();
		users.getTotal();
		users.getTookMs();
		assertNotNull(users.getResults());
		assertTrue(isBlank(users.getSqlWhereClause()));
	}

	@Test
	public final void testCreateUser_me_crash() throws Exception {

		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		assertThrows(ReservedUsernameException.class, ()

		-> usersService.createUser(correlationId, defaultUser(), "me", instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true)));
	}

	@Test
	public final void testCreateUser_full_noCrash() throws Exception {

		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		assumeFalse(usersService.hasUser(correlationId, defaultUser(), "full"));

		usersService.createUser(correlationId, defaultUser(), "full", instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		final UserInfo user = usersService.getUsers(correlationId, defaultUser(),
				usersService.validateUsersQuery(correlationId, defaultUser(), //
						"role = USERMGR and username = full", // q
						null, // sort
						0, // start
						10, // limit
						null // expand
				)).getResults()[0];

		assertEquals("full", user.getUsername());
	}

	@Test
	public final void testCreateUser_null_password() throws Exception {

		final String username = random40("USER-");
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(null) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		final UserInfo user = usersService.getUsers(correlationId, defaultUser(),
				usersService.validateUsersQuery(correlationId, defaultUser(), //
						"role = USERMGR and username = " + username, // q
						null, // sort
						0, // start
						10, // limit
						null // expand
				)).getResults()[0];

		assertEquals(username, user.getUsername());
		assertSame(Role.USERMGR, user.getRole());
	}

	@Test
	public final void testCreateDuplicateUser_crash() throws Exception {

		final String username = random40("USER-");
		final String password1 = random8();
		final String password2 = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password1) //
				.setRole(Role.REGULAR) //
				.setEnabled(true));

		assertThrows(UsernameAlreadyExistsException.class, ()

		-> usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password2) //
				.setRole(Role.REGULAR) //
				.setEnabled(true)));
	}

	@Test
	public final void testCreateUser_getUserMe() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final int count0 = getUserCount();

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.REGULAR) //
						.setEnabled(true));

		assertEquals(count0 + 1, getUserCount());

		assertEquals(username, created.getUsername());
		assertSame(Role.REGULAR, created.getRole());
		assertNotNull(created.getCreatedAt());
		assertNull(created.getLastActiveAt());
		assertEquals(true, created.isEnabled());

		final UserInfo user = usersService.getUserMe(correlationId, toUser(created));

		assertEquals(username, user.getUsername());
		assertSame(Role.REGULAR, user.getRole());
		assertNotNull(user.getCreatedAt());
		assertEquals(user.getCreatedAt(), created.getUpdatedAt());
		assertEquals(true, user.isEnabled());
	}

	@Test
	public final void testCreateUser_getUser() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final int count0 = getUserCount();

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.USERMGR) //
						.setEnabled(true));

		assertEquals(count0 + 1, getUserCount());

		assertEquals(username, created.getUsername());
		assertSame(Role.USERMGR, created.getRole());
		assertNotNull(created.getCreatedAt());
		assertEquals(created.getCreatedAt(), created.getUpdatedAt());
		assertNull(created.getLastActiveAt());
		assertEquals(true, created.isEnabled());
		assertEquals(1, created.getRevision());

		final UserInfo user = usersService.getUser(correlationId, defaultUser(), username);

		assertEquals(username, user.getUsername());
		assertSame(Role.USERMGR, user.getRole());
		assertNotNull(user.getCreatedAt());
		assertNull(created.getLastActiveAt());
		assertEquals(true, created.isEnabled());
		assertEquals(1, created.getRevision());
	}

	@Test
	public final void testGetInexistentUser() throws Exception {

		final String username = random40("USER-");
		final String correlationId = setCorrelationId(newCorrelationId());

		assertThrows(UsernameNotFoundException.class, ()

		-> usersService.getUser(correlationId, defaultUser(), username));
	}

	@Test
	public final void testCreateUser_getUsers() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final int count0 = getUserCount();

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		final UsersInfo users = usersService.getUsers(correlationId, defaultUser(), null);

		assertEquals(0, users.getStart());
		assertEquals(10, users.getLimit());
		assertNotEquals(0, users.getSize());
		assertEquals(count0 + 1, users.getTotal());

		assertNotNull(users.getResults()[0]);
	}

	@Test
	public final void testCreateUser_getUsers_afterCreatedAt() throws Exception {

		final String username1 = random40("USERb-");
		final String username2 = random40("USERa-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		Thread.sleep(10);

		final DateTime now1 = now();

		deleteIllegalUsers(correlationId, now1);

		usersService.createUser(correlationId, defaultUser(), username1, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		final UsersInfo users1 = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now1)));

		assertEquals(0, users1.getStart());
		assertEquals(10, users1.getLimit());
		assertEquals(1, users1.getSize());
		assertEquals(1, users1.getTotal());

		assertEquals(username1, users1.getResults()[0].getUsername());

		Thread.sleep(10);

		final DateTime now2 = now();

		usersService.createUser(correlationId, defaultUser(), username2, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		final UsersInfo users2 = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now2)));

		assertEquals(0, users2.getStart());
		assertEquals(10, users2.getLimit());
		assertEquals(1, users2.getSize());
		assertEquals(1, users2.getTotal());

		assertEquals(username2, users2.getResults()[0].getUsername());

		final UsersInfo users_1_2_sortByUsername = usersService.getUsers(correlationId, defaultUser(),
				instantiate(UsersQuery.class) //
						.setFiltering(gte(CREATED_AT, now1)));

		assertEquals(0, users_1_2_sortByUsername.getStart());
		assertEquals(10, users_1_2_sortByUsername.getLimit());
		assertEquals(2, users_1_2_sortByUsername.getSize());
		assertEquals(2, users_1_2_sortByUsername.getTotal());

		assertEquals(username2, users_1_2_sortByUsername.getResults()[0].getUsername());
		assertEquals(username1, users_1_2_sortByUsername.getResults()[1].getUsername());

		final UsersInfo users_1_2_sortByDate = usersService.getUsers(correlationId, defaultUser(),
				instantiate(UsersQuery.class) //
						.setFiltering(gte(CREATED_AT, now1)) //
						.setSortBys(SORT_BY_CREATED_AT));

		assertEquals(0, users_1_2_sortByDate.getStart());
		assertEquals(10, users_1_2_sortByDate.getLimit());
		assertEquals(2, users_1_2_sortByDate.getSize());
		assertEquals(2, users_1_2_sortByDate.getTotal());

		assertEquals(username1, users_1_2_sortByDate.getResults()[0].getUsername());
		assertEquals(username2, users_1_2_sortByDate.getResults()[1].getUsername());
	}

	@Test
	public final void testCreateUser_getUsers_smallPagination() throws Exception {

		final String username1 = random40("USERb-");
		final String username2 = random40("USERa-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		for (int i = 0; i < 4; ++i) {

			usersService.createUser(correlationId, defaultUser(), random40("USER-"), instantiate(UserCreate.class) //
					.setPassword(password) //
					.setRole(Role.USERMGR) //
					.setEnabled(true));
		}

		Thread.sleep(10);

		final DateTime now = now();

		deleteIllegalUsers(correlationId, now);

		usersService.createUser(correlationId, defaultUser(), username1, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		Thread.sleep(10);

		usersService.createUser(correlationId, defaultUser(), username2, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		final UsersInfo users2 = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now)) //
				.setSortBys(SORT_BY_CREATED_AT_DESC));

		System.out.println("users2:");

		System.out.println(users2.getSqlWhereClause());

		for (final UserInfo userInfo : users2.getResults()) {
			System.out.println(userInfo);
		}

		assertEquals(0, users2.getStart());
		assertEquals(10, users2.getLimit());
		assertEquals(2, users2.getSize());
		assertEquals(2, users2.getTotal());

		assertEquals(username2, users2.getResults()[0].getUsername());
		assertEquals(username1, users2.getResults()[1].getUsername());

		final UsersInfo users_1_2_page1 = usersService.getUsers(correlationId, defaultUser(),
				instantiate(UsersQuery.class) //
						.setFiltering(gte(CREATED_AT, now)) //
						.setStart(0) //
						.setLimit(1) //
						.setSortBys(SORT_BY_CREATED_AT));

		assertEquals(0, users_1_2_page1.getStart());
		assertEquals(1, users_1_2_page1.getLimit());
		assertEquals(1, users_1_2_page1.getSize());
		assertEquals(2, users_1_2_page1.getTotal());
		assertEquals(1, users_1_2_page1.getResults().length);

		assertEquals(username1, users_1_2_page1.getResults()[0].getUsername());

		final UsersInfo users_1_2_page2 = usersService.getUsers(correlationId, defaultUser(),
				instantiate(UsersQuery.class) //
						.setFiltering(gte(CREATED_AT, now)) //
						.setStart(1) //
						.setLimit(1) //
						.setSortBys(SORT_BY_CREATED_AT));

		assertEquals(1, users_1_2_page2.getStart());
		assertEquals(1, users_1_2_page2.getLimit());
		assertEquals(1, users_1_2_page2.getSize());
		assertEquals(2, users_1_2_page2.getTotal());
		assertEquals(1, users_1_2_page2.getResults().length);

		assertEquals(username2, users_1_2_page2.getResults()[0].getUsername());

		final UsersInfo users_1_2_page3 = usersService.getUsers(correlationId, defaultUser(),
				instantiate(UsersQuery.class) //
						.setFiltering(gte(CREATED_AT, now)) //
						.setStart(2) //
						.setLimit(1) //
						.setSortBys(SORT_BY_CREATED_AT));

		assertEquals(2, users_1_2_page3.getStart());
		assertEquals(1, users_1_2_page3.getLimit());
		assertEquals(0, users_1_2_page3.getSize());
		assertEquals(2, users_1_2_page3.getTotal());
		assertEquals(0, users_1_2_page3.getResults().length);
	}

	@Test
	public final void testCreateUser_getUsers_betweenCreatedAt() throws Exception {

		final String username1 = random40("USERa-");
		final String username2 = random40("USERb-");
		final String username3 = random40("USERc-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final DateTime now0 = now();

		final int count0 = getUserCount();

		Thread.sleep(10);

		// final DateTime now1 = now();

		usersService.createUser(correlationId, defaultUser(), username1, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		Thread.sleep(10);

		// final DateTime now2 = now();

		usersService.createUser(correlationId, defaultUser(), username2, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		Thread.sleep(10);

		final DateTime now3 = now();

		usersService.createUser(correlationId, defaultUser(), username3, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		assertEquals(count0 + 3, getUserCount());

		Thread.sleep(10);

		// final DateTime now4 = now();

		final UsersInfo users2 = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(and(gt(CREATED_AT, now0), lt(CREATED_AT, now3))));

		assertEquals(0, users2.getStart());
		assertEquals(10, users2.getLimit());
		assertEquals(2, users2.getSize());
		assertEquals(2, users2.getTotal());

		assertEquals(username1, users2.getResults()[0].getUsername());
		assertEquals(username2, users2.getResults()[1].getUsername());
	}

	@Test
	public final void test_validQuery_start_0() throws Exception {

		final String correlationId = setCorrelationId(newCorrelationId());

		final UsersQuery query1 = usersService.validateUsersQuery(correlationId, defaultUser(),
				"created_at eq 2019-03-04", null, 0, null, null);

		assertEquals(0, query1.getStart().intValue());

		final UsersQuery query2 = usersService.validateUsersQuery(correlationId, defaultUser(), null, null, 2, null,
				null);

		assertEquals(2, query2.getStart().intValue());
	}

	@Test
	public final void test_validQuery_limit_1() throws Exception {

		final String correlationId = setCorrelationId(newCorrelationId());

		final UsersQuery query1 = usersService.validateUsersQuery(correlationId, defaultUser(),
				"created_at eq 2019-03-04", null, null, 1, null);

		assertEquals(1, query1.getLimit().intValue());

		final UsersQuery query2 = usersService.validateUsersQuery(correlationId, defaultUser(), null, null, null, 2,
				null);

		assertEquals(2, query2.getLimit().intValue());
	}

	@Test
	public final void test_invalidQuery_start_minus1() throws Exception {

		assertThrows(InvalidQueryFilteringException.class, ()

		-> usersService.validateUsersQuery(newCorrelationId(), defaultUser(), null, null, -1, null, null));
	}

	@Test
	public final void test_invalidQuery_limit_0() throws Exception {

		usersService.validateUsersQuery(newCorrelationId(), defaultUser(), null, null, null, 0, null);
	}

	@Test
	public final void test_invalidQuery_limit_minus1() throws Exception {

		assertThrows(InvalidQueryFilteringException.class, ()

		-> usersService.validateUsersQuery(newCorrelationId(), defaultUser(), null, null, null, -1, null));
	}

	@Test
	public final void test_validQuery_sortBy_createdAt() throws Exception {

		final UsersQuery query = usersService.validateUsersQuery(newCorrelationId(), defaultUser(), null, "+created_at",
				null, null, null);

		assertArrayEquals(new SortBy[] { SORT_BY_CREATED_AT }, query.getSortBys());
	}

	@Test
	public final void test_validQuery_sortBy_m_createdAt_username() throws Exception {

		final UsersQuery query = usersService.validateUsersQuery(newCorrelationId(), defaultUser(), null, //
				"-created_at,username", null, null, null);

		assertArrayEquals(new SortBy[] { SORT_BY_CREATED_AT_DESC, SORT_BY_USERNAME }, query.getSortBys());
	}

	@Test
	public final void test_validQuery_sortBy_role_m_role() throws Exception {

		final UsersQuery query = usersService.validateUsersQuery(newCorrelationId(), defaultUser(), null, //
				"role,-role", null, null, null);

		assertArrayEquals(new SortBy[] { SORT_BY_ROLE, SORT_BY_ROLE_DESC }, query.getSortBys());
	}

	private final void deleteIllegalUsers(final String correlationId, final DateTime now) throws ServiceException {

		// 0. CLEAN UP ENVIRONMENT: OLD USERS WITH LAST_ACTIVE_AT > now() BECAUSE OF
		// AUTH_TESTS

		while (true) {

			final UsersInfo usersK00 = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
					.setFiltering(or( //
							gte(CREATED_AT, now), //
							gte(UPDATED_AT, now), //
							gte(LAST_ACTIVE_AT, now))) //
					.setSortBys(SORT_BY_LAST_ACTIVE_AT, SORT_BY_CREATED_AT));

			if (usersK00.getTotal() == 0) {
				break;
			}

			System.out.println("now: " + now);

			System.out.println("usersK00: (" + usersK00.getTotal() + ")");

			System.out.println("  sqlWhereClause: " + usersK00.getSqlWhereClause());

			for (final UserInfo user : usersK00.getResults()) {

				System.out.println("Deleting: " + user);

				usersService.deleteUser(correlationId, defaultUser(), user.getUsername());
			}
		}
	}

	@Test
	public final void testCreateUsers_getUsers_sortByXxx() throws Exception {

		final String correlationId = setCorrelationId(newCorrelationId());

		Thread.sleep(10);

		final DateTime now = now();

		deleteIllegalUsers(correlationId, now);

		// 1. RUN TEST

		final String username1 = random40("USERa-");
		final String username2 = random40("USERb-");
		final String username3 = random40("USERc-");
		final String username4 = random40("USERd-");
		final String username5 = random40("USERe-");
		final String username6 = random40("USERf-");

		assertEquals(0, usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(LAST_ACTIVE_AT, now)) //
				.setSortBys(SORT_BY_LAST_ACTIVE_AT, SORT_BY_CREATED_AT)).getTotal());

		final UserCreate create = instantiate(UserCreate.class) //
				.setEnabled(true) //
				.setPassword("xxx");

		usersService.createUser(correlationId, defaultUser(), username1, create //
				.setEnabled(true) //
				.setRole(Role.ADMIN));

		Thread.sleep(10);

		usersService.createUser(correlationId, defaultUser(), username2, create //
				.setEnabled(true) //
				.setRole(Role.USERMGR));

		Thread.sleep(10);

		usersService.createUser(correlationId, defaultUser(), username3, create //
				.setEnabled(true) //
				.setRole(Role.USERMGR));

		Thread.sleep(10);

		usersService.createUser(correlationId, defaultUser(), username4, create //
				.setEnabled(true) //
				.setRole(Role.USERMGR));

		Thread.sleep(10);

		usersService.createUser(correlationId, defaultUser(), username5, create //
				.setEnabled(true) //
				.setRole(Role.USERMGR));

		Thread.sleep(10);

		usersService.createUser(correlationId, defaultUser(), username6, create //
				.setEnabled(true) //
				.setRole(Role.REGULAR));

		final UsersInfo usersA = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now)) //
				.setSortBys(SORT_BY_USERNAME));

		assertEquals(username1, usersA.getResults()[0].getUsername());
		assertEquals(username2, usersA.getResults()[1].getUsername());
		assertEquals(6, usersA.getTotal());

		final UsersInfo usersB = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now)) //
				.setSortBys(SORT_BY_USERNAME_DESC));

		assertEquals(username6, usersB.getResults()[0].getUsername());
		assertEquals(username5, usersB.getResults()[1].getUsername());

		final UsersInfo usersC = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now)) //
				.setSortBys(SORT_BY_ROLE, SORT_BY_CREATED_AT));

		assertEquals(username1, usersC.getResults()[0].getUsername());
		assertEquals(username6, usersC.getResults()[1].getUsername());

		final UsersInfo usersD = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now)) //
				.setSortBys(SORT_BY_ROLE_DESC, SORT_BY_CREATED_AT));

		assertEquals(username2, usersD.getResults()[0].getUsername());
		assertEquals(username3, usersD.getResults()[1].getUsername());

		final UsersInfo usersG = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now)) //
				.setSortBys(SORT_BY_CREATED_AT));

		assertEquals(username1, usersG.getResults()[0].getUsername());
		assertEquals(username2, usersG.getResults()[1].getUsername());

		final UsersInfo usersH = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(CREATED_AT, now)) //
				.setSortBys(SORT_BY_CREATED_AT_DESC));

		assertEquals(username6, usersH.getResults()[0].getUsername());
		assertEquals(username5, usersH.getResults()[1].getUsername());

		final UsersInfo usersI = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(UPDATED_AT, now)) //
				.setSortBys(SORT_BY_UPDATED_AT));

		assertEquals(username1, usersI.getResults()[0].getUsername());
		assertEquals(username2, usersI.getResults()[1].getUsername());

		final UsersInfo usersJ = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(UPDATED_AT, now)) //
				.setSortBys(SORT_BY_UPDATED_AT_DESC));

		assertEquals(username6, usersJ.getResults()[0].getUsername());
		assertEquals(username5, usersJ.getResults()[1].getUsername());

		final UsersInfo usersK0 = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(and(gte(CREATED_AT, now), gte(LAST_ACTIVE_AT, now))) //
				.setSortBys(SORT_BY_LAST_ACTIVE_AT, SORT_BY_CREATED_AT));

		assertEquals(0, usersK0.getTotal());

		authService.authenticate(correlationId, username1, "xxx");

		Thread.sleep(10);

		authService.authenticate(correlationId, username2, "xxx");

		final UsersInfo usersK1 = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(LAST_ACTIVE_AT, now)) //
				.setSortBys(SORT_BY_LAST_ACTIVE_AT, SORT_BY_CREATED_AT));

		System.out.println("username1: " + username1);
		System.out.println("username2: " + username2);
		System.out.println("username3: " + username3);
		System.out.println("username4: " + username4);
		System.out.println("username5: " + username5);
		System.out.println("username6: " + username6);
		System.out.println("usersK1: (" + usersK1.getTotal() + ")");
		System.out.println("  sqlWhereClause: " + usersK1.getSqlWhereClause());
		for (final UserInfo user : usersK1.getResults()) {
			System.out.println("  " + user);
		}

		assertEquals(2, usersK1.getTotal());

		// assertEquals("superadmin", usersK1.getResults()[0].getUsername());
		assertEquals(username1, usersK1.getResults()[0].getUsername());
		assertEquals(username2, usersK1.getResults()[1].getUsername());

		final UsersInfo usersL = usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
				.setFiltering(gte(LAST_ACTIVE_AT, now)) //
				.setSortBys(SORT_BY_LAST_ACTIVE_AT_DESC));

		assertEquals(username2, usersL.getResults()[0].getUsername());
		assertEquals(username1, usersL.getResults()[1].getUsername());
	}

	@Test
	public final void testCreateUser_authenticateSetLastActiveAt() throws Exception {

		final String username = random40("USER-");
		final String password1 = random8();
		final String password2 = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password1) //
						.setRole(Role.USERMGR) //
						.setEnabled(true));

		assertNull(created.getLastActiveAt());

		final UserSession session1 = authService.authenticate(correlationId, username, password1);

		assertNotNull(session1);

		final UserInfo authenticated1 = usersService.getUser(correlationId, defaultUser(), username);

		assertNotNull(authenticated1.getLastActiveAt());

		Thread.sleep(10);

		UserSession session_invalid;
		try {
			session_invalid = authService.authenticate(correlationId, username, password2);
		} catch (final UnauthenticatedException e) {
			session_invalid = null;
		}

		assertNull(session_invalid);

		final UserInfo after_wrong_password = usersService.getUser(correlationId, defaultUser(), username);

		assertEquals(authenticated1.getLastActiveAt(), after_wrong_password.getLastActiveAt());

		final UserInfo updated = usersService.updateUser(correlationId, defaultUser(), username,
				instantiate(UserUpdate.class) //
						.setPassword(password2) //
						.setRole(Role.ADMIN) //
						.setFromRevision(1) //
						.setEnabled(true));

		assertEquals(authenticated1.getLastActiveAt(), updated.getLastActiveAt());

		try {
			session_invalid = authService.authenticate(correlationId, username, password1);
		} catch (final UnauthenticatedException e) {
			session_invalid = null;
		}

		assertNull(session_invalid);

		Thread.sleep(10);

		final UserSession session2 = authService.authenticate(correlationId, username, password2);

		assertNotNull(session2);

		final UserInfo authenticated2 = usersService.getUser(correlationId, defaultUser(), username);

		assertNotNull(authenticated2.getLastActiveAt());

		assertNotEquals(authenticated1.getLastActiveAt(), authenticated2.getLastActiveAt());
	}

	@Test
	public final void testCreateUser_updateUser() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String password2 = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.USERMGR) //
						.setEnabled(false));

		assertEquals(username, created.getUsername());
		assertSame(Role.USERMGR, created.getRole());
		assertNotNull(created.getCreatedAt());
		assertEquals(created.getCreatedAt(), created.getUpdatedAt());
		assertNull(created.getLastActiveAt());
		assertEquals(false, created.isEnabled());
		assertEquals(1, created.getRevision());

		Thread.sleep(10);

		final UserInfo updated = usersService.updateUser(correlationId, defaultUser(), username,
				instantiate(UserUpdate.class) //
						.setPassword(password2) //
						.setRole(Role.ADMIN) //
						.setFromRevision(1) //
						.setEnabled(true));

		assertEquals(username, updated.getUsername());
		assertSame(Role.ADMIN, updated.getRole());
		assertNotNull(updated.getUpdatedAt());
		assertEquals(created.getCreatedAt(), updated.getCreatedAt());
		assertNotEquals(updated.getCreatedAt(), updated.getUpdatedAt());
		assertNull(created.getLastActiveAt());
		assertEquals(true, updated.isEnabled());
		assertEquals(2, updated.getRevision());

		final UserInfo user = usersService.getUser(correlationId, defaultUser(), username);

		assertEquals(username, user.getUsername());
		assertSame(Role.ADMIN, user.getRole());
		assertNotNull(user.getUpdatedAt());
		assertEquals(updated.getCreatedAt(), user.getCreatedAt());
		assertEquals(updated.getUpdatedAt(), user.getUpdatedAt());
		assertNull(user.getLastActiveAt());
		assertEquals(true, updated.isEnabled());
		assertEquals(2, updated.getRevision());
	}

	@Test
	public final void testCreateUser_updateMe_wrongRole() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.REGULAR) //
						.setEnabled(true));

		assertEquals(username, created.getUsername());
		assertSame(Role.REGULAR, created.getRole());

		assertThrows(UnauthorizedException.class, ()

		-> usersService.updateUser(correlationId, toUser(created), username, instantiate(UserUpdate.class) //
				.setPassword(null) //
				.setRole(Role.ADMIN) //
				.setFromRevision(1) //
				.setEnabled(true)));
	}

	@Test
	public final void testCreateUser_updateFullMe_nullRole() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.REGULAR) //
						.setEnabled(true));

		assertEquals(username, created.getUsername());
		assertSame(Role.REGULAR, created.getRole());

		assertThrows(UnauthorizedException.class, ()

		-> usersService.updateUser(correlationId, toUser(created), username, instantiate(UserUpdate.class) //
				.setPassword(null) //
				.setRole(null) //
				.setFromRevision(1) //
				.setEnabled(true)));
	}

	@Test
	public final void testCreateUser_superadmin_update_nullRole() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.REGULAR) //
						.setEnabled(true));

		assertEquals(username, created.getUsername());
		assertSame(Role.REGULAR, created.getRole());

		final UserInfo updated = usersService.updateUser(correlationId, defaultUser(), username,
				instantiate(UserUpdate.class) //
						.setPassword(null) //
						.setRole(null) //
						.setFromRevision(1) //
						.setEnabled(true));

		assertEquals(username, updated.getUsername());
		assertSame(Role.REGULAR, updated.getRole());
	}

	@Test
	public final void testCreateUser_updateUser_deleteUser() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String password2 = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password2) //
				.setRole(Role.ADMIN) //
				.setFromRevision(1) //
				.setEnabled(true));

		usersService.deleteUser(correlationId, defaultUser(), username);

		assertThrows(UsernameNotFoundException.class, ()

		-> usersService.getUser(correlationId, defaultUser(), username));
	}

	@Test
	public final void testCreateUser_updateUser_incorrectRevision_1_4() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String password2 = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		assertThrows(IllegalStateUpdateException.class, ()

		-> usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password2) //
				.setRole(Role.ADMIN) //
				.setFromRevision(4) //
				.setEnabled(true)));
	}

	@Test
	public final void testCreateUser_updateUser_incorrectRevision_2_1() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String password2 = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password2) //
				.setRole(Role.ADMIN) //
				.setFromRevision(1) //
				.setEnabled(true));

		assertThrows(IllegalStateUpdateException.class, ()

		-> usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password2) //
				.setRole(Role.ADMIN) //
				.setFromRevision(1) //
				.setEnabled(true)));
	}

	@Test
	public final void testCreateUser_updateUser_correctRevision() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String password2 = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password2) //
				.setRole(Role.ADMIN) //
				.setFromRevision(1) //
				.setEnabled(true));

		usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password2) //
				.setRole(Role.ADMIN) //
				.setFromRevision(2) //
				.setEnabled(true));
	}

	@Test
	public final void testDelete_superadmin() throws Exception {

		assertThrows(ReservedUsernameException.class, ()

		-> usersService.deleteUser(newCorrelationId(), defaultUser(), "superadmin"));
	}

	@Test
	public final void testCreateUser_deleteMyself() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.USERMGR) //
						.setEnabled(true));

		final User user = toUser(created);

		assertThrows(UnauthorizedException.class, ()

		-> usersService.deleteUser(correlationId, user, username));
	}

	@Test
	public final void testCreateUser_ANONYMOUS() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		assertThrows(InvalidRoleException.class, ()

		-> usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.ANONYMOUS) //
				.setEnabled(true)));
	}

	@Test
	public final void testCreateUser_SUPERADMIN() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		assertThrows(InvalidRoleException.class, ()

		-> usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.SUPERADMIN) //
				.setEnabled(true)));
	}

	@Test
	public final void testUpdateUser_SUPERADMIN() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.USERMGR) //
				.setEnabled(true));

		assertThrows(InvalidRoleException.class, ()

		-> usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password) //
				.setRole(Role.SUPERADMIN) //
				.setEnabled(true)));
	}

	@Test
	public final void testUpdateullUser_ANONYMOUS() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.ADMIN) //
				.setEnabled(true));

		assertThrows(InvalidRoleException.class, ()

		-> usersService.updateUser(correlationId, defaultUser(), username, instantiate(UserUpdate.class) //
				.setPassword(password) //
				.setRole(Role.ANONYMOUS) //
				.setEnabled(true)));
	}

	@Test
	public final void testQuery_sortBys_all() throws Exception {

		final String correlationId = setCorrelationId(newCorrelationId());

		ensureUsername(random40("U-1"));
		ensureUsername(random40("U-2"));

		for (final SortBy sortBy : SortBy.values()) {

			usersService.getUsers(correlationId, defaultUser(), instantiate(UsersQuery.class) //
					.setSortBys(sortBy));

			usersService.getUsers(correlationId, defaultUser(),
					usersService.validateUsersQuery(correlationId, defaultUser(), //
							null, // q
							substringAfter(sortBy.name(), "SORT_BY_").toLowerCase(ENGLISH), // sort
							0, // start
							10, // limit
							null // expand
					));
		}
	}

	@Test
	public final void testCreateUser_preferredLang_and_TimeZone() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setPassword(password) //
						.setRole(Role.REGULAR) //
						.setPreferredLang("EN") //
						.setPreferredTimeZone("-04:00") //
						.setEnabled(true));

		assertEquals("EN", created.getPreferredLang());
		assertEquals("-04:00", created.getPreferredTimeZone());
	}

	@Test
	public final void testCreateUser_updateUserMe_preferredLang_and_TimeZone() throws Exception {

		final String username = random40("USER-");
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setRole(Role.REGULAR) //
						.setEnabled(false));

		assertSame(Role.REGULAR, created.getRole());

		assertEquals(null, created.getPreferredLang());
		assertEquals(null, created.getPreferredTimeZone());
		assertEquals(false, created.isEnabled());

		final UserInfo updated1 = usersService.updateUserMe(correlationId, toUser(created),
				instantiate(UserUpdate.class) //
						.setPreferredTimeZone("-04:00") //
						.setFromRevision(1));

		assertEquals(null, updated1.getPreferredLang());
		assertEquals("-04:00", updated1.getPreferredTimeZone());
		assertEquals(false, updated1.isEnabled());

		final UserInfo updated2 = usersService.updateUserMe(correlationId, toUser(created),
				instantiate(UserUpdate.class) //
						.setPreferredLang("EN") //
						.setEnabled(true) //
						.setFromRevision(2));

		assertEquals("EN", updated2.getPreferredLang());
		assertEquals("-04:00", updated2.getPreferredTimeZone());
		assertEquals(true, updated2.isEnabled());
	}

	@Test
	public final void testCreateUser_updateUser_preferredLang_and_TimeZone() throws Exception {

		final String username = random40("USER-");
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo created = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setRole(Role.REGULAR) //
						.setEnabled(true));

		assertEquals(true, created.isEnabled());

		final UserInfo updated1 = usersService.updateUser(correlationId, defaultUser(), username,
				instantiate(UserUpdate.class) //
						.setPreferredLang("EN") //
						.setFromRevision(1));

		assertEquals("EN", updated1.getPreferredLang());
		assertEquals(null, updated1.getPreferredTimeZone());
		assertEquals(true, updated1.isEnabled());

		final UserInfo updated2 = usersService.updateUser(correlationId, defaultUser(), username,
				instantiate(UserUpdate.class) //
						.setPreferredTimeZone("-04:00") //
						.setEnabled(false) //
						.setFromRevision(2));

		assertEquals("EN", updated2.getPreferredLang());
		assertEquals("-04:00", updated2.getPreferredTimeZone());
		assertEquals(false, updated2.isEnabled());
	}
}
