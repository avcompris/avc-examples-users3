package net.avcompris.examples.users3.core.tests;

import static net.avcompris.commons3.core.tests.CoreTestUtils.grantAll;
import static net.avcompris.commons3.core.tests.CoreTestUtils.newCorrelationId;
import static net.avcompris.commons3.core.tests.CoreTestUtils.random20;
import static net.avcompris.commons3.core.tests.CoreTestUtils.random40;
import static net.avcompris.commons3.core.tests.CoreTestUtils.random8;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.examples.shared3.core.tests.MyCoreTestUtils.defaultUser;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.setCorrelationId;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.api.UserSessions;
import net.avcompris.commons3.api.exception.UnauthenticatedException;
import net.avcompris.commons3.core.AuthService;
import net.avcompris.commons3.core.tests.AbstractServiceTest;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.examples.users3.core.impl.AuthServiceImpl;
import net.avcompris.examples.users3.core.impl.UsersServiceImpl;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;

public abstract class AbstractAuthServiceTest extends AbstractServiceTest<Pair<UsersDao, AuthDao>> {

	protected AuthService authService;
	protected UsersService usersService;
	protected Clock dummyClock;

	protected File superadminAuthorizationFile;

	@BeforeEach
	public final void setUpBeans() throws Exception {

		dummyClock = new DummyClock(60);

		final Pair<UsersDao, AuthDao> beans = getBeans(dummyClock);

		final UsersDao usersDao = beans.getLeft();
		final AuthDao authDao = beans.getRight();

		final String superadminAuthorizationFilePath = "target/superadmin.authorization";

		System.setProperty("superadmin.authorizationFile", superadminAuthorizationFilePath);

		superadminAuthorizationFile = new File(superadminAuthorizationFilePath);

		FileUtils.touch(superadminAuthorizationFile);

		authService = new AuthServiceImpl(grantAll(), dummyClock, usersDao, authDao);
		usersService = new UsersServiceImpl(grantAll(), dummyClock, usersDao, authDao);

		LogFactory.resetCorrelationId();
	}

	@Test
	public final void testSuperadminAuthorization() throws Exception {

		final String authorization = random20();

		FileUtils.writeStringToFile(superadminAuthorizationFile, authorization, UTF_8);

		final User user = authService.getAuthenticatedUser(authorization, null);

		assertSame(Role.SUPERADMIN, user.getRole());
	}

	@Test
	public final void testCreate10Users() throws Exception {

		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		for (int i = 0; i < 10; ++i) {

			usersService.createUser(correlationId, defaultUser(), random40("USER-"), instantiate(UserCreate.class) //
					.setRole(Role.REGULAR) //
					.setPassword(password) //
					.setEnabled(true));
		}
	}

	@Test
	public final void testCreateUsernamePassword() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final UserInfo userInfo0 = usersService.createUser(correlationId, defaultUser(), username,
				instantiate(UserCreate.class) //
						.setRole(Role.REGULAR) //
						.setPassword(password) //
						.setEnabled(true));

		assertEquals(username, userInfo0.getUsername());
		assertSame(Role.REGULAR, userInfo0.getRole());
		assertNotNull(userInfo0.getCreatedAt());
		assertEquals(userInfo0.getCreatedAt(), userInfo0.getUpdatedAt());
		assertNull(userInfo0.getLastActiveAt());
		assertEquals(1, userInfo0.getRevision());

		final UserSession session = authService.authenticate(correlationId, username, password);

		final String userSessionId = session.getUserSessionId();

		assertEquals(username, session.getUsername());
		assertNotNull(session.getCreatedAt());
		assertNotNull(session.getUpdatedAt());
		assertNotNull(session.getExpiresAt());
		assertNull(session.getExpiredAt());

		final User user = authService.getAuthenticatedUser(null, userSessionId);

		assertEquals(username, user.getUsername());
		assertSame(Role.REGULAR, user.getRole());

		final UserInfo userInfo2 = usersService.getUser(correlationId, defaultUser(), username);

		assertEquals(username, userInfo2.getUsername());
		assertEquals(userInfo2.getCreatedAt(), userInfo2.getUpdatedAt());
		assertNotNull(userInfo2.getLastActiveAt());
		assertEquals(1, userInfo2.getRevision());
	}

	@Test
	public final void testInvalidUsernamePassword() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		assertThrows(UnauthenticatedException.class, ()

		-> authService.authenticate(correlationId, username, password));
	}

	@Test
	public final void testGetSessions_null() throws Exception {

		final UserSessions sessions = authService.getUserSessions(newCorrelationId(), defaultUser(), null);

		assertNotNull(sessions);

		sessions.getStart();
		sessions.getLimit();
		sessions.getSize();
		sessions.getTotal();
		sessions.getTookMs();
		assertNotNull(sessions.getResults());
		assertTrue(isBlank(sessions.getSqlWhereClause()));
	}

	@Test
	public final void testSessionExpires() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.REGULAR) //
				.setEnabled(true));

		final UserSession session0 = authService.authenticate(correlationId, username, password);

		assertEquals(session0.getCreatedAt(), session0.getUpdatedAt());
		assertNotNull(session0.getExpiresAt());
		assertNull(session0.getExpiredAt());

		final String userSessionId = session0.getUserSessionId();

		final User user1 = authService.getAuthenticatedUser(null, userSessionId);

		assertNotNull(user1);

		dummyClock.now(); // Advance by 60 sec = 1 min.

		final User user2 = authService.getAuthenticatedUser(null, userSessionId);

		assertNotNull(user2);

		final UserSession session2 = authService.getUserSession(correlationId, defaultUser(), userSessionId);

		assertEquals(session0.getCreatedAt(), session2.getCreatedAt());
		assertNotEquals(session2.getCreatedAt(), session2.getUpdatedAt());
		assertNull(session2.getExpiredAt());

		assertNotNull(user2);

		for (int i = 0; i < 100; ++i) {
			dummyClock.now(); // Advance by 100 * 60 sec = 1 h 40 min.
		}

		final User user3 = authService.getAuthenticatedUser(null, userSessionId);

		assertNull(user3);

		final UserSession session3 = authService.getUserSession(correlationId, defaultUser(), userSessionId);

		assertEquals(session0.getCreatedAt(), session3.getCreatedAt());
		assertNotEquals(session3.getCreatedAt(), session3.getUpdatedAt());
		assertNotNull(session3.getExpiresAt());
		assertNotNull(session3.getExpiredAt());
	}

	@Test
	public final void testSessionKeptAliveDoesntExpire() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.REGULAR) //
				.setEnabled(true));

		final UserSession session0 = authService.authenticate(correlationId, username, password);

		assertEquals(session0.getCreatedAt(), session0.getUpdatedAt());
		assertNotNull(session0.getExpiresAt());
		assertNull(session0.getExpiredAt());

		final String userSessionId = session0.getUserSessionId();

		final User user1 = authService.getAuthenticatedUser(null, userSessionId);

		assertNotNull(user1);

		dummyClock.now(); // Advance by 60 sec = 1 min.

		final User user2 = authService.getAuthenticatedUser(null, userSessionId);

		assertNotNull(user2);

		final UserSession session2 = authService.getUserSession(correlationId, defaultUser(), userSessionId);

		assertEquals(session0.getCreatedAt(), session2.getCreatedAt());
		assertNotEquals(session2.getCreatedAt(), session2.getUpdatedAt());
		assertNull(session2.getExpiredAt());

		assertNotNull(user2);

		for (int i = 0; i < 100; ++i) {

			authService.getAuthenticatedUser(null, userSessionId);

			dummyClock.now(); // Advance by 100 * 60 sec = 1 h 40 min.
		}

		final User user3 = authService.getAuthenticatedUser(null, userSessionId);

		assertNotNull(user3);

		final UserSession session3 = authService.getUserSession(correlationId, defaultUser(), userSessionId);

		assertEquals(session0.getCreatedAt(), session3.getCreatedAt());
		assertNotEquals(session3.getCreatedAt(), session3.getUpdatedAt());
		assertNotNull(session3.getExpiresAt());
		assertNull(session3.getExpiredAt());
	}

	@Test
	public final void testUnauthorizedByAuthorization() throws Exception {

		final String authorization = random20();

		// FileUtils.writeStringToFile(superadminAuthorizationFile, authorization,
		// UTF_8);

		final User user = authService.getAuthenticatedUser(authorization, null);

		assertNull(user);
	}

	@Test
	public final void testTerminateMySession() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		final int sessionCount0 = authService.getUserSessions(correlationId, defaultUser(), null).getTotal();

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.REGULAR) //
				.setEnabled(true));

		assertEquals(sessionCount0, authService.getUserSessions(correlationId, defaultUser(), null).getTotal());

		final UserSession session0 = authService.authenticate(correlationId, username, password);

		assertEquals(session0.getCreatedAt(), session0.getUpdatedAt());
		assertNotNull(session0.getExpiresAt());
		assertNull(session0.getExpiredAt());

		final UserSessions sessions1 = authService.getUserSessions(correlationId, defaultUser(), null);

		assertEquals(sessionCount0 + 1, sessions1.getTotal());

		final String userSessionId = session0.getUserSessionId();

		assertEquals(userSessionId, sessions1.getResults()[0].getUserSessionId());

		final User user1 = authService.getAuthenticatedUser(null, userSessionId);

		assertNotNull(user1);

		dummyClock.now(); // Advance by 60 sec = 1 min.

		authService.terminateMySession(correlationId, user1, userSessionId);

		final User user2 = authService.getAuthenticatedUser(null, userSessionId);

		assertNull(user2);

		final UserSession session2 = authService.getUserSession(correlationId, defaultUser(), userSessionId);

		assertEquals(session0.getCreatedAt(), session2.getCreatedAt());
		assertNotEquals(session2.getCreatedAt(), session2.getUpdatedAt());
		assertNotNull(session2.getExpiresAt());
		assertNotNull(session2.getExpiredAt());
		assertEquals(session2.getUpdatedAt(), session2.getExpiredAt());
	}

	@Test
	public final void testTerminateUserSession() throws Exception {

		final String username = random40("USER-");
		final String password = random8();
		final String correlationId = setCorrelationId(newCorrelationId());

		usersService.createUser(correlationId, defaultUser(), username, instantiate(UserCreate.class) //
				.setPassword(password) //
				.setRole(Role.REGULAR) //
				.setEnabled(true));

		final UserSession session0 = authService.authenticate(correlationId, username, password);

		assertEquals(session0.getCreatedAt(), session0.getUpdatedAt());
		assertNotNull(session0.getExpiresAt());
		assertNull(session0.getExpiredAt());

		final String userSessionId = session0.getUserSessionId();

		final User user1 = authService.getAuthenticatedUser(null, userSessionId);

		assertNotNull(user1);

		dummyClock.now(); // Advance by 60 sec = 1 min.

		authService.terminateUserSession(correlationId, defaultUser(), userSessionId);

		final User user2 = authService.getAuthenticatedUser(null, userSessionId);

		assertNull(user2);

		final UserSession session2 = authService.getUserSession(correlationId, defaultUser(), userSessionId);

		assertEquals(session0.getCreatedAt(), session2.getCreatedAt());
		assertNotEquals(session2.getCreatedAt(), session2.getUpdatedAt());
		assertNotNull(session2.getExpiresAt());
		assertNotNull(session2.getExpiredAt());
		assertNotEquals(session2.getUpdatedAt(), session2.getExpiredAt());
	}
}
