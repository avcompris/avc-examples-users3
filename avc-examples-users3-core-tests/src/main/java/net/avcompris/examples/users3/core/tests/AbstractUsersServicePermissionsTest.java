package net.avcompris.examples.users3.core.tests;

import static net.avcompris.commons3.core.tests.CoreTestUtils.defaultClock;
import static net.avcompris.commons3.core.tests.CoreTestUtils.newCorrelationId;
import static net.avcompris.commons3.core.tests.CoreTestUtils.random40;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.ensureAnyUser;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.setCorrelationId;
import static net.avcompris.examples.users3.core.tests.UsersCoreTestUtils.setUsersService;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.UnauthorizedException;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.core.tests.AbstractServiceTest;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.examples.users3.core.impl.UsersServiceImpl;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;

public abstract class AbstractUsersServicePermissionsTest extends AbstractServiceTest<Pair<UsersDao, AuthDao>> {

	protected UsersService usersService;

	private String correlationId;

	@BeforeEach
	public final void setUpBeans() throws Exception {

		final Pair<UsersDao, AuthDao> beans = getBeans(defaultClock());

		final UsersDao usersDao = beans.getLeft();
		final AuthDao authDao = beans.getRight();

		final Permissions permissions = new PermissionsImpl();

		usersService = new UsersServiceImpl(permissions, defaultClock(), usersDao, authDao);

		setUsersService(usersService);

		LogFactory.resetCorrelationId();

		correlationId = setCorrelationId(newCorrelationId());
	}

	@Test
	public final void test_Usermgr_can_createUsermgrUser() throws Exception {

		final User user0 = ensureAnyUser(Role.USERMGR);

		usersService.createUser(correlationId, user0, random40("USER-"), instantiate(UserCreate.class) //
				.setEnabled(true) //
				.setRole(Role.USERMGR));
	}

	@Test
	public final void test_Admin_can_createAdminUser() throws Exception {

		final User user0 = ensureAnyUser(Role.ADMIN);

		usersService.createUser(correlationId, user0, random40("USER-"), instantiate(UserCreate.class) //
				.setEnabled(true) //
				.setRole(Role.ADMIN));
	}

	@Test
	public final void test_Admin_can_createUsermgrUser() throws Exception {

		final User user0 = ensureAnyUser(Role.ADMIN);

		usersService.createUser(correlationId, user0, random40("USER-"), instantiate(UserCreate.class) //
				.setEnabled(true) //
				.setRole(Role.USERMGR));
	}

	@Test
	public final void test_Usermgr_cannot_createAdminUser() throws Exception {

		final User user0 = ensureAnyUser(Role.USERMGR);

		assertThrows(UnauthorizedException.class, ()

		-> usersService.createUser(correlationId, user0, random40("USER-"), instantiate(UserCreate.class) //
				.setEnabled(true) //
				.setRole(Role.ADMIN)));
	}
}
