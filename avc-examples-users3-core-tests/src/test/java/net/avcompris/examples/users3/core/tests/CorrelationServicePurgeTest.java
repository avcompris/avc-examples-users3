package net.avcompris.examples.users3.core.tests;

import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.commons3.dao.impl.CorrelationDaoInMemory;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.core.tests.AbstractCorrelationServicePurgeTest;

public class CorrelationServicePurgeTest extends AbstractCorrelationServicePurgeTest {

	@Override
	protected CorrelationDao getBeans(final Clock clock) throws Exception {

		return new CorrelationDaoInMemory(clock);
	}
}
