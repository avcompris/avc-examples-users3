package net.avcompris.examples.users3.core.tests;

import org.apache.commons.lang3.tuple.Pair;

import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.core.tests.AbstractAuthServiceTest;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.impl.AuthDaoInMemory;
import net.avcompris.examples.users3.dao.impl.UsersDaoInMemory;

public class AuthServiceTest extends AbstractAuthServiceTest {

	@Override
	protected Pair<UsersDao, AuthDao> getBeans(final Clock clock) throws Exception {

		return Pair.of( //
				new UsersDaoInMemory(clock), //
				new AuthDaoInMemory(clock));
	}
}
