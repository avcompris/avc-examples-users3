package net.avcompris.examples.users3.core.tests;

import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.provider.Arguments;

import net.avcompris.commons3.api.tests.ApiTestsUtils;
import net.avcompris.commons3.api.tests.TestSpecParametersExtension;
import net.avcompris.commons3.api.tests.TestsSpec.TestSpec;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.impl.AuthDaoInMemory;
import net.avcompris.examples.users3.dao.impl.UsersDaoInMemory;

@ExtendWith(TestSpecParametersExtension.class)
public class UsersServiceApiTests extends AbstractUsersServiceApiTest {

	public UsersServiceApiTests(final TestSpec spec) {

		super(spec);
	}

	@Override
	protected Pair<UsersDao, AuthDao> getBeans(final Clock clock) throws Exception {

		final AuthDao authDao = new AuthDaoInMemory(clock);
		final UsersDao usersDao = new UsersDaoInMemory(clock);

		return Pair.of(usersDao, authDao);
	}

	public static Stream<Arguments> testSpecs() throws Exception {

		return ApiTestsUtils.testSpecs( //
				UserInfo.class, //
				UsersInfo.class);
	}
}
