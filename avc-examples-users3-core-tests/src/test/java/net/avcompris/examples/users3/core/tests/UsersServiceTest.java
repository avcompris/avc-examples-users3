package net.avcompris.examples.users3.core.tests;

import org.apache.commons.lang3.tuple.Pair;

import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.core.tests.AbstractUsersServiceTest;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.impl.AuthDaoInMemory;
import net.avcompris.examples.users3.dao.impl.UsersDaoInMemory;

public class UsersServiceTest extends AbstractUsersServiceTest {

	@Override
	protected Pair<UsersDao, AuthDao> getBeans(final Clock clock) throws Exception {

		final AuthDao authDao = new AuthDaoInMemory(clock);
		final UsersDao usersDao = new UsersDaoInMemory(clock);

		return Pair.of(usersDao, authDao);
	}
}
