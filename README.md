# avc-examples-users3

An example project that shows how to use the
`avc-commons3` framework to build a simple REST API.

## Full Environment

* This project relies on 
  [avc-commons3](https://gitlab.com/avcompris/avc-commons3);
* Maven Artifacts are sent to the
  [https://oss.sonatype.org/](https://oss.sonatype.org/)
  repository, that hosts Open Source artifacts;
* This GitLab project creates a Docker image,
  `avcompris/avc-examples-users3`, 
  which is then stored in the GitLab Registry;
* Two other GitLab projects are used in order to
  test this project:
    * [avc-examples-users-3core-RDS-it](https://gitlab.com/avcompris/avc-examples-users3-core-RDS-it),
      that checks the usage of the DataBase
    * [avc-examples-users3-web-it](https://gitlab.com/avcompris/avc-examples-users3-web-it),
      that checks the full API through HTTP.

![](doc/diagrams/images/avc-examples-users3.png)

