package net.avcompris.examples.users3.query;

import org.joda.time.DateTime;

import net.avcompris.commons.query.Filterings;
import net.avcompris.examples.shared3.Role;

public interface UserFilterings extends Filterings<UserFiltering, UserFiltering.Field> {

	// ======== STRING COMPARISONS

	@Override
	UserFiltering eq(UserFiltering.Field field, String s);

	@Override
	UserFiltering neq(UserFiltering.Field field, String s);

	@Override
	UserFiltering contains(UserFiltering.Field field, String s);

	@Override
	UserFiltering doesntContain(UserFiltering.Field field, String s);

	// ======== ROLE COMPARISONS

	UserFiltering eq(UserFiltering.Field field, Role role);

	UserFiltering neq(UserFiltering.Field field, Role role);

	// ======== INT COMPARISONS

	@Override
	UserFiltering eq(UserFiltering.Field field, int n);

	@Override
	UserFiltering neq(UserFiltering.Field field, int n);

	@Override
	UserFiltering gt(UserFiltering.Field field, int n);

	@Override
	UserFiltering gte(UserFiltering.Field field, int n);

	@Override
	UserFiltering lt(UserFiltering.Field field, int n);

	@Override
	UserFiltering lte(UserFiltering.Field field, int n);

	// ======== BOOLEAN COMPARISONS

	@Override
	UserFiltering eq(UserFiltering.Field field, boolean b);

	@Override
	UserFiltering neq(UserFiltering.Field field, boolean b);

	// ======== DATETIME COMPARISONS

	@Override
	UserFiltering eq(UserFiltering.Field field, DateTime dateTime);

	@Override
	UserFiltering gt(UserFiltering.Field field, DateTime dateTime);

	@Override
	UserFiltering gte(UserFiltering.Field field, DateTime dateTime);

	@Override
	UserFiltering lt(UserFiltering.Field field, DateTime dateTime);

	@Override
	UserFiltering lte(UserFiltering.Field field, DateTime dateTime);
}
