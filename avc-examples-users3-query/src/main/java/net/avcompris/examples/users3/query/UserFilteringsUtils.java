package net.avcompris.examples.users3.query;

import static net.avcompris.commons.query.impl.FilteringsFactory.instantiate;

import org.joda.time.DateTime;

import net.avcompris.commons.query.FilterSyntaxException;
import net.avcompris.commons.query.impl.SqlWhereClause;
import net.avcompris.examples.shared3.Role;

public abstract class UserFilteringsUtils {

	// ======== PARSE

	public static UserFiltering parse(final String expression) throws FilterSyntaxException {

		return instantiate(UserFilterings.class).parse(expression);
	}

	public static SqlWhereClause buildSqlWhereClause(final String expression) throws FilterSyntaxException {

		return SqlWhereClause.build( //
				instantiate(UserFilterings.class).parse(expression), //
				UserFiltering.Field.class);
	}

	// ======== STRING COMPARISONS

	public static UserFiltering eq(final UserFiltering.Field field, final String s) {

		return instantiate(UserFilterings.class).eq(field, s);
	}

	public static UserFiltering neq(final UserFiltering.Field field, final String s) {

		return instantiate(UserFilterings.class).neq(field, s);
	}

	public static UserFiltering contains(final UserFiltering.Field field, final String s) {

		return instantiate(UserFilterings.class).contains(field, s);
	}

	public static UserFiltering doesntContain(final UserFiltering.Field field, final String s) {

		return instantiate(UserFilterings.class).doesntContain(field, s);
	}

	// ======== ROLE COMPARISONS

	public static UserFiltering eq(final UserFiltering.Field field, final Role role) {

		return instantiate(UserFilterings.class).eq(field, role);
	}

	public static UserFiltering neq(final UserFiltering.Field field, final Role role) {

		return instantiate(UserFilterings.class).neq(field, role);
	}

	// ======== INT COMPARISONS

	public static UserFiltering eq(final UserFiltering.Field field, final int n) {

		return instantiate(UserFilterings.class).eq(field, n);
	}

	public static UserFiltering neq(final UserFiltering.Field field, final int n) {

		return instantiate(UserFilterings.class).neq(field, n);
	}

	public static UserFiltering gt(final UserFiltering.Field field, final int n) {

		return instantiate(UserFilterings.class).gt(field, n);
	}

	public static UserFiltering gte(final UserFiltering.Field field, final int n) {

		return instantiate(UserFilterings.class).gte(field, n);
	}

	public static UserFiltering lt(final UserFiltering.Field field, final int n) {

		return instantiate(UserFilterings.class).lt(field, n);
	}

	public static UserFiltering lte(final UserFiltering.Field field, final int n) {

		return instantiate(UserFilterings.class).lte(field, n);
	}

	// ======== BOOLEAN COMPARISONS

	public static UserFiltering eq(final UserFiltering.Field field, final boolean b) {

		return instantiate(UserFilterings.class).eq(field, b);
	}

	public static UserFiltering neq(final UserFiltering.Field field, final boolean b) {

		return instantiate(UserFilterings.class).neq(field, b);
	}

	// ======== DATETIME COMPARISONS

	public static UserFiltering eq(final UserFiltering.Field field, final DateTime dateTime) {

		return instantiate(UserFilterings.class).eq(field, dateTime);
	}

	public static UserFiltering gt(final UserFiltering.Field field, final DateTime dateTime) {

		return instantiate(UserFilterings.class).gt(field, dateTime);
	}

	public static UserFiltering gte(final UserFiltering.Field field, final DateTime dateTime) {

		return instantiate(UserFilterings.class).gte(field, dateTime);
	}

	public static UserFiltering lt(final UserFiltering.Field field, final DateTime dateTime) {

		return instantiate(UserFilterings.class).lt(field, dateTime);
	}

	public static UserFiltering lte(final UserFiltering.Field field, final DateTime dateTime) {

		return instantiate(UserFilterings.class).lte(field, dateTime);
	}
}
