package net.avcompris.examples.users3.query;

import org.joda.time.DateTime;

import net.avcompris.commons.query.Filtering;
import net.avcompris.examples.shared3.Role;

public interface UserFiltering extends Filtering<UserFiltering.Field> {

	enum Field implements Filtering.Field {

		@Spec(type = String.class, alias = "user")
		USERNAME,

		@Spec(type = Role.class, propertyName = "rolename", sqlName = "rolename")
		ROLE,

		@Spec(type = boolean.class)
		ENABLED,

		@Spec(type = DateTime.class)
		CREATED_AT,

		@Spec(type = DateTime.class)
		UPDATED_AT,

		@Spec(type = int.class)
		REVISION,

		@Spec(type = DateTime.class)
		LAST_ACTIVE_AT,
	}
}
