package net.avcompris.examples.users3.query;

import static net.avcompris.commons.query.impl.FilteringsFactory.and;
import static net.avcompris.commons.query.impl.FilteringsFactory.not;
import static net.avcompris.commons.query.impl.FilteringsFactory.or;
import static net.avcompris.examples.users3.query.UserFiltering.Field.CREATED_AT;
import static net.avcompris.examples.users3.query.UserFiltering.Field.ROLE;
import static net.avcompris.examples.users3.query.UserFiltering.Field.USERNAME;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.contains;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.eq;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.gt;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.neq;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.parse;
import static org.joda.time.DateTimeZone.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import net.avcompris.commons.query.FilterSyntaxException;
import net.avcompris.commons.query.Filtering;
import net.avcompris.examples.shared3.Role;

public class UsersFilteringTest {

	@Test
	public void testParse_createdAt_gt_xxx() throws Exception {

		final DateTime ref = new DateTime().withZone(UTC);

		assertEquals(gt(CREATED_AT, ref), parse("createdAt gt " + ref));
	}

	@Test
	public void testParse_created_at_gt_xxx() throws Exception {

		final DateTime ref = new DateTime().withZone(UTC);

		assertEquals(gt(CREATED_AT, ref), parse("created_at gt " + ref));
	}

	@Test
	public void testParse_created_at_gt_xxx_multispaces() throws Exception {

		final DateTime ref = new DateTime().withZone(UTC);

		assertEquals(gt(CREATED_AT, ref), parse("  created_at   gt   " + ref + "  "));
	}

	@Test
	public void testParse_created_at_gt2_xxx() throws Exception {

		final DateTime ref = new DateTime().withZone(UTC);

		assertEquals(gt(CREATED_AT, ref), parse("created_at > " + ref));
	}

	@Test
	public void testParse_created_at_gt2_xxx_quotes() throws Exception {

		final DateTime ref = new DateTime().withZone(UTC);

		assertEquals(gt(CREATED_AT, ref), parse("created_at > \"" + ref + "\""));
	}

	@Test
	public void testParse_created_at_gt2_xxx_simplequotes() throws Exception {

		final DateTime ref = new DateTime().withZone(UTC);

		assertEquals(gt(CREATED_AT, ref), parse("created_at > '" + ref + "'"));
	}

	@Test
	public void testParse_username_eq_xxx() throws Exception {

		assertEquals(eq(USERNAME, "toto"), parse("username eq toto"));
	}

	@Test
	public void testParse_username_eq_xxx_quotes() throws Exception {

		assertEquals(eq(USERNAME, "toto"), parse("username eq \"toto\""));
	}

	@Test
	public void testParse_user_eq3_xxx_quotes() throws Exception {

		assertEquals(eq(USERNAME, "toto"), parse("user = \"toto\""));
	}

	@Test
	public void testParse_username_eq2_xxx() throws Exception {

		assertEquals(eq(USERNAME, "toto"), parse("username == toto"));
	}

	@Test
	public void testParse_username_eq2_xxx_parenthesis() throws Exception {

		assertEquals(eq(USERNAME, "toto"), parse("(username == toto)"));
	}

	@Test
	public void testParse_username_eq2_xxx_embeddedParenthesis() throws Exception {

		assertEquals(eq(USERNAME, "to(to"), parse("(username == \"to(to\")"));
	}

	@Test
	public void testParse_username_eq2_xxx_embeddedQuotes() throws Exception {

		assertEquals(eq(USERNAME, "to\"to"), parse("(username == \"to\\\"to\")"));
	}

	@Test
	public void testParse_username_eq2_xxx_and_created_at_eq_xxx() throws Exception {

		assertEquals(and( //
				eq(USERNAME, "toto"), //
				eq(CREATED_AT, "2010-03-04")),

				parse("username == toto and created_at eq 2010-03-04"));
	}

	@Test
	public void testParse_username_eq2_xxx_and2_created_at_eq_xxx_and_role_eq_xxx() throws Exception {

		assertEquals(and( //
				eq(USERNAME, "toto"), //
				eq(CREATED_AT, "2010-03-04"), //
				eq(ROLE, Role.ADMIN)),

				parse("username == toto && created_at eq 2010-03-04 and role == ADMIN"));
	}

	@Test
	public void testParse_username_neq2_xxx_and_createdAt_e_xxx_parenthesis() throws Exception {

		assertEquals(and( //
				neq(USERNAME, "toto"), //
				eq(CREATED_AT, "2010-03-04")),

				parse("(username != toto) and (createdAt = '2010-03-04')"));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithMinutes() throws Exception {

		assertEquals(eq(CREATED_AT, // new DateTime(2010, 03, 04, 12, 34, 00, UTC)),
				"2010-03-04T12:34"),

				parse("createdAt = 2010-03-04T12:34"));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithSeconds() throws Exception {

		assertEquals(eq(CREATED_AT, // new DateTime(2010, 03, 04, 12, 34, 56, UTC)),
				"2010-03-04T12:34:56"),

				parse("createdAt = 2010-03-04T12:34:56"));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithMilliseconds() throws Exception {

		assertEquals(eq(CREATED_AT, new DateTime(2010, 03, 04, 12, 34, 56, 431, UTC)),

				parse("createdAt = 2010-03-04T12:34:56.431"));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithMillisecondsZone() throws Exception {

		assertEquals(eq(CREATED_AT, new DateTime(2010, 03, 04, 12, 34, 56, 123, UTC)),

				parse("createdAt = 2010-03-04T12:34:56.123Z"));
	}

	@Test
	public void testNotEquals_and_or() throws Exception {

		assertNotEquals( //
				or( //
						neq(USERNAME, "toto"), //
						eq(CREATED_AT, "2019-03-04")),

				and( //
						neq(USERNAME, "toto"), //
						eq(CREATED_AT, "2019-03-04")));

		assertEquals( //
				or( //
						neq(USERNAME, "toto"), //
						eq(CREATED_AT, "2019-03-04")),

				or( //
						neq(USERNAME, "toto"), //
						eq(CREATED_AT, "2019-03-04")));

		assertEquals( //
				and( //
						neq(USERNAME, "toto"), //
						eq(CREATED_AT, "2019-03-04")),

				and( //
						neq(USERNAME, "toto"), //
						eq(CREATED_AT, "2019-03-04")));
	}

	@Test
	public void testParse_username_EQ_xxx_and_createdAt_NEQ_xxx_parenthesis() throws Exception {

		assertEquals(and( //
				eq(USERNAME, "toto"), //
				neq(CREATED_AT, "2010-03-04")),

				parse("(username EQ toto) and (createdAt NEQ \"2010-03-04\")"));
	}

	@Test
	public void testParse_username_eq3_and_and_role_eq2_or() throws Exception {

		assertEquals(and( //
				eq(USERNAME, "and"), //
				eq(ROLE, "or")),

				parse("username = and and role == or"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_1_2() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("(role = a) and (role = b and role = c)"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_0_2() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("role = a and (role = b and role = c)"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_2_0() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("(role = a and role = b) and role = c"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_left_open() throws Exception {

		assertThrows(FilterSyntaxException.class, ()

		-> parse("(role = a and role = b) and role = )"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_2_1() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("(role = a and role = b) and (role = c)"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_3() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("(role = a and role = b and role = c)"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_1_1_1() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("(role = a) and (role = b) and (role = c)"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_0_1_0() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("role = a and (role = b) and role = c"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("(role = a) and (role = b and role = c)"));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_0_2_0() throws Exception {

		assertEquals(and( //
				eq(ROLE, "a"), //
				eq(ROLE, "b"), //
				eq(ROLE, "c")),

				parse("role = a and ((role = b) and role = c)"));
	}

	@Test
	public void testParse_role_a_or_role_b() throws Exception {

		assertEquals(or( //
				eq(ROLE, "a"), //
				eq(ROLE, "b")),

				parse("role = a or role = b"));
	}

	@Test
	public void testParse_role_a_or_role_b_and_role_c_and_role_d_precedence() throws Exception {

		final Filtering<UserFiltering.Field> f = parse("role == a || role == b && role == c && role == d");

		assertEquals(or( //
				eq(ROLE, "a"), //
				and( //
						eq(ROLE, "b"), //
						eq(ROLE, "c"), //
						eq(ROLE, "d"))),

				f);

		// assertEquals("(ROLE EQ a) OR ((ROLE EQ b) AND (ROLE EQ c) AND (ROLE EQ d))",
		// f.toString());

		assertEquals("(role eq \"a\" or (role eq \"b\" and role eq \"c\" and role eq \"d\"))", f.toString());
	}

	@Test
	public void testParse_role_a_or_role_b_and_role_c_and_role_d_par_2_0_0() throws Exception {

		final Filtering<UserFiltering.Field> f = parse("(role == a || role == b) && role == c && role == d");

		assertEquals(and( //
				or( //
						eq(ROLE, "a"), //
						eq(ROLE, "b")), //
				eq(ROLE, "c"), //
				eq(ROLE, "d")),

				f);

		// assertEquals("((ROLE EQ a) OR (ROLE EQ b)) AND (ROLE EQ c) AND (ROLE EQ d)",
		// f.toString());

		assertEquals("((role eq \"a\" or role eq \"b\") and role eq \"c\" and role eq \"d\")", f.toString());
	}

	@Test
	public void testParse_username_EQ3_xxx_noSpace() throws Exception {

		assertEquals(eq(USERNAME, "toto"),

				parse("username=toto"));
	}

	@Test
	public void testParse_username_EQ2_xxx_noSpace() throws Exception {

		assertEquals(eq(USERNAME, "toto"),

				parse("username==toto"));
	}

	@Test
	public void testParse_date_alone() throws Exception {

		assertThrows(FilterSyntaxException.class, ()

		-> parse("2019-12-35"));
	}

	@Test
	public void testParse_createdDate_eq_illegalDate() throws Exception {

		// System.out.println(buildSqlWhereClause("createdAt=2019-12-35").getSQL(""));

		assertThrows(FilterSyntaxException.class, ()

		-> parse("createdAt=2019-12-35"));
	}

	@Test
	public void testParse_wrongFieldName_eq_xxx() throws Exception {

		assertThrows(FilterSyntaxException.class, ()

		-> parse("createdA=2019-12-30"));
	}

	@Test
	public void testParse_username_contains_xxx() throws Exception {

		assertEquals(contains(USERNAME, "toto"),

				parse("username contains toto"));
	}

	@Test
	public void testParse_username_contains_xxx_UPPERCASE() throws Exception {

		assertEquals(contains(USERNAME, "wiQDLLgVn3p2Hsi1PfPJ"),

				parse("USERNAME CONTAINS wiQDLLgVn3p2Hsi1PfPJ"));
	}

	@Test
	public void testParse_username_eq3_null() throws Exception {

		assertEquals(eq(USERNAME, (String) null),

				parse("username == null"));
	}

	@Test
	public void testEquals_eq_null_eq_null() throws Exception {

		assertEquals(eq(USERNAME, (String) null), eq(USERNAME, (String) null));
	}

	@Test
	public void testNotEquals_eq_null_eq_notNull() throws Exception {

		assertNotEquals(eq(USERNAME, (String) null), eq(USERNAME, "coco"));
	}

	@Test
	public void testNotEquals_eq_notNull_eq_null() throws Exception {

		assertNotEquals(eq(USERNAME, "coco"), eq(USERNAME, (String) null));
	}

	@Test
	public void testEquals_eq_notNull_eq_notNull() throws Exception {

		assertEquals(eq(USERNAME, "coco"), eq(USERNAME, "coco"));
	}

	@Test
	public void testNotEquals_eq_notNull_eq_notNull() throws Exception {

		assertNotEquals(eq(USERNAME, "coco"), eq(USERNAME, "tata"));
	}

	@Test
	public void testParse_username_eq3_null_quotes() throws Exception {

		assertEquals(eq(USERNAME, "null"),

				parse("username == \"null\""));
	}

	@Test
	public void testParse_username_neq2_null() throws Exception {

		assertEquals(neq(USERNAME, (String) null),

				parse("username != null"));
	}

	@Test
	public void testParse_username_neq2_null_quotes() throws Exception {

		assertEquals(neq(USERNAME, "null"),

				parse("username != \"null\""));
	}

	@Test
	public void testParse_not_role_eq_xxx() throws Exception {

		assertEquals(not(eq(ROLE, Role.ADMIN)),

				parse("not(role eq ADMIN)"));
	}

	@Test
	public void testParse_not2_role_eq_xxx() throws Exception {

		assertEquals(not(eq(ROLE, Role.ADMIN)),

				parse("!(role eq ADMIN)"));
	}

	@Test
	public void testParse_not_role_eq_xxx_and_role_neq_xxx() throws Exception {

		assertEquals(and( //
				not(eq(ROLE, Role.ADMIN)), //
				neq(ROLE, Role.SILENT_WORKER)),

				parse("not(role eq ADMIN) and role neq SILENT_WORKER"));
	}

	@Test
	public void testParse_not2_role_eq_xxx_and_role_neq_xxx() throws Exception {

		assertEquals(and( //
				not(eq(ROLE, Role.ADMIN)), //
				neq(ROLE, Role.SILENT_WORKER)),

				parse("!(role eq ADMIN) && role != SILENT_WORKER"));
	}

	@Test
	public void testParse_not2_role_eq_xxx_and_role_neq_xxx_par() throws Exception {

		assertEquals(not(and( //
				eq(ROLE, Role.ADMIN), //
				neq(ROLE, Role.SILENT_WORKER))),

				parse("!(role eq ADMIN && role != SILENT_WORKER)"));
	}

	@Test
	public void testParse_role_eq_xxx_and_not_role_neq_xxx() throws Exception {

		assertEquals(and( //
				eq(ROLE, Role.ADMIN), //
				not(neq(ROLE, Role.SILENT_WORKER))),

				parse("role eq ADMIN && !(role != SILENT_WORKER)"));
	}

	@Test
	public void testParse_role_eq_xxx_and_not_role_neq_xxx_tooManyParenthesis() throws Exception {

		assertThrows(FilterSyntaxException.class, ()

		-> parse("role eq ADMIN && !(role != SILENT_WORKER))"));
	}
}
