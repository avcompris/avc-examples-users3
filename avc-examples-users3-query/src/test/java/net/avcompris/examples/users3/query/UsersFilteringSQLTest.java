package net.avcompris.examples.users3.query;

import static net.avcompris.commons.query.impl.FilteringsFactory.or;
import static net.avcompris.examples.users3.query.UserFiltering.Field.LAST_ACTIVE_AT;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.buildSqlWhereClause;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.eq;
import static net.avcompris.examples.users3.query.UserFilteringsUtils.neq;
import static org.joda.time.DateTimeZone.UTC;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.joda.time.DateTime;
import org.junit.jupiter.api.Test;

import net.avcompris.commons.query.FilterSyntaxException;
import net.avcompris.commons.query.impl.SqlWhereClause;

public class UsersFilteringSQLTest {

	@Test
	public void testParse_createdAt_gt_xxx() throws Exception {

		final DateTime ref = new DateTime(2019, 3, 4, 5, 2, 0, UTC);

		assertEquals(
				"DATE_TRUNC('milliseconds', created_at) > TO_TIMESTAMP('2019-03-04 05:02:00.000', 'YYYY-MM-DD HH24:MI:SS.MS')",

				buildSqlWhereClause("createdAt gt " + ref).getSQL(""));
	}

	@Test
	public void testParse_username_eq_xxx() throws Exception {

		assertEquals("username = 'toto'",

				buildSqlWhereClause("username eq toto").getSQL(""));
	}

	@Test
	public void testParse_customerId_eq_xxx() throws Exception {

		assertThrows(FilterSyntaxException.class, ()

		-> buildSqlWhereClause("customerId eq toto").getSQL(""));
	}

	@Test
	public void testParse_username_eq_xxx_withDoubleQuotes() throws Exception {

		assertEquals("username = 'to\"to'",

				buildSqlWhereClause("username eq \"to\\\"to\"").getSQL(""));
	}

	@Test
	public void testParse_username_eq_xxx_withSimpleQuote() throws Exception {

		assertEquals("username = 'to''to'",

				buildSqlWhereClause("username eq \"to'to\"").getSQL(""));
	}

	@Test
	public void testParse_username_eq2_xxx_embeddedParenthesis() throws Exception {

		assertEquals("username = 'to(to'",

				buildSqlWhereClause("(username == \"to(to\")").getSQL(""));
	}

	@Test
	public void testParse_username_eq2_xxx_embeddedQuotes() throws Exception {

		assertEquals("username = 'to\"to'",

				buildSqlWhereClause("(username == \"to\\\"to\")").getSQL(""));
	}

	@Test
	public void testParse_username_eq2_xxx_and_created_at_eq_xxx() throws Exception {

		assertEquals("(username = 'toto')" //
				+ " AND (DATE_TRUNC('day', created_at) = TO_TIMESTAMP('2010-03-04', 'YYYY-MM-DD'))",

				buildSqlWhereClause("username == toto and created_at eq 2010-03-04").getSQL(""));
	}

	@Test
	public void testParse_username_eq2_xxx_and2_created_at_eq_xxx_and_role_eq_xxx() throws Exception {

		assertEquals("(username = 'toto')" //
				+ " AND (DATE_TRUNC('day', created_at) = TO_TIMESTAMP('2010-03-04', 'YYYY-MM-DD'))" //
				+ " AND (rolename = 'ADMIN')",

				buildSqlWhereClause("username == toto && created_at eq 2010-03-04 and role == ADMIN").getSQL(""));
	}

	@Test
	public void testParse_username_neq2_xxx_and_createdAt_e_xxx_parenthesis() throws Exception {

		assertEquals("(username != 'toto')" //
				+ " AND (DATE_TRUNC('day', created_at) = TO_TIMESTAMP('2010-03-04', 'YYYY-MM-DD'))",

				buildSqlWhereClause("(username != toto) and (createdAt = '2010-03-04')").getSQL(""));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithMinutes() throws Exception {

		assertEquals("DATE_TRUNC('minute', created_at) = TO_TIMESTAMP('2010-03-04 12:34', 'YYYY-MM-DD HH24:MI')",

				buildSqlWhereClause("createdAt = 2010-03-04T12:34").getSQL(""));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithSeconds() throws Exception {

		assertEquals("DATE_TRUNC('second', created_at) = TO_TIMESTAMP('2010-03-04 12:34:56', 'YYYY-MM-DD HH24:MI:SS')",

				buildSqlWhereClause("createdAt = 2010-03-04T12:34:56").getSQL(""));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithMilliseconds() throws Exception {

		assertEquals(
				"DATE_TRUNC('milliseconds', created_at) = TO_TIMESTAMP('2010-03-04 12:34:56.431', 'YYYY-MM-DD HH24:MI:SS.MS')",

				buildSqlWhereClause("createdAt = 2010-03-04T12:34:56.431").getSQL(""));
	}

	@Test
	public void testParse_createdAt_eq_dateTimeWithMillisecondsZone() throws Exception {

		assertEquals(
				"DATE_TRUNC('milliseconds', created_at) = TO_TIMESTAMP('2010-03-04 12:34:56.123', 'YYYY-MM-DD HH24:MI:SS.MS')",

				buildSqlWhereClause("createdAt = 2010-03-04T12:34:56.123Z").getSQL(""));
	}

	@Test
	public void testParse_lastActiveAt_IS_NULL() throws Exception {

		assertEquals("last_active_at IS NULL",

				buildSqlWhereClause("lastActiveAt = null").getSQL(""));
	}

	@Test
	public void testParse_lastActiveAt_IS_NULL_or_IS_NOT_NULL() throws Exception {

//		assertEquals("(last_active_at IS NULL) OR (last_active_at IS NOT NULL)",
		assertEquals("(last_active_at IS NULL) OR (last_active_at IS NOT NULL)",

				buildSqlWhereClause("lastActiveAt = null or lastActiveAt != null").getSQL(""));
	}

	@Test
	public void testParse_lastActiveAt_IS_NULL_or_IS_NOT_NULL_eq() throws Exception {

		assertEquals("(last_active_at IS NULL) OR (last_active_at IS NOT NULL)",

				SqlWhereClause.build(

						or(eq(LAST_ACTIVE_AT, (String) null), neq(LAST_ACTIVE_AT, (String) null)),

						UserFiltering.Field.class).getSQL(""));
	}

	@Test
	public void testParse_username_EQ_xxx_and_createdAt_NEQ_xxx_parenthesis() throws Exception {

		assertEquals("(username = 'toto')"//
				+ " AND (DATE_TRUNC('day', created_at) != TO_TIMESTAMP('2010-03-04', 'YYYY-MM-DD'))",

				buildSqlWhereClause("(username EQ toto) and (createdAt NEQ \"2010-03-04\")").getSQL(""));
	}

	@Test
	public void testParse_username_eq3_and_and_role_eq2_or() throws Exception {

		assertEquals("(username = 'and')"//
				+ " AND (rolename = 'or')",

				buildSqlWhereClause("username = and and role == or").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_1_2() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("(role = a) and (role = b and role = c)").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_0_2() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("role = a and (role = b and role = c)").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_2_0() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("(role = a and role = b) and role = c").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_2_1() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("(role = a and role = b) and (role = c)").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_3() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("(role = a and role = b and role = c)").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_1_1_1() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("(role = a) and (role = b) and (role = c)").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_01_0() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("role = a and (role = b) and role = c").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("(role = a) and (role = b and role = c)").getSQL(""));
	}

	@Test
	public void testParse_role_a_and_role_b_and_role_c_parenthesis_0_210() throws Exception {

		assertEquals("(rolename = 'a') AND (rolename = 'b') AND (rolename = 'c')",

				buildSqlWhereClause("role = a and ((role = b) and role = c)").getSQL(""));
	}

	@Test
	public void testParse_role_a_or_role_b() throws Exception {

		assertEquals("(rolename = 'a') OR (rolename = 'b')",

				buildSqlWhereClause("role = a or role = b").getSQL(""));
	}

	@Test
	public void testParse_role_a_or_role_b_and_role_c_and_role_d_precedence() throws Exception {

		assertEquals("(rolename = 'a') OR ((rolename = 'b') AND (rolename = 'c') AND (rolename = 'd'))",

				buildSqlWhereClause("role == a || role == b && role == c && role == d").getSQL(""));
	}

	@Test
	public void testParse_role_a_or_role_b_and_role_c_and_role_d_par_2_0_0() throws Exception {

		assertEquals("((rolename = 'a') OR (rolename = 'b')) AND (rolename = 'c') AND (rolename = 'd')",

				buildSqlWhereClause("(role == a || role == b) && role == c && role == d").getSQL(""));
	}

	@Test
	public void testParse_username_EQ3_xxx_noSpace() throws Exception {

		assertEquals("username = 'toto'",

				buildSqlWhereClause("username=toto").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx() throws Exception {

		assertEquals("username LIKE '%toto%'",

				buildSqlWhereClause("username contains toto").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx_embeddedQuote() throws Exception {

		assertEquals("username LIKE '%to''to%'",

				buildSqlWhereClause("username contains \"to'to\"").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx_UPPERCASE() throws Exception {

		assertEquals("username LIKE '%wiQDLLgVn3p2Hsi1PfPJ%'",

				buildSqlWhereClause("USERNAME CONTAINS wiQDLLgVn3p2Hsi1PfPJ").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx_percent_start() throws Exception {

		assertEquals("username LIKE '%\\%m%'",

				buildSqlWhereClause("USERNAME CONTAINS %m").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx_percent_end() throws Exception {

		assertEquals("username LIKE '%m\\%%'",

				buildSqlWhereClause("USERNAME CONTAINS m%").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx_percent_middle() throws Exception {

		assertEquals("username LIKE '%n\\%m%'",

				buildSqlWhereClause("USERNAME CONTAINS n%m").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx_backslash() throws Exception {

		assertEquals("username LIKE '%n\\\\m%'",

				buildSqlWhereClause("USERNAME CONTAINS n\\m").getSQL(""));
	}

	@Test
	public void testParse_username_contains_xxx_doubleBackslash() throws Exception {

		assertEquals("username LIKE '%n\\\\\\\\m%'",

				buildSqlWhereClause("USERNAME CONTAINS n\\\\m").getSQL(""));
	}

	@Test
	public void testParse_username_eq3_null() throws Exception {

		assertEquals("username IS NULL",

				buildSqlWhereClause("username == null").getSQL(""));
	}

	@Test
	public void testParse_username_eq3_null_quotes() throws Exception {

		assertEquals("username = 'null'",

				buildSqlWhereClause("username == \"null\"").getSQL(""));
	}

	@Test
	public void testParse_username_neq2_null() throws Exception {

		assertEquals("username IS NOT NULL",

				buildSqlWhereClause("username != null").getSQL(""));
	}

	@Test
	public void testParse_username_neq2_null_quotes() throws Exception {

		assertEquals("username != 'null'",

				buildSqlWhereClause("username != \"null\"").getSQL(""));
	}

	@Test
	public void testParse_not_role_eq_xxx() throws Exception {

		assertEquals("NOT(rolename = 'ADMIN')",

				buildSqlWhereClause("not(role eq ADMIN)").getSQL(""));
	}

	@Test
	public void testParse_not2_role_eq_xxx() throws Exception {

		assertEquals("NOT(rolename = 'ADMIN')",

				buildSqlWhereClause("!(role eq ADMIN)").getSQL(""));
	}

	@Test
	public void testParse_not_role_eq_xxx_and_role_neq_xxx() throws Exception {

		assertEquals("(NOT(rolename = 'ADMIN')) AND (rolename != 'SUPERVISOR')",

				buildSqlWhereClause("not(role eq ADMIN) and role neq SUPERVISOR").getSQL(""));
	}

	@Test
	public void testParse_not2_role_eq_xxx_and_role_neq_xxx() throws Exception {

		assertEquals("(NOT(rolename = 'ADMIN')) AND (rolename != 'SUPERVISOR')",

				buildSqlWhereClause("!(role eq ADMIN) && role != SUPERVISOR").getSQL(""));
	}

	@Test
	public void testParse_not2_role_eq_xxx_and_role_neq_xxx_par() throws Exception {

		assertEquals("NOT((rolename = 'ADMIN') AND (rolename != 'SUPERVISOR'))",

				buildSqlWhereClause("!(role eq ADMIN && role != SUPERVISOR)").getSQL(""));
	}

	@Test
	public void testParse_role_eq_xxx_and_not_role_neq_xxx() throws Exception {

		assertEquals("(rolename = 'ADMIN') AND (NOT(rolename != 'SUPERVISOR'))",

				buildSqlWhereClause("role eq ADMIN && !(role != SUPERVISOR)").getSQL(""));
	}

	@Test
	public void testParse_role_eq_xxx_and_not_role_neq_xxx_tooManyParenthesis() throws Exception {

		assertThrows(FilterSyntaxException.class, ()

		-> buildSqlWhereClause("role eq ADMIN && !(role != SUPERVISOR))").getSQL(""));
	}
}
