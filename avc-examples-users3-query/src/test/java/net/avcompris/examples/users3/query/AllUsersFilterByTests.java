package net.avcompris.examples.users3.query;

import static java.util.Locale.ENGLISH;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.joda.time.DateTimeZone.UTC;

import java.util.stream.Stream;

import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;
import org.junit.jupiter.params.provider.Arguments;

import net.avcompris.commons.query.tests.AbstractAllFilterByTest;
import net.avcompris.commons3.dao.EntityDto;
import net.avcompris.examples.shared3.Role;

public class AllUsersFilterByTests extends AbstractAllFilterByTest<UserFiltering, UserFiltering.Field> {

	private static interface MyUser extends EntityDto {

		String getUsername();

		String getRolename();

		boolean isEnabled();

		DateTime getLastActiveAt();
	}

	public static Stream<Arguments> testCases() throws Exception {

		return testCases(UserFiltering.Field.values());
	}

	public AllUsersFilterByTests() {

		super(UserFilterings.class, MyUser.class);
	}

	@Override
	public Object newValueFor(final UserFiltering.Field field) {

		switch (field) {
		case CREATED_AT:
		case UPDATED_AT:
		case LAST_ACTIVE_AT:
			return new DateTime().withZone(UTC);
		case ROLE:
			return Role.REGULAR.name();
		case USERNAME:
			return (randomAlphabetic(1) + randomAlphanumeric(19)).toLowerCase(ENGLISH);
		case ENABLED:
			return true;
		case REVISION:
			return 1;
		default:
			throw new NotImplementedException("field: " + field);
		}
	}

//	@Override
//	public Object randomValueFor(final FilterBy filterBy) {
//
//		switch (filterBy) {
//		case CREATED_AT:
//		case UPDATED_AT:
//		case LAST_ACTIVE_AT:
//			return new DateTime();
//		case ROLE:
//			return Role.REGULAR;
//		case USERNAME:
//			return RandomStringUtils.randomAlphanumeric(20);
//		case ENABLED:
//			return RandomUtils.nextInt(0, 2) == 0;
//		case REVISION:
//			return RandomUtils.nextInt(1, 8);
//		default:
//			throw new NotImplementedException("filterBy: " + filterBy);
//		}
//	}
}
