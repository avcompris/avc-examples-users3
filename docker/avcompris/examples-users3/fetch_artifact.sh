#!/bin/sh

# File: avc-examples-users3/docker/avcompris/examples-users3/fetch_artifact.sh

set -e

ARTIFACT_ID=avc-examples-users3-web

VERSION=`grep \<version\> "../../../${ARTIFACT_ID}/pom.xml" | head -1 | awk -F "\>|\<" '{ print $3 }'`

VERSION_IN_DOCKERFILE=`grep VERSION Dockerfile | head -1 | awk '{ print $3 }'`

if [ "{$VERSION}" != "{$VERSION_IN_DOCKERFILE}" ]; then
	echo "<version> in pom.xml and VERSION in Dockerfile differ: ${VERSION} != {$VERSION_IN_DOCKERFILE}" >&2
	echo "Exiting." >&2
	exit 1
fi

JAR_FILE="../../../${ARTIFACT_ID}/target/${ARTIFACT_ID}-${VERSION}-exec.jar"

if [ ! -f "${JAR_FILE}" ]; then

	# Do not use Basic Auth info in the repo URL, since it is then dumped in
	# clear in the logs.
	#
	# Use maven-dependency-plugin:2.4 rather than 3.1.0, since 2.4 *knows*
	# how to use settings.xml/server information for authenticated
	# downloading.
	#
	mvn -U org.apache.maven.plugins:maven-dependency-plugin:2.4:get \
		-Dtransitive=false \
		-s ~/.m2/settings.xml \
		-DremoteRepositories=OSSRH::::https://oss.sonatype.org/content/repositories/snapshots \
		"-Dartifact=net.avcompris.commons:${ARTIFACT_ID}:${VERSION}" \
	|| exit 1

	JAR_FILE=~/.m2/repository/net/avcompris/commons/${ARTIFACT_ID}/${VERSION}/${ARTIFACT_ID}-${VERSION}-exec.jar

fi

cp "${JAR_FILE}" .
