#!/bin/bash

# File: avc-examples-users3/docker/avcompris/examples-users3/entrypoint.sh

set -e

preamble() {

	echo "--------------------------------------------------------------------------------"
	echo "image: avcompris/examples-users3"
	echo "--------------------------------------------------------------------------------"
	echo "buildinfo:"
	cat /buildinfo | sort
	echo "--------------------------------------------------------------------------------"
	echo "MANIFEST.MF:"
	cat /MANIFEST.MF | sort
	echo "--------------------------------------------------------------------------------"
	echo -n Date\ && date
}

preamble

# "ARTIFACT_ID" and "VERSION" are set as ENV in the Dockerfile.
#
ARCHIVE="${ARTIFACT_ID}-${VERSION}-exec.jar"

# 1. OPTS

OPTS="-Dserver.port=8080"

echo "SUPERADMIN_AUTHORIZATION_FILE: ${SUPERADMIN_AUTHORIZATION_FILE}"
echo "RDS_URL:                       ${RDS_URL}"
echo "RDS_USERNAME:                  ${RDS_USERNAME}"
echo "RDS_TABLENAME_PREFIX:          ${RDS_TABLENAME_PREFIX}"
echo "RABBITMQ_URL:                  ${RABBITMQ_URL}"
echo "RABBITMQ_QUEUE_ERROR:          ${RABBITMQ_QUEUE_ERROR}"
echo "DEBUG:                         ${DEBUG}"

if [ -n "${SUPERADMIN_AUTHORIZATION_FILE}" ]; then
	OPTS="${OPTS} -Dsuperadmin.authorizationFile=${SUPERADMIN_AUTHORIZATION_FILE}"
fi
if [ -n "${RDS_URL}" ]; then
	OPTS="${OPTS} -Drds.url=${RDS_URL}"
fi
if [ -n "${RDS_USERNAME}" ]; then
	OPTS="${OPTS} -Drds.username=${RDS_USERNAME}"
fi
if [ -n "${RDS_PASSWORD}" ]; then
	OPTS="${OPTS} -Drds.password=${RDS_PASSWORD}"
fi
if [ -n "${RDS_TABLENAME_PREFIX}" ]; then
	OPTS="${OPTS} -Drds.tableNamePrefix=${RDS_TABLENAME_PREFIX}"
fi
if [ -n "${RABBITMQ_URL}" ]; then
	OPTS="${OPTS} -Drabbitmq.url=${RABBITMQ_URL}"
fi
if [ -n "${RABBITMQ_QUEUE_ERROR}" ]; then
	OPTS="${OPTS} -Drabbitmq.queue.error=${RABBITMQ_QUEUE_ERROR}"
fi
if [ -n "${DEBUG}" ]; then
	OPTS="${OPTS} -Ddebug"
fi

# 2. RUN SPRING BOOT APPLICATION

java ${OPTS} -jar "${ARCHIVE}" "$@"
