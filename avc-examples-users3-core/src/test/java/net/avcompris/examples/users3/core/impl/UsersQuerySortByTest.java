package net.avcompris.examples.users3.core.impl;

import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_CREATED_AT;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_CREATED_AT_DESC;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_ENABLED;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_LAST_ACTIVE_AT;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_LAST_ACTIVE_AT_DESC;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_ROLE;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_UPDATED_AT;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_USERNAME;
import static net.avcompris.examples.users3.api.UsersQuery.SortBy.SORT_BY_USERNAME_DESC;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import net.avcompris.commons3.core.impl.SortBysParser;
import net.avcompris.examples.users3.api.UsersQuery.SortBy;

public class UsersQuerySortByTest {

	private static void checkParsed(final String s, final SortBy... sortBys) {

		final SortBy[] parsed = new SortBysParser<SortBy>(SortBy.class, SortBy.values()).parse(s);

		assertArrayEquals(sortBys, parsed);
	}

	@Test
	public void test() {

		checkParsed("+username", SORT_BY_USERNAME);
		checkParsed("username", SORT_BY_USERNAME);
		checkParsed("-username", SORT_BY_USERNAME_DESC);

		checkParsed("role, username", SORT_BY_ROLE, SORT_BY_USERNAME);

		checkParsed("createdAt, +updatedAt-lastActiveAt", SORT_BY_CREATED_AT, SORT_BY_UPDATED_AT,
				SORT_BY_LAST_ACTIVE_AT_DESC);

		checkParsed("-createdAt, +updatedAt", SORT_BY_CREATED_AT_DESC, SORT_BY_UPDATED_AT);

		checkParsed("-createdAt,updatedAt", SORT_BY_CREATED_AT_DESC, SORT_BY_UPDATED_AT);

		checkParsed("+enabled,lastactiveat ", SORT_BY_ENABLED, SORT_BY_LAST_ACTIVE_AT);
	}

	@Test
	public void testNulls() {

		assertNull(new SortBysParser<SortBy>(SortBy.class, SortBy.values()).parse(""));
		assertNull(new SortBysParser<SortBy>(SortBy.class, SortBy.values()).parse(null));
	}

	@Test
	public void testInvalid() {

		assertThrows(IllegalArgumentException.class, ()

		-> new SortBysParser<SortBy>(SortBy.class, SortBy.values()).parse("xxx"));
	}
}
