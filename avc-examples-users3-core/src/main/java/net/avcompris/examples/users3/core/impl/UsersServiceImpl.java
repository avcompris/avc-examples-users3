package net.avcompris.examples.users3.core.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolder;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolderOrNull;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.examples.shared3.core.impl.Validations.assertNonSuperadminUsername;
import static net.avcompris.examples.shared3.core.impl.Validations.assertValidPassword;
import static net.avcompris.examples.shared3.core.impl.Validations.assertValidUsername;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.IOException;
import java.sql.SQLException;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.avcompris.commons.query.FilterSyntaxException;
import net.avcompris.commons.query.impl.FilteringsFactory;
import net.avcompris.commons3.api.EnumRole;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.DaoException;
import net.avcompris.commons3.api.exception.InvalidQueryFilteringException;
import net.avcompris.commons3.api.exception.InvalidRoleException;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthorizedException;
import net.avcompris.commons3.api.exception.UsernameAlreadyExistsException;
import net.avcompris.commons3.api.exception.UsernameNotFoundException;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.AbstractServiceImpl;
import net.avcompris.commons3.core.impl.SortBysParser;
import net.avcompris.commons3.dao.exception.DuplicateEntityException;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UserUpdate;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.api.UsersQuery;
import net.avcompris.examples.users3.api.UsersQuery.SortBy;
import net.avcompris.examples.users3.core.api.MutableUserInfo;
import net.avcompris.examples.users3.core.api.MutableUsersInfo;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UserDto;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.UsersDto;
import net.avcompris.examples.users3.dao.UsersDtoQuery;
import net.avcompris.examples.users3.query.UserFiltering;
import net.avcompris.examples.users3.query.UserFilterings;

@Service
public final class UsersServiceImpl extends AbstractServiceImpl implements UsersService {

	private static final Log logger = LogFactory.getLog(UsersServiceImpl.class);

	private static final UsersDtoQuery.SortBy[] DEFAULT_SORT_BYS = new UsersDtoQuery.SortBy[] {
			UsersDtoQuery.SortBy.SORT_BY_USERNAME };

	private static final UsersDtoQuery.Expand[] DEFAULT_EXPANDS = new UsersDtoQuery.Expand[] {
			UsersDtoQuery.Expand.EXPAND_ALL };

	private final AuthDao authDao;
	private final UsersDao usersDao;

	private static final SortBysParser<SortBy> SORT_BYS_PARSER = new SortBysParser<SortBy>(SortBy.class,
			SortBy.values());

	@Autowired
	public UsersServiceImpl(final Permissions permissions, final Clock clock, //
			final UsersDao usersDao, //
			final AuthDao authDao) {

		super(permissions, clock);

		this.authDao = checkNotNull(authDao, "authDao");
		this.usersDao = checkNotNull(usersDao, "usersDao");
	}

	private static UsersDtoQuery.SortBy[] sortBys2Dto(final UsersQuery.SortBy... sortBys) {

		if (sortBys == null || sortBys.length == 0) {
			return DEFAULT_SORT_BYS;
		}

		final UsersDtoQuery.SortBy[] dto = new UsersDtoQuery.SortBy[sortBys.length];

		for (int i = 0; i < sortBys.length; ++i) {
			dto[i] = sortBy2Dto(sortBys[i]);
		}

		return dto;
	}

	private static UsersDtoQuery.SortBy sortBy2Dto(final UsersQuery.SortBy sortBy) {

		checkNotNull(sortBy, "sortBy");

		switch (sortBy) {
		case SORT_BY_USERNAME:
			return UsersDtoQuery.SortBy.SORT_BY_USERNAME;
		case SORT_BY_USERNAME_DESC:
			return UsersDtoQuery.SortBy.SORT_BY_USERNAME_DESC;
		case SORT_BY_ROLE:
			return UsersDtoQuery.SortBy.SORT_BY_ROLENAME;
		case SORT_BY_ROLE_DESC:
			return UsersDtoQuery.SortBy.SORT_BY_ROLENAME_DESC;
		case SORT_BY_ENABLED:
			return UsersDtoQuery.SortBy.SORT_BY_ENABLED;
		case SORT_BY_ENABLED_DESC:
			return UsersDtoQuery.SortBy.SORT_BY_ENABLED_DESC;
		case SORT_BY_CREATED_AT:
			return UsersDtoQuery.SortBy.SORT_BY_CREATED_AT;
		case SORT_BY_CREATED_AT_DESC:
			return UsersDtoQuery.SortBy.SORT_BY_CREATED_AT_DESC;
		case SORT_BY_UPDATED_AT:
			return UsersDtoQuery.SortBy.SORT_BY_UPDATED_AT;
		case SORT_BY_UPDATED_AT_DESC:
			return UsersDtoQuery.SortBy.SORT_BY_UPDATED_AT_DESC;
		case SORT_BY_LAST_ACTIVE_AT:
			return UsersDtoQuery.SortBy.SORT_BY_LAST_ACTIVE_AT;
		case SORT_BY_LAST_ACTIVE_AT_DESC:
			return UsersDtoQuery.SortBy.SORT_BY_LAST_ACTIVE_AT_DESC;
		case SORT_BY_REVISION:
			return UsersDtoQuery.SortBy.SORT_BY_REVISION;
		case SORT_BY_REVISION_DESC:
			return UsersDtoQuery.SortBy.SORT_BY_REVISION_DESC;
		default:
			throw new NotImplementedException("sortBy: " + sortBy);
		}
	}

	private static UsersDtoQuery.Expand[] expands2Dto(final UsersQuery.Expand... expands) {

		if (expands == null || expands.length == 0) {
			return DEFAULT_EXPANDS;
		}

		throw new NotImplementedException("");
	}

	@Override
	public UsersInfo getUsers(final String correlationId, final User user, @Nullable final UsersQuery query)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user, "query", query);

		// 1. LOGIC

		return privateGetUsers(query);
	}

	private UsersInfo privateGetUsers(@Nullable final UsersQuery query) throws ServiceException {

		final long startMs = System.currentTimeMillis();

		final int start = getQueryStart(query, 0);
		final int limit = getQueryLimit(query, 10);

		final UsersDtoQuery.Expand[] expandDtos;

		if (query == null) {
			expandDtos = DEFAULT_EXPANDS;
		} else {
			expandDtos = expands2Dto(query.getExpands());
		}

		final UsersDtoQuery dtoQuery = instantiate(UsersDtoQuery.class) //
				.setFiltering(query != null ? query.getFiltering() : null) //
				.setSortBys(query != null ? sortBys2Dto(query.getSortBys()) : DEFAULT_SORT_BYS) // ;
				.setExpands(expandDtos) //
				.setStart(start) //
				.setLimit(limit); //

		final UsersDto usersDto = wrap(()

		-> usersDao.getUsers(dtoQuery));

		final MutableUsersInfo result = instantiate(MutableUsersInfo.class) //
				.setStart(start) //
				.setLimit(limit) //
				.setSize(usersDto.getResults().length) //
				.setTotal(usersDto.getTotal()) //
				.setSqlWhereClause(usersDto.getSqlWhereClause());

		for (final UserDto userDto : usersDto.getResults()) {

			result.addToResults(dto2UserInfo(userDto));
		}

		final int tookMs = (int) (System.currentTimeMillis() - startMs);

		return result.setTookMs(tookMs);
	}

	private static UserInfo dto2UserInfo(final UserDto dto) {

		checkNotNull(dto, "dto");

		return instantiate(MutableUserInfo.class) //
				.setUsername(dto.getUsername()) //
				.setRole(Role.valueOf(dto.getRolename())) //
				.setPreferredLang(dto.getPreferredLang()) //
				.setPreferredTimeZone(dto.getPreferredTimeZone()) //
				.setCreatedAt(toDateTimeHolder(dto.getCreatedAt())) //
				.setUpdatedAt(toDateTimeHolder(dto.getUpdatedAt())) //
				.setRevision(dto.getRevision()) //
				.setLastActiveAt(toDateTimeHolderOrNull(dto.getLastActiveAt())) //
				.setEnabled(dto.isEnabled());
	}

	@Override
	public UserInfo createUser(final String correlationId, final User user, final String username,
			final UserCreate create) throws ServiceException {

		checkNotNull(user, "user");
		checkNotNull(correlationId, "correlationId");
		checkNotNull(username, "username");
		checkNotNull(create, "create");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user, "create", create);

		// 1. LOGIC

		return privateCreateUser(correlationId, user, username, create);
	}

	private UserInfo privateCreateUser(final String correlationId, final User user, final String username,
			final UserCreate create) throws ServiceException {

		final String password = create.getPassword();

		assertValidUsername(username);
		assertNonSuperadminUsername(username);

		if (password != null) {

			assertValidPassword(password);
		}

		final Role role = create.getRole();

		if (role == Role.ANONYMOUS || role == Role.SUPERADMIN) {

			throw new InvalidRoleException("role: " + role);
		}

		assertRoleCanManageRole(user.getRole(), role);

		try {

			usersDao.createUser( //
					username, //
					role.getRolename(), //
					create.getPreferredLang(), //
					create.getPreferredTimeZone(), //
					create.isEnabled());

			if (password == null) {

				authDao.removeUserPassword(username); // In case the DB was incoherent

			} else {

				authDao.setUserPassword(username, password);
			}

		} catch (final IOException | SQLException e) {

			throw new DaoException(e);

		} catch (final DuplicateEntityException e) {

			throw new UsernameAlreadyExistsException(username, e);
		}

		return privateGetUser(username);
	}

	@Override
	public UserInfo getUser(final String correlationId, final User user, final String username)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user, "username", username);

		// 1. LOGIC

		return privateGetUser(username);
	}

	@Override
	public boolean hasUser(final String correlationId, final User user, final String username) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user, "username", username);

		// 1. LOGIC

		assertValidUsername(username);

		final UserDto dto = wrap(()

		-> usersDao.getUser(username));

		return dto != null;
	}

	private UserInfo privateGetUser(final String username) throws ServiceException {

		checkNotNull(username, "username");

		assertValidUsername(username);

		final UserDto dto = wrap(()

		-> usersDao.getUser(username));

		if (dto == null) {

			throw new UsernameNotFoundException(username);
		}

		return dto2UserInfo(dto);
	}

	@Override
	public UserInfo getUserMe(final String correlationId, final User user) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		return privateGetUser(user.getUsername());
	}

	@Override
	@Nullable
	public UsersQuery validateUsersQuery(final String correlationId, final User user, @Nullable final String q,
			@Nullable final String sort, @Nullable final Integer start, @Nullable final Integer limit,
			@Nullable final String expand) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		if (isBlank(q) && isBlank(sort) && start == null && limit == null && isBlank(expand)) {
			return null;
		}

		final UsersQuery query = instantiate(UsersQuery.class);

		if (start != null) {

			if (start < 0) {
				throw new InvalidQueryFilteringException("Query.start should be >= 0, but was: " + start);
			}

			query.setStart(start);
		}

		if (limit != null) {

			if (limit < 0) {
				throw new InvalidQueryFilteringException("Query.limit should be >= 0, but was: " + limit);
			}

			query.setLimit(limit);
		}

		if (!isBlank(q)) {

			if (q.length() > 1000) {
				throw new InvalidQueryFilteringException(
						"Query should be at most 1,000 characters, but was: " + q.length());
			}

			final UserFilterings filterings = FilteringsFactory.instantiate(UserFilterings.class);

			final UserFiltering filtering;

			try {

				filtering = filterings.parse(q);

			} catch (final FilterSyntaxException e) {

				throw new InvalidQueryFilteringException(e);
			}

			query.setFiltering(filtering);
		}

		if (!isBlank(sort)) {

			query.setSortBys(SORT_BYS_PARSER.parse(sort));
		}

		if (!isBlank(expand)) {

			throw new NotImplementedException("expand: " + expand);
		}

		return query;
	}

	@Override
	public UserInfo updateUserMe(final String correlationId, final User user, final UserUpdate update)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(update, "update");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		final String username = user.getUsername();

		if (logger.isInfoEnabled()) {
			logger.info("updateUserMe(): username: " + username + ", " + update + "...");
		}

		@Nullable
		final String password = update.getPassword();

		final UserDto toDto = wrap(() -> {

			final Role role = extractUpdateRole(update, () -> //
			getUserMe(correlationId, user).getRole());

			final boolean enabled = extractUpdateEnabled(update, () -> //
			usersDao.getUser(username).isEnabled());

			usersDao.updateUser(username, //
					role.getRolename(), //
					update.getPreferredLang(), //
					update.getPreferredTimeZone(), //
					enabled, //
					update.getFromRevision());

			if (password != null) {

				authDao.setUserPassword(username, password);
			}

			return usersDao.getUser(username);

		});

		if (logger.isInfoEnabled()) {
			logger.info("updateUserMe(): Ending.");
		}

		return dto2UserInfo(toDto);
	}

	private Role extractUpdateRole(final UserUpdate update, final Action<Role> action)
			throws IOException, SQLException, ServiceException {

		final Role role;

		if (update.getRole() != null) {

			role = update.getRole();

		} else {

			role = action.execute();
		}

		if (role == Role.ANONYMOUS || role == Role.SUPERADMIN) {

			throw new InvalidRoleException("role: " + role);
		}

		return role;
	}

	private boolean extractUpdateEnabled(final UserUpdate update, final Action<Boolean> action)
			throws IOException, SQLException, ServiceException {

		if (update.isEnabled() != null) {

			return update.isEnabled();
		}

		return action.execute();
	}

	@Override
	public UserInfo updateUser(final String correlationId, final User user, final String username,
			final UserUpdate update) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");
		checkNotNull(update, "update");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		if (logger.isInfoEnabled()) {
			logger.info("updateUser(): " + update + "...");
		}

		final UserInfo updated = privateUpdateUser(correlationId, user, username, update);

		if (logger.isInfoEnabled()) {
			logger.info("updateUser(): Ending.");
		}

		return updated;
	}

	private static void assertRoleCanManageRole(final EnumRole manager, final Role managed)
			throws UnauthorizedException {

		checkNotNull(manager, "manager");
		checkNotNull(managed, "managed");

		if (!((Role) manager).canManage(managed)) {

			throw new UnauthorizedException(
					"Role: " + manager.getRolename() + " cannot manage role: " + managed.getRolename());
		}
	}

	public UserInfo privateUpdateUser(final String correlationId, final User user, final String username,
			final UserUpdate update) throws ServiceException {

		@Nullable
		final String password = update.getPassword();

		final Role oldRole = privateGetUser(username).getRole();

		assertRoleCanManageRole(user.getRole(), oldRole);

		final Role role = wrap(() -> extractUpdateRole(update, () -> oldRole));

		assertRoleCanManageRole(user.getRole(), role);

		final boolean enabled = wrap(() -> extractUpdateEnabled(update, () -> //
		usersDao.getUser(username).isEnabled()));

		final UserDto toDto = wrap(() -> {

			usersDao.updateUser(username, //
					role.getRolename(), //
					update.getPreferredLang(), //
					update.getPreferredTimeZone(), //
					enabled, //
					update.getFromRevision());

			if (password != null) {

				authDao.setUserPassword(username, password);
			}

			return usersDao.getUser(username);

		});

		return dto2UserInfo(toDto);
	}

	@Override
	public void deleteUser(final String correlationId, final User user, final String username) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		privateDeleteUser(user, username);
	}

	private void privateDeleteUser(final User user, final String username) throws ServiceException {

		// final UserInfo old = getUser(correlationId, user, username);
		//
		// if (user.getRole() == Role.USERMGR && old.getRole() == Role.ADMIN) {
		// throw new UnauthorizedException("USERMGR cannot act on ADMINs");
		// }

		assertNonSuperadminUsername(username);

		if (user.getUsername().contentEquals(username)) {
			throw new UnauthorizedException("A user may not delete itself: " + username);
		}

		wrap(()

		-> usersDao.deleteUser(username));
	}
}
