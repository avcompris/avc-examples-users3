package net.avcompris.examples.users3.core.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolder;
import static net.avcompris.commons3.core.DateTimeHolderImpl.toDateTimeHolderOrNull;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.apache.commons.lang3.CharEncoding.UTF_8;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.File;
import java.io.IOException;

import javax.annotation.Nullable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.avcompris.commons.query.FilterSyntaxException;
import net.avcompris.commons.query.impl.FilteringsFactory;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.api.UserSessionFiltering;
import net.avcompris.commons3.api.UserSessionFilterings;
import net.avcompris.commons3.api.UserSessions;
import net.avcompris.commons3.api.UserSessionsQuery;
import net.avcompris.commons3.api.UserSessionsQuery.SortBy;
import net.avcompris.commons3.api.exception.InvalidQueryFilteringException;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthenticatedException;
import net.avcompris.commons3.api.exception.UnexpectedException;
import net.avcompris.commons3.core.Permissions;
import net.avcompris.commons3.core.impl.AbstractServiceImpl;
import net.avcompris.commons3.core.impl.SortBysParser;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.shared3.Constants.ReservedUsername;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.core.api.MutableUserSession;
import net.avcompris.examples.users3.core.api.MutableUserSessions;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UserDto;
import net.avcompris.examples.users3.dao.UserSessionDto;
import net.avcompris.examples.users3.dao.UserSessionsDto;
import net.avcompris.examples.users3.dao.UserSessionsDtoQuery;
import net.avcompris.examples.users3.dao.UsersDao;

@Service
public final class AuthServiceImpl extends AbstractServiceImpl implements MyAuthService {

	private final AuthDao authDao;
	private final UsersDao usersDao;

	@Nullable
	private final File superadminAuthorizationFile;

	private static final UserSessionsDtoQuery.SortBy[] DEFAULT_SORT_BYS = new UserSessionsDtoQuery.SortBy[] {
			UserSessionsDtoQuery.SortBy.SORT_BY_USERNAME };

	private static final UserSessionsDtoQuery.Expand[] DEFAULT_EXPANDS = new UserSessionsDtoQuery.Expand[] {
			UserSessionsDtoQuery.Expand.EXPAND_ALL };

	private static final SortBysParser<SortBy> SORT_BYS_PARSER = new SortBysParser<SortBy>(SortBy.class,
			SortBy.values());

	@Autowired
	public AuthServiceImpl(final Permissions permissions, final Clock clock, final UsersDao usersDao,
			final AuthDao authDao) throws IOException {

		super(permissions, clock);

		this.authDao = checkNotNull(authDao, "authDao");
		this.usersDao = checkNotNull(usersDao, "usersDao");

		final String superadminAuthorizationFileProperty = System.getProperty("superadmin.authorizationFile");

		if (isBlank(superadminAuthorizationFileProperty)) {

			System.out.println("=== INFO === No \"superadmin.authorizationFile\" env property has been set.");

			superadminAuthorizationFile = null;

		} else {

			System.out.println("=== INFO === \"superadmin.authorizationFile\" has been set in the System environment: "
					+ superadminAuthorizationFileProperty);

			superadminAuthorizationFile = new File(superadminAuthorizationFileProperty);

			System.out.println("superadmin.authorizationFile: " + superadminAuthorizationFile.getCanonicalPath());
		}
	}

	@Override
	@Nullable
	public User getAuthenticatedUser(@Nullable final String authorization, @Nullable final String userSessionId)
			throws ServiceException {

		// 0. PERMISSIONS

		// permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		// 1. SUPERADMIN AUTHORIZATION?

		final boolean isSuperadminAuthorization;

		if (authorization == null) {

			isSuperadminAuthorization = false;

		} else if (superadminAuthorizationFile != null) {

			final String superadminAuthorizationFromFile;

			try {

				superadminAuthorizationFromFile = FileUtils.readFileToString(superadminAuthorizationFile, UTF_8).trim();

			} catch (final IOException e) {

				throw new UnexpectedException("Cannot read superadminAuthorization from: " //
						+ superadminAuthorizationFile, e);
			}

			isSuperadminAuthorization = superadminAuthorizationFromFile.contentEquals(authorization);

		} else {

			isSuperadminAuthorization = false;
		}

		if (isSuperadminAuthorization) {

			// This User object is *not* stored in the database, it’s just here for admin
			// purpose.
			//
			return new User() {

				@Override
				public String getUsername() {
					return ReservedUsername.SUPERADMIN.label(); // Here, we hardcode the "superadmin" username
				}

				@Override
				public Role getRole() {
					return Role.SUPERADMIN;
				}
			};
		}

		// 2. AUTHORIZATION?

		if (authorization != null) {

			final String username = wrap(()

			-> authDao.getUsernameByAuthorization(authorization, clock.now()));

			final UserDto dto = getUserDto(username);

			if (dto != null) {

				return dto2User(dto);
			}
		}

		// 3. USER SESSION ID?

		if (userSessionId != null) {

			final String username = wrap(()

			-> authDao.getUsernameBySessionId(userSessionId, clock.now()));

			final UserDto dto = getUserDto(username);

			if (dto != null) {

				return dto2User(dto);
			}
		}

		// 4. NO AUTHENTICATED USER

		return null;
	}

	@Nullable
	private UserDto getUserDto(@Nullable final String username) throws ServiceException {

		if (username == null) {
			return null;
		}

		return wrap(()

		-> usersDao.getUser(username));
	}

	private static User dto2User(final UserDto dto) {

		checkNotNull(dto, "dto");

		final String username = dto.getUsername();
		final Role role = Role.valueOf(dto.getRolename());

		return new User() {

			@Override
			public String getUsername() {
				return username;
			}

			@Override
			public Role getRole() {
				return role;
			}
		};
	}

	@Override
	public UserSession authenticate(final String correlationId, final String username, final String password)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(username, "username");
		checkNotNull(password, "password");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		// permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		final boolean isValid = wrap(()

		-> authDao.isValidUserPassword(username, password));

		if (!isValid) {

			throw new UnauthenticatedException();
		}

		final DateTime now = clock.now();

		final UserSessionDto dto = wrap(() -> {

			final UserSessionDto s = authDao.newUserSession(username, now);

			usersDao.setLastActiveAt(username, now);

			return s;

		});

		return dto2UserSession(dto);
	}

	private static UserSession dto2UserSession(final UserSessionDto dto) {

		checkNotNull(dto, "dto");

		return instantiate(MutableUserSession.class) //
				.setUsername(dto.getUsername()) //
				.setUserSessionId(dto.getUserSessionId()) //
				.setCreatedAt(toDateTimeHolder(dto.getCreatedAt())) //
				.setUpdatedAt(toDateTimeHolder(dto.getUpdatedAt())) //
				.setExpiresAt(toDateTimeHolder(dto.getExpiresAt())) //
				.setExpiredAt(toDateTimeHolderOrNull(dto.getExpiredAt()));
	}

	@Override
	public UserSession getMySession(final String correlationId, final User user, final String userSessionId)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(userSessionId, "userSessionId");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		return getUserSession(userSessionId);
	}

	@Override
	public UserSession getUserSession(final String correlationId, final User user, final String userSessionId)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(userSessionId, "userSessionId");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		return getUserSession(userSessionId);
	}

	private UserSession getUserSession(final String userSessionId) throws ServiceException {

		checkNotNull(userSessionId, "userSessionId");

		final UserSessionDto dto = wrap(()

		-> authDao.getUserSession(userSessionId, clock.now()));

		return (dto != null) ? dto2UserSession(dto) : null;
	}

	@Override
	public UserSession terminateMySession(final String correlationId, final User user, final String userSessionId)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(userSessionId, "userSessionId");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		final DateTime now = clock.now();

		wrap(() -> {

			authDao.terminateSession(userSessionId, now, now);

			usersDao.setLastActiveAt(user.getUsername(), now);
		});

		return getUserSession(userSessionId);
	}

	@Override
	public UserSession terminateUserSession(final String correlationId, final User user, final String userSessionId)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(userSessionId, "userSessionId");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		final DateTime now = clock.now();

		wrap(()

		-> authDao.terminateSession(userSessionId, null, now));

		return getUserSession(userSessionId);
	}

	@Override
	public void setLastActiveAt(final String correlationId, final User user) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		wrap(()

		-> usersDao.setLastActiveAt(user.getUsername(), clock.now()));
	}

	@Override
	public UserSessions getUserSessions(final String correlationId, final User user,
			@Nullable final UserSessionsQuery query) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user, "query", query);

		// 1. LOGIC

		return privateGetUserSessions(query);
	}

	private UserSessions privateGetUserSessions(@Nullable final UserSessionsQuery query) throws ServiceException {

		final long startMs = System.currentTimeMillis();

		final int start = getQueryStart(query, 0);
		final int limit = getQueryLimit(query, 10);

		final UserSessionsDtoQuery.Expand[] expandDtos;

		if (query == null) {
			expandDtos = DEFAULT_EXPANDS;
		} else {
			expandDtos = expands2Dto(query.getExpands());
		}

		final UserSessionsDtoQuery dtoQuery = instantiate(UserSessionsDtoQuery.class) //
				.setFiltering(query != null ? query.getFiltering() : null) //
				.setSortBys(query != null ? sortBys2Dto(query.getSortBys()) : DEFAULT_SORT_BYS) // ;
				.setExpands(expandDtos) //
				.setStart(start) //
				.setLimit(limit); //

		final UserSessionsDto userSessionsDto = wrap(()

		-> authDao.getUserSessions(dtoQuery));

		final MutableUserSessions result = instantiate(MutableUserSessions.class) //
				.setStart(start) //
				.setLimit(limit) //
				.setSize(userSessionsDto.getResults().length) //
				.setTotal(userSessionsDto.getTotal()) //
				.setSqlWhereClause(userSessionsDto.getSqlWhereClause());

		for (final UserSessionDto userSessionDto : userSessionsDto.getResults()) {

			result.addToResults(dto2UserSession(userSessionDto));
		}

		final int tookMs = (int) (System.currentTimeMillis() - startMs);

		return result.setTookMs(tookMs);
	}

	private static UserSessionsDtoQuery.Expand[] expands2Dto(final UserSessionsQuery.Expand... expands) {

		if (expands == null || expands.length == 0) {
			return DEFAULT_EXPANDS;
		}

		throw new NotImplementedException("");
	}

	private static UserSessionsDtoQuery.SortBy[] sortBys2Dto(final UserSessionsQuery.SortBy... sortBys) {

		if (sortBys == null || sortBys.length == 0) {
			return DEFAULT_SORT_BYS;
		}

		final UserSessionsDtoQuery.SortBy[] dto = new UserSessionsDtoQuery.SortBy[sortBys.length];

		for (int i = 0; i < sortBys.length; ++i) {
			dto[i] = sortBy2Dto(sortBys[i]);
		}

		return dto;
	}

	private static UserSessionsDtoQuery.SortBy sortBy2Dto(final UserSessionsQuery.SortBy sortBy) {

		checkNotNull(sortBy, "sortBy");

		switch (sortBy) {
		case SORT_BY_USERNAME:
			return UserSessionsDtoQuery.SortBy.SORT_BY_USERNAME;
		case SORT_BY_USERNAME_DESC:
			return UserSessionsDtoQuery.SortBy.SORT_BY_USERNAME_DESC;
		case SORT_BY_USER_SESSION_ID:
			return UserSessionsDtoQuery.SortBy.SORT_BY_USER_SESSION_ID;
		case SORT_BY_USER_SESSION_ID_DESC:
			return UserSessionsDtoQuery.SortBy.SORT_BY_USER_SESSION_ID_DESC;
		case SORT_BY_CREATED_AT:
			return UserSessionsDtoQuery.SortBy.SORT_BY_CREATED_AT;
		case SORT_BY_CREATED_AT_DESC:
			return UserSessionsDtoQuery.SortBy.SORT_BY_CREATED_AT_DESC;
		case SORT_BY_UPDATED_AT:
			return UserSessionsDtoQuery.SortBy.SORT_BY_UPDATED_AT;
		case SORT_BY_UPDATED_AT_DESC:
			return UserSessionsDtoQuery.SortBy.SORT_BY_UPDATED_AT_DESC;
		case SORT_BY_EXPIRES_AT:
			return UserSessionsDtoQuery.SortBy.SORT_BY_EXPIRES_AT;
		case SORT_BY_EXPIRES_AT_DESC:
			return UserSessionsDtoQuery.SortBy.SORT_BY_EXPIRES_AT_DESC;
		case SORT_BY_EXPIRED_AT:
			return UserSessionsDtoQuery.SortBy.SORT_BY_EXPIRED_AT;
		case SORT_BY_EXPIRED_AT_DESC:
			return UserSessionsDtoQuery.SortBy.SORT_BY_EXPIRED_AT_DESC;
		default:
			throw new NotImplementedException("sortBy: " + sortBy);
		}
	}

	@Override
	@Nullable
	public UserSessionsQuery validateUserSessionsQuery(final String correlationId, final User user,
			@Nullable final String q, @Nullable final String sort, @Nullable final Integer start,
			@Nullable final Integer limit, @Nullable final String expand) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		LogFactory.setCorrelationId(correlationId);

		// 0. PERMISSIONS

		permissions.assertAuthorized(correlationId, user);

		// 1. LOGIC

		if (isBlank(q) && isBlank(sort) && start == null && limit == null && isBlank(expand)) {
			return null;
		}

		final UserSessionsQuery query = instantiate(UserSessionsQuery.class);

		if (start != null) {

			if (start < 0) {
				throw new InvalidQueryFilteringException("Query.start should be >= 0, but was: " + start);
			}

			query.setStart(start);
		}

		if (limit != null) {

			if (limit < 0) {
				throw new InvalidQueryFilteringException("Query.limit should be >= 0, but was: " + limit);
			}

			query.setLimit(limit);
		}

		if (!isBlank(q)) {

			if (q.length() > 1000) {
				throw new InvalidQueryFilteringException(
						"Query should be at most 1,000 characters, but was: " + q.length());
			}

			final UserSessionFilterings filterings = FilteringsFactory.instantiate(UserSessionFilterings.class);

			final UserSessionFiltering filtering;

			try {

				filtering = filterings.parse(q);

			} catch (final FilterSyntaxException e) {

				throw new InvalidQueryFilteringException(e);
			}

			query.setFiltering(filtering);
		}

		if (!isBlank(sort)) {

			query.setSortBys(SORT_BYS_PARSER.parse(sort));
		}

		if (!isBlank(expand)) {

			throw new NotImplementedException("expand: " + expand);
		}

		return query;
	}
}
