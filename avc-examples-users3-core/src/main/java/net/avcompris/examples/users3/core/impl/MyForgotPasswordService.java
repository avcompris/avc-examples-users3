package net.avcompris.examples.users3.core.impl;

import static net.avcompris.examples.shared3.Permission.GET_MY_SESSION;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.core.ForgotPasswordService;
import net.avcompris.examples.shared3.core.api.Permissions;

public interface MyForgotPasswordService extends ForgotPasswordService {

	@Permissions(GET_MY_SESSION)
	@Override
	void forgotPassword(String correlationId, String uiUrl, String emailAddress) throws ServiceException;

	@Permissions(GET_MY_SESSION)
	@Override
	void resetPassword(String correlationId, String token, String password) throws ServiceException;
}
