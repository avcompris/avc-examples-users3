package net.avcompris.examples.users3.core.impl;

import static net.avcompris.examples.shared3.Permission.ANY;
import static net.avcompris.examples.shared3.Permission.GET_ANY_USER_SESSION;
import static net.avcompris.examples.shared3.Permission.GET_MY_SESSION;
import static net.avcompris.examples.shared3.Permission.SET_LAST_ACTIVE_AT;
import static net.avcompris.examples.shared3.Permission.TERMINATE_ANY_USER_SESSION;
import static net.avcompris.examples.shared3.Permission.TERMINATE_MY_SESSION;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.api.UserSessions;
import net.avcompris.commons3.api.UserSessionsQuery;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.core.AuthService;
import net.avcompris.examples.shared3.core.api.Permissions;

public interface MyAuthService extends AuthService {

	@Permissions(ANY)
	@Nullable
	UserSessionsQuery validateUserSessionsQuery(String correlationId, User user, //
			@Nullable String q, //
			@Nullable String sort, //
			@Nullable Integer start, //
			@Nullable Integer limit, //
			@Nullable String expand //
	) throws ServiceException;

	@Permissions(GET_ANY_USER_SESSION)
	@Override
	UserSessions getUserSessions(String correlationId, User user, //
			@Nullable UserSessionsQuery query //
	) throws ServiceException;

	@Permissions(ANY)
	@Nullable
	@Override
	User getAuthenticatedUser(@Nullable String authorization, @Nullable String userSessionId) throws ServiceException;

	@Permissions(ANY)
	@Nullable
	@Override
	UserSession authenticate(String correlationId, String username, String password) throws ServiceException;

	@Permissions(SET_LAST_ACTIVE_AT)
	@Override
	void setLastActiveAt(String correlationId, User user) throws ServiceException;

	@Permissions(GET_ANY_USER_SESSION)
	@Override
	UserSession getUserSession(String correlationId, User user, String userSessionId) throws ServiceException;

	@Permissions(TERMINATE_ANY_USER_SESSION)
	@Override
	UserSession terminateUserSession(String correlationId, User user, String userSessionId) throws ServiceException;

	@Permissions(GET_MY_SESSION)
	@Override
	UserSession getMySession(String correlationId, User user, String userSessionId) throws ServiceException;

	@Permissions(TERMINATE_MY_SESSION)
	@Override
	UserSession terminateMySession(String correlationId, User user, String userSessionId) throws ServiceException;
}
