package net.avcompris.examples.users3.client;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.utils.EnvUtils.getEnvProperty;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import net.avcompris.commons3.api.EntitiesQueryRaw;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.client.impl.AbstractClient;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UserUpdate;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.api.UsersQuery;
import net.avcompris.examples.users3.core.api.MutableUserInfo;
import net.avcompris.examples.users3.core.api.MutableUsersInfo;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.examples.users3.query.UserFiltering;

@Component
public class UsersServiceClient extends AbstractClient implements UsersService {

	@Autowired
	public UsersServiceClient(final SessionPropagator sessionPropagator) {

		super(getEnvProperty("users.baseURL", "http://localhost:8080/api/v1/"), sessionPropagator);
	}

	@Override
	public UsersQuery validateUsersQuery(final String correlationId, final User user, @Nullable final String q,
			@Nullable final String sort, @Nullable final Integer start, @Nullable final Integer limit,
			@Nullable final String expand) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		throw new NotImplementedException("");
	}

	@Override
	public UsersInfo getUsers(final String correlationId, final User user, @Nullable final UsersQuery query)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		final RestTemplate restTemplate = createRestTemplate(MutableUsersInfo.class, //
				UserInfo.class, MutableUserInfo.class);

		final UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(baseURL);

		if (query == null) {

			final HttpEntity<EntitiesQueryRaw<UserFiltering, UserFiltering.Field>> request = null;

			return wrap(()

			-> exchange(restTemplate, urlBuilder.toUriString(), HttpMethod.GET, request, MutableUsersInfo.class)
					.getBody());

		} else {

			// final EntitiesQueryRaw<UserFiltering, UserFiltering.Field> queryRaw =
			// EntitiesQueryRawUtils.serialize(query);
			//
			// final HttpEntity<EntitiesQueryRaw<UserFiltering, UserFiltering.Field>>
			// request = new HttpEntity<>(queryRaw,
			// headers(correlationId, sessionPropagator));

			// return wrap(()
			//
			// -> exchange(restTemplate, urlBuilder.toUriString(), HttpMethod.POST, request,
			// MutableUsersInfo.class)
//					.getBody());

			throw new NotImplementedException("query: " + query);
		}
	}

	@Override
	public boolean hasUser(final String correlationId, final User user, final String username) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");

		throw new NotImplementedException("");
	}

	@Override
	public UserInfo createUser(final String correlationId, final User user, String username, final UserCreate create)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");
		checkNotNull(create, "create");

		final RestTemplate restTemplate = createRestTemplate(MutableUserInfo.class);

		final UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(baseURL) //
				.pathSegment("users") //
				.pathSegment(username);

		final HttpEntity<UserCreate> request = new HttpEntity<>(create, headers(correlationId, sessionPropagator));

		return wrap(()

		-> exchange(restTemplate, urlBuilder.toUriString(), HttpMethod.POST, request, MutableUserInfo.class).getBody());
	}

	@Override
	public UserInfo getUser(final String correlationId, final User user, final String username)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");

		final RestTemplate restTemplate = createRestTemplate(MutableUserInfo.class);

		final UriComponentsBuilder urlBuilder = UriComponentsBuilder.fromHttpUrl(baseURL) //
				.pathSegment("users") //
				.pathSegment(username);

		final HttpEntity<String> request = new HttpEntity<>(null, headers(correlationId, sessionPropagator));

		return wrap(()

		-> exchange(restTemplate, urlBuilder.toUriString(), HttpMethod.GET, request, MutableUserInfo.class).getBody());
	}

	@Override
	public UserInfo updateUser(final String correlationId, final User user, final String username,
			final UserUpdate update) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");
		checkNotNull(update, "update");

		throw new NotImplementedException("");
	}

	@Override
	public void deleteUser(final String correlationId, final User user, final String username) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(username, "username");

		throw new NotImplementedException("");
	}

	@Override
	public UserInfo getUserMe(final String correlationId, final User user) throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");

		throw new NotImplementedException("");
	}

	@Override
	public UserInfo updateUserMe(final String correlationId, final User user, final UserUpdate update)
			throws ServiceException {

		checkNotNull(correlationId, "correlationId");
		checkNotNull(user, "user");
		checkNotNull(update, "update");

		throw new NotImplementedException("");
	}
}
