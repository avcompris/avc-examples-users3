#!/bin/sh

# File: avc-examples-users3/build_run_dev.sh
#
# Build and run the "examples-users3" HTTP/REST API from the command
# line.  The web service will be listening on port 8080.
#
# 1. COMMAND LINE OPTIONS:
#
#      --inMemory   (stores data in memory)
#      --rds        (stores data in PostgreSQL)
#      -c           (doesn't run "mvn clean")
#      -d           (doesn't run "mvn test")

set -e

MVN_GOAL_CLEAN=clean
MVN_OPT_SKIPTESTS=
APP_OPT=--inMemory # Or: --rds

while test -n "${1}"; do
	if test "${1}" = "-c"; then
		MVN_GOAL_CLEAN=
		shift
	elif test "${1}" = "-d"; then
		MVN_OPT_SKIPTESTS=-DskipTests
		shift
	elif test "${1}" = "--rds"; then
		APP_OPT=--rds
		shift
	elif test "${1}" = "--inMemory"; then
		APP_OPT=--inMemory
		shift
	else
		echo "Unknown option: ${1}" >&2
		echo "Exiting." >&2
		exit 1
	fi
done

# 2. ENVIRONMENT VARIABLES:
#
#      SUPERADMIN_AUTHORIZATION_FILE
#      RDS_URL               (e.g. jdbc:postgresql://192.168.54.54:5432/devdb)
#      RDS_USERNAME          (e.g. postgres)
#      RDS_PASSWORD
#      RDS_TABLENAME_PREFIX  (e.g. qa_)
#      RABBITMQ_URL          (e.g. amqp://192.168.54.54:5672)

if [ -f ../local/setenv.sh ]; then
	. ../local/setenv.sh
fi

MVN_CMD="mvn ${MVN_GOAL_CLEAN} install ${MVN_OPT_SKIPTESTS}"

# 3. BUILD THE APP

echo "MVN_CMD: ${MVN_CMD}"

${MVN_CMD} -f pom.xml || exit 1

ARCHIVE=avc-examples-users3-web
VERSION=`grep version pom.xml | head -1 | sed 's/.*>\(.*\)<.*/\1/'`

# 4. RUN THE APP

echo "APP_OPT: ${APP_OPT}"

java -Dserver.port=8080 \
     -Dsuperadmin.authorizationFile=${SUPERADMIN_AUTHORIZATION_FILE} \
     -Ddebug \
     -Drds.url=${RDS_URL} \
     -Drds.username=${RDS_USERNAME} \
     -Drds.password=${RDS_PASSWORD} \
     -Drds.tableNamePrefix=${RDS_TABLENAME_PREFIX} \
     -Drabbitmq.url=${RABBITMQ_URL} \
     -Drabbitmq.queue.error=error_out \
     -jar "${ARCHIVE}/target/${ARCHIVE}-${VERSION}-exec.jar" \
     ${APP_OPT} --unsecure --debug
