package net.avcompris.examples.users3.core.api;

import static net.avcompris.examples.shared3.Permission.ANY;
import static net.avcompris.examples.shared3.Permission.CREATE_ANY_USER;
import static net.avcompris.examples.shared3.Permission.DELETE_ANY_USER;
import static net.avcompris.examples.shared3.Permission.GET_ANY_USER;
import static net.avcompris.examples.shared3.Permission.GET_USER_ME;
import static net.avcompris.examples.shared3.Permission.QUERY_ALL_USERS;
import static net.avcompris.examples.shared3.Permission.UPDATE_ANY_USER;
import static net.avcompris.examples.shared3.Permission.UPDATE_USER_ME;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.examples.shared3.core.api.Permissions;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UserUpdate;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.api.UsersQuery;

public interface UsersService {

	@Permissions(ANY)
	@Nullable
	UsersQuery validateUsersQuery(String correlationId, User user, //
			@Nullable String q, //
			@Nullable String sort, //
			@Nullable Integer start, //
			@Nullable Integer limit, //
			@Nullable String expand //
	) throws ServiceException;

	@Permissions(QUERY_ALL_USERS)
	UsersInfo getUsers(String correlationId, User user, //
			@Nullable UsersQuery query //
	) throws ServiceException;

	@Permissions(GET_ANY_USER)
	boolean hasUser(String correlationId, User user, //
			String username //
	) throws ServiceException;

	@Permissions(CREATE_ANY_USER)
	UserInfo createUser(String correlationId, User user, //
			String username, //
			UserCreate create //
	) throws ServiceException;

	@Permissions(GET_ANY_USER)
	UserInfo getUser(String correlationId, User user, //
			String username //
	) throws ServiceException;

	@Permissions(UPDATE_ANY_USER)
	UserInfo updateUser(String correlationId, User user, //
			String username, //
			UserUpdate update //
	) throws ServiceException;

	@Permissions(DELETE_ANY_USER)
	void deleteUser(String correlationId, User user, //
			String username //
	) throws ServiceException;

	@Permissions(GET_USER_ME)
	UserInfo getUserMe(String correlationId, User user) throws ServiceException;

	@Permissions(UPDATE_USER_ME)
	UserInfo updateUserMe(String correlationId, User user, //
			UserUpdate update //
	) throws ServiceException;
}
