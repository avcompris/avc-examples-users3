package net.avcompris.examples.users3.core.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.core.MutableEntity;
import net.avcompris.commons3.types.DateTimeHolder;
import net.avcompris.examples.shared3.Role;
import net.avcompris.examples.users3.api.UserInfo;

public interface MutableUserInfo extends UserInfo, MutableEntity {

	MutableUserInfo setUsername(String username);

	MutableUserInfo setRole(Role role);

	MutableUserInfo setPreferredLang(@Nullable String preferredLang);

	MutableUserInfo setPreferredTimeZone(@Nullable String preferredTimeZone);

	@Override
	MutableUserInfo setCreatedAt(DateTimeHolder createdAt);

	@Override
	MutableUserInfo setUpdatedAt(DateTimeHolder updatedAt);

	@Override
	MutableUserInfo setRevision(int revision);

	MutableUserInfo setLastActiveAt(@Nullable DateTimeHolder lastActiveAt);

	MutableUserInfo setEnabled(boolean enabled);
}
