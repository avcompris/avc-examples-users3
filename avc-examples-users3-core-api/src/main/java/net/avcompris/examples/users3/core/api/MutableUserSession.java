package net.avcompris.examples.users3.core.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.types.DateTimeHolder;

public interface MutableUserSession extends UserSession {

	MutableUserSession setUserSessionId(String userSessionId);

	MutableUserSession setUsername(String username);

	MutableUserSession setCreatedAt(DateTimeHolder createdAt);

	MutableUserSession setUpdatedAt(DateTimeHolder updatedAt);

	MutableUserSession setExpiresAt(DateTimeHolder expiresAt);

	MutableUserSession setExpiredAt(@Nullable DateTimeHolder expiredAt);
}
