package net.avcompris.examples.users3.core.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.core.MutableEntities;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UsersInfo;

public interface MutableUsersInfo extends UsersInfo, MutableEntities<UserInfo> {

	MutableUsersInfo setResults(UserInfo... items);

	@Override
	MutableUsersInfo addToResults(UserInfo item);

	@Override
	MutableUsersInfo setStart(int start);

	@Override
	MutableUsersInfo setLimit(int limit);

	@Override
	MutableUsersInfo setSize(int size);

	@Override
	MutableUsersInfo setTotal(int total);

	@Override
	MutableUsersInfo setTookMs(int tookMs);

	@Override
	MutableUsersInfo setSqlWhereClause(@Nullable String sqlWhereClause);
}
