package net.avcompris.examples.users3.core.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.api.UserSessions;

public interface MutableUserSessions extends UserSessions {

	MutableUserSessions setResults(UserSession... items);

	MutableUserSessions addToResults(UserSession item);

	MutableUserSessions setStart(int start);

	MutableUserSessions setLimit(int limit);

	MutableUserSessions setSize(int size);

	MutableUserSessions setTotal(int total);

	MutableUserSessions setTookMs(int tookMs);

	MutableUserSessions setSqlWhereClause(@Nullable String sqlWhereClause);
}
