package net.avcompris.examples.users3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Set;

import javax.annotation.Nullable;
import javax.sql.DataSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.avcompris.commons.query.impl.SqlWhereClause;
import net.avcompris.commons3.api.UserSessionFiltering;
import net.avcompris.commons3.dao.impl.AbstractDaoInRDS;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UserSessionDto;
import net.avcompris.examples.users3.dao.UserSessionsDto;
import net.avcompris.examples.users3.dao.UserSessionsDtoQuery;

@Component
public final class AuthDaoInRDS extends AbstractDaoInRDS implements AuthDao {

	private static final int SESSION_TIMEOUT_MINUTES = 60;

	private final String sessionsTableName;

	private final boolean debug;

	@Autowired
	public AuthDaoInRDS( //
			@Value("#{rds.dataSource}") final DataSource dataSource, //
			@Value("#{rds.tableNames.auth}") final String tableName, //
			final Clock clock) {

		super(dataSource, tableName, clock);

		sessionsTableName = tableName + "_sessions";

		debug = System.getProperty("debug") != null;
	}

	private static String hashPassword(final String passwordSalt, final String password) {

		return DigestUtils.sha256Hex(passwordSalt + password);
	}

	@Override
	public void setUserPassword(final String username, final String password) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(password, "password");

		final String passwordSalt = randomAlphanumeric(20);

		final String passwordHash = hashPassword(passwordSalt, password);

		try (Connection cxn = getConnection()) {

			final int updated;

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + tableName //
					+ " SET" //
					+ " password_salt = ?," //
					+ " password_hash = ?" //
					+ " WHERE username = ?" //
			)) {

				setString(pstmt, 1, passwordSalt);
				setString(pstmt, 2, passwordHash);
				setString(pstmt, 3, username);

				updated = pstmt.executeUpdate();
			}

			if (updated == 0) {

				try (PreparedStatement pstmt = cxn.prepareStatement("INSERT INTO " + tableName //
						+ " (username, password_salt, password_hash)" //
						+ " VALUES (?, ?, ?)" //
				)) {

					setString(pstmt, 1, username);
					setString(pstmt, 2, passwordSalt);
					setString(pstmt, 3, passwordHash);

					pstmt.executeUpdate();
				}
			}
		}
	}

	@Override
	public void removeUserPassword(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("DELETE FROM " + tableName //
					+ " WHERE username = ?" //
			)) {

				setString(pstmt, 1, username);

				pstmt.executeUpdate();
			}
		}
	}

	@Override
	@Nullable
	public String getUsernameByAuthorization(final String authorization, //
			final DateTime updatedAt //
	) throws SQLException, IOException {

		checkNotNull(authorization, "authorization");
		checkNotNull(updatedAt, "updatedAt");

		// throw new NotImplementedException("");

		return null;
	}

	@Override
	@Nullable
	public String getUsernameBySessionId(final String userSessionId, //
			final DateTime updatedAt //
	) throws SQLException, IOException {

		checkNotNull(userSessionId, "userSessionId");
		checkNotNull(updatedAt, "updatedAt");

		final long startMs = System.currentTimeMillis();

		if (debug) {
			System.out.println(AuthDaoInRDS.class.getSimpleName() + ".getUsernameBySessionId(), userSessionId: "
					+ userSessionId + "...");
		}

		final String username;

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + sessionsTableName //
					+ " SET" //
					+ " updated_at = ?" //
					+ " WHERE user_session_id = ?" //
					+ " AND expired_at IS NULL" //
			)) {

				setDateTime(pstmt, 1, updatedAt);
				setString(pstmt, 2, userSessionId);

				final int updated = pstmt.executeUpdate();

				if (updated == 0) {
					return null;
				}
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + sessionsTableName //
					+ " SET" //
					+ " expired_at = ?" //
					+ " WHERE user_session_id = ?" //
					+ " AND updated_at >= expires_at" //
			)) {

				setDateTime(pstmt, 1, updatedAt);
				setString(pstmt, 2, userSessionId);

				final int updated = pstmt.executeUpdate();

				if (updated != 0) {
					return null;
				}
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + sessionsTableName //
					+ " SET" //
					+ " expires_at = ?" //
					+ " WHERE user_session_id = ?" //
					+ " AND expired_at IS NULL" //
			)) {

				setDateTime(pstmt, 1, updatedAt.plusMinutes(SESSION_TIMEOUT_MINUTES));
				setString(pstmt, 2, userSessionId);

				final int updated = pstmt.executeUpdate();

				if (updated == 0) {
					return null;
				}
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " username" //
					+ " FROM " + sessionsTableName //
					+ " WHERE user_session_id = ?" //
					+ " AND expired_at IS NULL" //
			)) {

				setString(pstmt, 1, userSessionId);

				try (ResultSet rs = pstmt.executeQuery()) {

					if (rs.next()) {
						username = getString(rs, 1);
					} else {
						return null;
					}
				}
			}
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (debug) {
			System.out
					.println(AuthDaoInRDS.class.getSimpleName() + ".getUsernameBySessionId(), elapsedMs: " + elapsedMs);
		}

		return username;
	}

	@Override
	public boolean isValidUserPassword(final String username, final String password) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(password, "password");

		try (Connection cxn = getConnection()) {

			final Set<String> passwordSalts = newHashSet();

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " password_salt" //
					+ " FROM " + tableName //
					+ " WHERE username = ?" //
			)) {

				setString(pstmt, 1, username);

				try (ResultSet rs = pstmt.executeQuery()) {

					while (rs.next()) {

						final String passwordSalt = rs.getString(1);

						passwordSalts.add(passwordSalt);
					}
				}
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " 1" //
					+ " FROM " + tableName //
					+ " WHERE username = ?" //
					+ " AND password_salt = ?" //
					+ " AND password_hash = ?" //
			)) {

				setString(pstmt, 1, username);

				for (final String passwordSalt : passwordSalts) {

					setString(pstmt, 2, passwordSalt);

					final String passwordHash = hashPassword(passwordSalt, password);

					setString(pstmt, 3, passwordHash);

					try (ResultSet rs = pstmt.executeQuery()) {

						// Return true if and only if there is at least one such record in the database.

						if (rs.next()) {

							return true;
						}
					}
				}
			}
		}

		return false;
	}

	@Override
	public UserSessionDto newUserSession(final String username, final DateTime createdAt)
			throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(createdAt, "createdAt");

		final DateTime expiresAt = createdAt.plusMinutes(SESSION_TIMEOUT_MINUTES);

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("INSERT INTO " + sessionsTableName //
					+ " (user_session_id," //
					+ " username," //
					+ " created_at," //
					+ " updated_at," //
					+ " expires_at)" //
					+ " VALUES (?, ?, ?, ?, ?)" //
			)) {

				setString(pstmt, 2, username);
				setDateTime(pstmt, 3, createdAt);
				setDateTime(pstmt, 4, createdAt);
				setDateTime(pstmt, 5, expiresAt);

				final String userSessionId = retryUntil(4_000, 0, () -> {

					final String newSessionId = "S-" //
							+ System.currentTimeMillis() + "-" //
							+ randomAlphanumeric(20);

					try {

						setString(pstmt, 1, newSessionId);

						pstmt.executeUpdate();

					} catch (final SQLIntegrityConstraintViolationException e) {

						return null;

					} catch (final SQLException e) {

						if (isPSQLUniqueViolation(e)) {

							return null;
						}

						throw e;
					}

					return newSessionId;

				});

				return instantiate(MutableUserSessionDto.class) //
						.setUserSessionId(userSessionId).setUsername(username) //
						.setCreatedAt(createdAt) //
						.setUpdatedAt(createdAt) //
						.setExpiresAt(expiresAt);
			}
		}
	}

	@Override
	@Nullable
	public UserSessionDto getUserSession(final String userSessionId, //
			final DateTime updatedAt //
	) throws SQLException, IOException {

		checkNotNull(userSessionId, "userSessionId");
		checkNotNull(updatedAt, "updatedAt");

		final UserSessionDto dto;

		try (Connection cxn = getConnection()) {

			cxn.setAutoCommit(false);

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + sessionsTableName //
					+ " SET" //
					+ " updated_at = ?," //
					+ " expired_at = ?" //
					+ " WHERE user_session_id = ?" //
					+ " AND ? >= expires_at" //
					+ " AND expired_at IS NULL" //
			)) {

				setDateTime(pstmt, 1, updatedAt);
				setDateTime(pstmt, 2, updatedAt);
				setString(pstmt, 3, userSessionId);
				setDateTime(pstmt, 4, updatedAt);

				pstmt.executeUpdate();
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + sessionsTableName //
					+ " SET" //
					+ " updated_at = ?," //
					+ " expires_at = ?" //
					+ " WHERE user_session_id = ?" //
					+ " AND expired_at IS NULL" //
			)) {

				setDateTime(pstmt, 1, updatedAt);
				setDateTime(pstmt, 2, updatedAt.plusMinutes(SESSION_TIMEOUT_MINUTES));
				setString(pstmt, 3, userSessionId);

				pstmt.executeUpdate();
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " user_session_id," //
					+ " username," //
					+ " created_at," //
					+ " updated_at," //
					+ " expires_at," //
					+ " expired_at"

					+ " FROM " + sessionsTableName //
					+ " WHERE user_session_id = ?" //
			)) {

				setString(pstmt, 1, userSessionId);

				try (ResultSet rs = pstmt.executeQuery()) {

					if (!rs.next()) {
						return null;
					}

					dto = resultSet2UserSessionDto(rs);
				}
			}

			cxn.commit();
		}

		return dto;
	}

	@Override
	public void terminateSession(final String userSessionId, //
			@Nullable final DateTime updatedAt, //
			final DateTime expiredAt //
	) throws SQLException, IOException {

		checkNotNull(userSessionId, "userSessionId");
		checkNotNull(expiredAt, "expiredAt");

		try (Connection cxn = getConnection()) {

			if (updatedAt != null) {

				try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + sessionsTableName //
						+ " SET" //
						+ " updated_at = ?," //
						+ " expired_at = ?" //
						+ " WHERE user_session_id = ?" //
						+ " AND expired_at IS NULL" //
				)) {

					setDateTime(pstmt, 1, updatedAt);
					setDateTime(pstmt, 2, expiredAt);
					setString(pstmt, 3, userSessionId);

					pstmt.executeUpdate();
				}

			} else {

				try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + sessionsTableName //
						+ " SET" //
						+ " expired_at = ?" //
						+ " WHERE user_session_id = ?" //
						+ " AND expired_at IS NULL" //
				)) {

					setDateTime(pstmt, 1, expiredAt);
					setString(pstmt, 2, userSessionId);

					pstmt.executeUpdate();
				}
			}
		}
	}

	@Override
	@Nullable
	public DateTime getLastActiveAt(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " MAX(last_active_at)" //
					+ " FROM " + tableName //
					+ " WHERE username = ?" //
			)) {

				setString(pstmt, 1, username);

				try (ResultSet rs = pstmt.executeQuery()) {

					if (!rs.next()) {
						return null;
					}

					return getDateTime(rs, 1);
				}
			}
		}
	}

	private static UserSessionDto resultSet2UserSessionDto(final ResultSet rs) throws SQLException {

		return instantiate(MutableUserSessionDto.class) //
				.setUserSessionId(getString(rs, "user_session_id")) //
				.setUsername(getString(rs, "username")) //
				.setCreatedAt(getDateTime(rs, "created_at")) //
				.setUpdatedAt(getDateTime(rs, "updated_at")) //
				.setExpiresAt(getDateTime(rs, "expires_at")) //
				.setExpiredAt(getDateTime(rs, "expired_at"));
	}

	@Override
	public UserSessionsDto getUserSessions(final UserSessionsDtoQuery query) throws SQLException, IOException {

		checkNotNull(query, "query");

		final String sqlWhereClause = SqlWhereClause //
				.build(query.getFiltering(), UserSessionFiltering.Field.class) //
				.getSQL(" WHERE");

		final MutableUserSessionsDto sessions = instantiate(MutableUserSessionsDto.class) //
				.setSqlWhereClause(sqlWhereClause);

		final String orderDirective = toSQLOrderByDirective(query.getSortBys());

		final String limitClause = toSQLLimitClause(query.getStart(), query.getLimit());

		final int total;

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " COUNT(1)" //
					+ " FROM " + sessionsTableName //
					+ sqlWhereClause)) {

				try (ResultSet rs = pstmt.executeQuery()) {

					if (rs.next()) {

						total = getInt(rs, 1);

					} else {

						throw new IllegalStateException();
					}
				}
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " user_session_id," //
					+ " username," //
					+ " created_at," //
					+ " updated_at," //
					+ " expires_at," //
					+ " expired_at"

					+ " FROM " + sessionsTableName //
					+ sqlWhereClause //
					+ orderDirective //
					+ limitClause)) {

				try (ResultSet rs = pstmt.executeQuery()) {

					while (rs.next()) {

						final UserSessionDto session = resultSet2UserSessionDto(rs);

						sessions.addToResults(session);
					}
				}
			}
		}

		return sessions.setTotal(total);
	}
}
