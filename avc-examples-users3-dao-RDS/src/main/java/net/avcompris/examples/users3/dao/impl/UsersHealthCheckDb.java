package net.avcompris.examples.users3.dao.impl;

import static net.avcompris.examples.users3.dao.impl.DbTables.AUTH;
import static net.avcompris.examples.users3.dao.impl.DbTables.CORRELATIONS;
import static net.avcompris.examples.users3.dao.impl.DbTables.USERS;

import javax.sql.DataSource;

import net.avcompris.commons3.dao.impl.HealthCheckDbUtils;
import net.avcompris.commons3.dao.impl.MutableHealthCheckDb;

public abstract class UsersHealthCheckDb {

	public static void populateRuntimeDbStatus( //
			final MutableHealthCheckDb healthCheck, //
			final DataSource dataSource, //
			final String dbTableNamePrefix) {

		HealthCheckDbUtils.populateRuntimeDbStatus( //
				healthCheck, //
				dataSource, //
				dbTableNamePrefix, //
				AUTH, //
				CORRELATIONS, //
				USERS);
	}
}
