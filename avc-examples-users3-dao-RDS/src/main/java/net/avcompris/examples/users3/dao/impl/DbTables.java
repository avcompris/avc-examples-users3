package net.avcompris.examples.users3.dao.impl;

import static net.avcompris.commons3.dao.DbTable.Type.BOOLEAN;
import static net.avcompris.commons3.dao.DbTable.Type.INTEGER;
import static net.avcompris.commons3.dao.DbTable.Type.TIMESTAMP_WITH_TIMEZONE;
import static net.avcompris.commons3.dao.DbTable.Type.VARCHAR;
import static net.avcompris.commons3.dao.DbTablesUtils.column;

import java.util.Arrays;

import net.avcompris.commons3.dao.DbTable;
import net.avcompris.commons3.dao.DbTablesUtils.Column;

public enum DbTables implements DbTable {

	CORRELATIONS( //
			column("correlation_id") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.build(), //
			column("created_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build()),

	USERS( //
			column("username") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.build(), //
			column("rolename") //
					.type(VARCHAR) //
					.size(255) //
					.regexp("[A-Z]+(_[A-Z0-9_])*") //
					.notNull() //
					.index() //
					.build(),
			column("preferred_lang") //
					.type(VARCHAR) //
					.size(10) //
					.regexp("[a-z]+") //
					.nullable() //
					.index() //
					.build(),
			column("preferred_timezone") //
					.type(VARCHAR) //
					.size(10) //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.nullable() //
					.index() //
					.build(),
			column("enabled") //
					.type(BOOLEAN) //
					.notNull() //
					.index() //
					.build(),
			column("created_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build(), //
			column("updated_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build(), //
			column("revision") //
					.type(INTEGER) //
					.notNull() //
					.build(), //
			column("last_active_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.nullable() //
					.build()),

	AUTH( //
			column("username") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.refKey(USERS, "username", true) //
					.build(), //
			column("password_salt") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z0-9]*") //
					.build(),
			column("password_hash") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z0-9]*") //
					.build()),

	AUTH_SESSIONS( //
			column("user_session_id") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.primaryKey() //
					.build(), //
			column("username") //
					.type(VARCHAR) //
					.size(255) //
					.notNull() //
					.regexp("[a-zA-Z]+[a-zA-Z0-9-_]*") //
					.refKey(USERS, "username", true) //
					.build(), //
			column("created_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build(), //
			column("updated_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build(), //
			column("expires_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.notNull() //
					.build(), //
			column("expired_at") //
					.type(TIMESTAMP_WITH_TIMEZONE) //
					.nullable() //
					.build());

	private final Column[] columns;

	private DbTables(final Column... columns) {

		this.columns = columns;
	}

	@Override
	public Column[] columns() {

		return Arrays.copyOf(columns, columns.length);
	}
}
