package net.avcompris.examples.users3.dao.impl;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ConcurrentModificationException;

import javax.annotation.Nullable;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import net.avcompris.commons.query.impl.SqlWhereClause;
import net.avcompris.commons3.dao.exception.DuplicateEntityException;
import net.avcompris.commons3.dao.impl.AbstractDaoInRDS;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.users3.dao.UserDto;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.UsersDto;
import net.avcompris.examples.users3.dao.UsersDtoQuery;
import net.avcompris.examples.users3.query.UserFiltering;

@Component
public final class UsersDaoInRDS extends AbstractDaoInRDS implements UsersDao {

	private static final Log logger = LogFactory.getLog(UsersDaoInRDS.class);

	/**
	 * Alias to: "tableName".
	 */
	private final String usersTableName;

	private final String sessionsTableName;

	@Autowired
	public UsersDaoInRDS( //
			@Value("#{rds.dataSource}") final DataSource dataSource,
			@Value("#{rds.tableNames.users}") final String tableName, //
			@Value("#{rds.tableNames.auth}") final String authTableName, //
			final Clock clock) {

		super(dataSource, tableName, clock);

		this.usersTableName = tableName;
		this.sessionsTableName = authTableName + "_sessions";
	}

	@Override
	public UsersDto getUsers(final UsersDtoQuery query) throws SQLException, IOException {

		checkNotNull(query, "query");

		final String sqlWhereClause = SqlWhereClause //
				.build(query.getFiltering(), UserFiltering.Field.class) //
				.getSQL(" WHERE");

		final MutableUsersDto users = instantiate(MutableUsersDto.class) //
				.setSqlWhereClause(sqlWhereClause);

		final String orderDirective = toSQLOrderByDirective(query.getSortBys());

		final String limitClause = toSQLLimitClause(query.getStart(), query.getLimit());

		final int total;

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " COUNT(1)" //
					+ " FROM " + usersTableName //
					+ sqlWhereClause)) {

				try (ResultSet rs = pstmt.executeQuery()) {

					if (rs.next()) {

						total = getInt(rs, 1);

					} else {

						throw new IllegalStateException();
					}
				}
			}

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " username," //
					+ " rolename," //
					+ " preferred_lang," //
					+ " preferred_timezone," //
					+ " enabled," //
					+ " created_at," //
					+ " updated_at," //
					+ " last_active_at," //
					+ " revision" //

					+ " FROM " + usersTableName //
					+ sqlWhereClause //
					+ orderDirective //
					+ limitClause)) {

				try (ResultSet rs = pstmt.executeQuery()) {

					while (rs.next()) {

						final UserDto user = resultSet2UserDto(rs);

						users.addToResults(user);
					}
				}
			}
		}

		return users.setTotal(total);
	}

	@Override
	public void createUser(final String username, //
			final String rolename, //
			@Nullable final String preferredLang, //
			@Nullable final String preferredTimeZone, //
			final boolean enabled //
	) throws SQLException, IOException, DuplicateEntityException {

		checkNotNull(username, "username");
		checkNotNull(rolename, "rolename");

		final DateTime now = clock.now();

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("INSERT INTO " + tableName //
					+ " (username," //
					+ " rolename," //
					+ " preferred_lang," //
					+ " preferred_timezone," //
					+ " enabled," //
					+ " created_at," //
					+ " updated_at," //
					+ " revision)" //
					+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?)" //
			)) {

				setString(pstmt, 1, username);
				setString(pstmt, 2, rolename);
				setString(pstmt, 3, preferredLang);
				setString(pstmt, 4, preferredTimeZone);
				setBoolean(pstmt, 5, enabled);
				setDateTime(pstmt, 6, now);
				setDateTime(pstmt, 7, now);
				setInt(pstmt, 8, 1);

				try {

					pstmt.executeUpdate();

				} catch (final SQLIntegrityConstraintViolationException e) {

					throw new DuplicateEntityException("username: " + username, e);

				} catch (final SQLException e) {

					if (isPSQLUniqueViolation(e)) {

						throw new DuplicateEntityException("username: " + username, e);
					}

					throw e;
				}
			}
		}
	}

	@Override
	@Nullable
	public UserDto getUser(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("SELECT" //
					+ " username," //
					+ " rolename," //
					+ " preferred_lang," //
					+ " preferred_timezone," //
					+ " enabled," //
					+ " created_at," //
					+ " updated_at," //
					+ " revision," //

					+ "(SELECT MAX(updated_at)" //
					+ " FROM " + sessionsTableName //
					+ " WHERE username = ?" //
					+ ") AS last_active_at" //

					+ " FROM " + tableName //
					+ " WHERE username = ?" //
			)) {

				setString(pstmt, 1, username);
				setString(pstmt, 2, username);

				try (ResultSet rs = pstmt.executeQuery()) {

					if (!rs.next()) {

						return null;
					}

					return resultSet2UserDto(rs);
				}
			}
		}
	}

	private static UserDto resultSet2UserDto(final ResultSet rs) throws SQLException {

		return instantiate(MutableUserDto.class) //
				.setUsername(getString(rs, "username")) //
				.setRolename(getString(rs, "rolename")) //
				.setPreferredLang(getString(rs, "preferred_lang")) //
				.setPreferredTimeZone(getString(rs, "preferred_timezone")) //
				.setEnabled(getBoolean(rs, "enabled")) //
				.setCreatedAt(getDateTime(rs, "created_at")) //
				.setUpdatedAt(getDateTime(rs, "updated_at")) //
				.setLastActiveAt(getDateTime(rs, "last_active_at")) //
				.setRevision(getInt(rs, "revision"));
	}

	@Override
	public void updateUser(final String username, //
			final String rolename, //
			@Nullable final String preferredLang, //
			@Nullable final String preferredTimeZone, //
			final boolean enabled, //
			final int fromRevision //
	) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(rolename, "rolename");

		final DateTime now = clock.now();

		final int updated;

		try (Connection cxn = getConnection()) {

			cxn.setAutoCommit(false);

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + tableName //
					+ " SET rolename = ?," //
					+ " enabled = ?," //
					+ " updated_at = ?," //
					+ " revision = ?" //
					+ " WHERE username = ?" //
					+ " AND revision = ?" //
			)) {

				setString(pstmt, 1, rolename);
				setBoolean(pstmt, 2, enabled);
				setDateTime(pstmt, 3, now);
				setInt(pstmt, 4, fromRevision + 1);
				setString(pstmt, 5, username);
				setInt(pstmt, 6, fromRevision);

				updated = pstmt.executeUpdate();
			}

			if (updated != 1) {
				throw new ConcurrentModificationException("username: " + username + ", fromRevision: " + fromRevision);
			}

			if (preferredLang != null) {

				try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + tableName //
						+ " SET preferred_lang = ?" //
						+ " WHERE username = ?" //
						+ " AND revision = ?" //
				)) {

					setString(pstmt, 1, preferredLang);
					setString(pstmt, 2, username);
					setInt(pstmt, 3, fromRevision + 1);

					pstmt.executeUpdate();
				}
			}

			if (preferredTimeZone != null) {

				try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE " + tableName //
						+ " SET preferred_timezone = ?" //
						+ " WHERE username = ?" //
						+ " AND revision = ?" //
				)) {

					setString(pstmt, 1, preferredTimeZone);
					setString(pstmt, 2, username);
					setInt(pstmt, 3, fromRevision + 1);

					pstmt.executeUpdate();
				}
			}

			cxn.commit();
		}
	}

	@Override
	public void deleteUser(final String username) throws SQLException, IOException {

		checkNotNull(username, "username");

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("DELETE FROM " + tableName //
					+ " WHERE username = ?" //
			)) {

				setString(pstmt, 1, username);

				pstmt.executeUpdate();
			}
		}
	}

	@Override
	public void setLastActiveAt(final String username, final DateTime lastActiveAt) throws SQLException, IOException {

		checkNotNull(username, "username");
		checkNotNull(lastActiveAt, "lastActiveAt");

		final long startMs = System.currentTimeMillis();

		if (logger.isDebugEnabled()) {
			logger.debug("setLastActiveAt(), username: " + username + "...");
		}

		try (Connection cxn = getConnection()) {

			try (PreparedStatement pstmt = cxn.prepareStatement("UPDATE  " + tableName //
					+ " SET last_active_at = ?" //
					+ " WHERE username = ?" //
			)) {

				setDateTime(pstmt, 1, lastActiveAt);
				setString(pstmt, 2, username);

				pstmt.executeUpdate();
			}
		}

		final long elapsedMs = System.currentTimeMillis() - startMs;

		if (logger.isDebugEnabled()) {
			logger.debug("setLastActiveAt(), elapsedMs: " + elapsedMs);
		}
	}
}
