package net.avcompris.examples.users3.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.EntitiesQuery;
import net.avcompris.examples.users3.query.UserFiltering;

public interface UsersQuery extends EntitiesQuery<UserFiltering, UserFiltering.Field> {

	@Override
	SortBy[] getSortBys();

	@Override
	Expand[] getExpands();

	UsersQuery setFiltering(@Nullable UserFiltering filtering);

	UsersQuery setSortBys(SortBy... sortBys);

	UsersQuery setExpands(Expand... expands);

	enum SortBy {

		SORT_BY_USERNAME, SORT_BY_USERNAME_DESC, //
		SORT_BY_ROLE, SORT_BY_ROLE_DESC, //
		SORT_BY_ENABLED, SORT_BY_ENABLED_DESC, //
		SORT_BY_CREATED_AT, SORT_BY_CREATED_AT_DESC, //
		SORT_BY_UPDATED_AT, SORT_BY_UPDATED_AT_DESC, //
		SORT_BY_LAST_ACTIVE_AT, SORT_BY_LAST_ACTIVE_AT_DESC, //
		SORT_BY_REVISION, SORT_BY_REVISION_DESC, //
	}

	interface Expand {

	}

	UsersQuery setStart(@Nullable Integer start);

	UsersQuery setLimit(@Nullable Integer limit);
}
