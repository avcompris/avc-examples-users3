package net.avcompris.examples.users3.api;

public interface Credentials {

	String getUsername();

	String getPassword();

	Credentials setUsername(String username);

	Credentials setPassword(String password);
}
