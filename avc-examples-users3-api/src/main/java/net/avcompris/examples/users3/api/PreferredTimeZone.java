package net.avcompris.examples.users3.api;

public interface PreferredTimeZone {

	String getTimeZone();

	PreferredTimeZone setLang(String timeZone);
}
