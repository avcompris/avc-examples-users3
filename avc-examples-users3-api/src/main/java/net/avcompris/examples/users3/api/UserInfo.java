package net.avcompris.examples.users3.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.Entity;
import net.avcompris.commons3.types.DateTimeHolder;
import net.avcompris.examples.shared3.Role;

public interface UserInfo extends Entity {

	// @Override
	String getUsername();

	// @Override
	Role getRole();

	@Nullable
	String getPreferredLang();

	@Nullable
	String getPreferredTimeZone();

	boolean isEnabled();

	@Nullable
	DateTimeHolder getLastActiveAt();
}
