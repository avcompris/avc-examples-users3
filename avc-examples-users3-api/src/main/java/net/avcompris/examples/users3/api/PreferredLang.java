package net.avcompris.examples.users3.api;

public interface PreferredLang {

	String getLang();

	PreferredLang setLang(String lang);
}
