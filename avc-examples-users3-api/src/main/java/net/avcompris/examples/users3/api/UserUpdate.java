package net.avcompris.examples.users3.api;

import javax.annotation.Nullable;

import net.avcompris.commons3.api.EntityUpdate;
import net.avcompris.examples.shared3.Role;

public interface UserUpdate extends EntityUpdate {

	@Nullable
	String getPassword();

	@Nullable
	Role getRole();

	@Nullable
	String getPreferredLang();

	@Nullable
	String getPreferredTimeZone();

	@Nullable
	Boolean isEnabled();

	UserUpdate setPassword(@Nullable String password);

	UserUpdate setRole(@Nullable Role role);

	UserUpdate setPreferredLang(@Nullable String preferredLang);

	UserUpdate setPreferredTimeZone(@Nullable String preferredTimeZone);

	UserUpdate setEnabled(@Nullable Boolean enabled);

	@Override
	UserUpdate setFromRevision(int revision);
}
