package net.avcompris.examples.users3.api;

import net.avcompris.commons3.api.Entities;

public interface UsersInfo extends Entities<UserInfo> {

	@Override
	UserInfo[] getResults();
}
