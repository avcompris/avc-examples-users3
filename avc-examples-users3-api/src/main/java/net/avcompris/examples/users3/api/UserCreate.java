package net.avcompris.examples.users3.api;

import javax.annotation.Nullable;

import net.avcompris.examples.shared3.Role;

public interface UserCreate {

	@Nullable
	String getPassword();

	Role getRole();

	@Nullable
	String getPreferredLang();

	@Nullable
	String getPreferredTimeZone();

	boolean isEnabled();

	UserCreate setPassword(@Nullable String password);

	UserCreate setRole(Role role);

	UserCreate setPreferredLang(@Nullable String preferredLang);

	UserCreate setPreferredTimeZone(@Nullable String preferredTimeZone);

	UserCreate setEnabled(boolean enabled);
}
