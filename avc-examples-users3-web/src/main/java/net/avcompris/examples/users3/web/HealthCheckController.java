package net.avcompris.examples.users3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.dao.impl.AbstractDaoInRDS;
import net.avcompris.commons3.databeans.DataBeans;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.commons3.web.HealthCheck;
import net.avcompris.commons3.web.MutableHealthCheck;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.impl.UsersHealthCheckDb;
import net.avcompris.examples.users3.web.ApplicationConfig.RDSConfig;

@RestController
public final class HealthCheckController extends MyAbstractController {

	@SuppressWarnings("unused")
	private static final Log logger = LogFactory.getLog(HealthCheckController.class);

	private final ApplicationContext applicationContext;
	private final UsersDao usersDao;

	@Autowired
	public HealthCheckController(final CorrelationService correlationService, //
			final ApplicationContext applicationContext, //
			final SessionPropagator sessionPropagator, //
			final UsersDao usersDao, //
			final Clock clock) {

		super(correlationService, sessionPropagator, clock);

		this.applicationContext = checkNotNull(applicationContext, "applicationContext");
		this.usersDao = checkNotNull(usersDao, "usersDao");
	}

	@RequestMapping(value = "/healthcheck", method = GET)
	public ResponseEntity<HealthCheck> getHealthCheck(final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapNonAuthenticatedWithoutCorrelationId(request, () -> {

			final MutableHealthCheck healthCheck = DataBeans.instantiate(MutableHealthCheck.class) //
					.setOk(true) //
					.setComponentName(Application.NAME);

			if (usersDao instanceof AbstractDaoInRDS) {

				final RDSConfig rdsConfig = applicationContext.getBean("rds", RDSConfig.class);

				UsersHealthCheckDb.populateRuntimeDbStatus(healthCheck, //
						rdsConfig.getDataSource(), //
						rdsConfig.getTableNamePrefix());
			}

			return ResponseEntity.status(healthCheck.isOk() //
					? HttpStatus.OK //
					: HttpStatus.SERVICE_UNAVAILABLE) //
					.body(healthCheck);
		});
	}
}
