package net.avcompris.examples.users3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.dao.DbTablesUtils.composeActualDbTableName;
import static net.avcompris.examples.users3.dao.impl.DbTables.AUTH;
import static net.avcompris.examples.users3.dao.impl.DbTables.CORRELATIONS;
import static net.avcompris.examples.users3.dao.impl.DbTables.USERS;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.avcompris.commons3.web.AbstractApplicationConfig;

@Configuration
public class ApplicationConfig extends AbstractApplicationConfig {

	private final RDSConfig rds = new RDSConfig();

	@Bean(name = "rds")
	public RDSConfig getRDS() {

		return rds;
	}

	public static final class RDSConfig extends AbstractRDSConfig {

		public RDSConfig() {

			super( //
					"rds.url", //
					"rds.username", //
					"rds.password", //
					"rds.tableNamePrefix");
		}

		public TableNames getTableNames() {

			return new TableNames(getTableNamePrefix());
		}
	}

	public static final class TableNames {

		private final String tableNamePrefix;

		private TableNames(final String tableNamePrefix) {

			this.tableNamePrefix = checkNotNull(tableNamePrefix, "tableNamePrefix");
		}

		public String getAuth() {

			return composeActualDbTableName(tableNamePrefix, AUTH);
		}

		public String getCorrelations() {

			return composeActualDbTableName(tableNamePrefix, CORRELATIONS);
		}

		public String getUsers() {

			return composeActualDbTableName(tableNamePrefix, USERS);
		}
	}
}
