package net.avcompris.examples.users3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.examples.users3.web.Application.API;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.avcompris.commons3.api.EntitiesQueryRaw;
import net.avcompris.commons3.api.User;
import net.avcompris.commons3.api.UserSession;
import net.avcompris.commons3.api.UserSessions;
import net.avcompris.commons3.api.UserSessionsQuery;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.api.exception.UnauthenticatedException;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.AuthService;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.examples.users3.api.Credentials;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.query.UserFiltering;

@RestController
public final class AuthController extends MyAbstractController {

	private final AuthService authService;

	@Autowired
	public AuthController(final CorrelationService correlationService, final AuthService authService,
			final SessionPropagator sessionPropagator, final Clock clock) {

		super(correlationService, sessionPropagator, clock);

		this.authService = checkNotNull(authService, "authService");
	}

	@RequestMapping(value = API + "/auth", method = POST)
	public ResponseEntity<UserSession> authenticateUser(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestBody(required = true) final Credentials credentials //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final String username = credentials.getUsername();
			final String password = credentials.getPassword();

			final UserSession userSession = authService.authenticate(correlationId, username, password);

			if (userSession != null) {
				setUserSessionCookie(response, userSession.getUserSessionId());
			}

			return ResponseEntity.status(userSession != null //
					? HttpStatus.OK //
					: HttpStatus.NO_CONTENT) //
					.body(userSession);
		});
	}

	@RequestMapping(value = API + "/logout", method = { GET, POST })
	public ResponseEntity<UserSession> logout(final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final Object userSessionId = request.getAttribute(USER_SESSION_ID_ATTRIBUTE_NAME);

			if (userSessionId == null) {

				throw new UnauthenticatedException();
			}

			final UserSession userSession = authService.terminateMySession(correlationId, user,
					userSessionId.toString());

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userSession);
		});
	}

	/**
	 * Return {@link User}, not {@link UserInfo}, because otherwise it cannot work
	 * with superadmin auth.
	 */
	@RequestMapping(value = API + "/auth", method = GET)
	public ResponseEntity<User> getAuthenticatedUser(final HttpServletRequest request, //
			@RequestHeader(required = false, name = HttpHeaders.AUTHORIZATION) final String authorizationHeader, //
			@CookieValue(required = false, value = "user_session_id") final String userSessionIdCookie,
			@RequestHeader(required = false, value = "user_session_id") final String userSessionIdHeader //
	) throws ServiceException {

		return wrapNonAuthenticated(request, (correlationId) -> {

			final String authorization = authorizationHeader;

			final String userSessionId = userSessionIdHeader != null //
					? userSessionIdHeader //
					: userSessionIdCookie;

			final User user = authService.getAuthenticatedUser(authorization, userSessionId);

			if (user == null) {

				return ResponseEntity.status(HttpStatus.NO_CONTENT) //
						.body(null);
			}

			return ResponseEntity.status(HttpStatus.OK) //
					.body(user);
		});
	}

	/**
	 * Return {@link User}, not {@link UserInfo}, because otherwise it cannot work
	 * with superadmin auth.
	 */
	@RequestMapping(value = API + "/active", method = POST)
	public ResponseEntity<User> setActive( //
			final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			authService.setLastActiveAt(correlationId, user);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(user);
		});
	}

	@RequestMapping(value = API + "/sessions", method = GET)
	public ResponseEntity<UserSessions> getSessions(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestParam(name = "q", required = false) final String q, //
			@RequestParam(name = "sort", required = false) final String sort, //
			@RequestParam(name = "start", required = false) final Integer start, //
			@RequestParam(name = "limit", required = false) final Integer limit, //
			@RequestParam(name = "expand", required = false) final String expand //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserSessionsQuery query = authService.validateUserSessionsQuery(correlationId, user, q, sort, start,
					limit, expand);

			final UserSessions sessions = authService.getUserSessions(correlationId, user, query);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(sessions);
		});
	}

	@RequestMapping(value = API + "/sessions", method = POST)
	public ResponseEntity<UserSessions> getSessions(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestBody(required = true) final EntitiesQueryRaw<UserFiltering, UserFiltering.Field> raw //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserSessionsQuery query = authService.validateUserSessionsQuery(correlationId, user, raw.getQ(),
					raw.getSort(), raw.getStart(), raw.getLimit(), raw.getExpand());

			final UserSessions sessions = authService.getUserSessions(correlationId, user, query);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(sessions);
		});
	}

	@RequestMapping(value = API + "/sessions/{userSessionId}", method = GET)
	public ResponseEntity<UserSession> getUserSession(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "userSessionId", required = true) final String userSessionId //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserSession userSession = authService.getUserSession(correlationId, user, userSessionId);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userSession);
		});
	}

	@RequestMapping(value = API + "/sessions/{userSessionId}/terminate", method = POST)
	public ResponseEntity<UserSession> terminateUserSession(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "userSessionId", required = true) final String userSessionId //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserSession userSession = authService.terminateUserSession(correlationId, user, userSessionId);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userSession);
		});
	}
}
