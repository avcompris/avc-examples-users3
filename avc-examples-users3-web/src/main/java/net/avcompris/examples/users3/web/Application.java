package net.avcompris.examples.users3.web;

import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.beust.jcommander.JCommander;

import net.avcompris.commons3.api.EntitiesQueryRaw;
import net.avcompris.commons3.client.DataBeanDeserializer;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.impl.PermissionsImpl;
import net.avcompris.commons3.dao.CorrelationDao;
import net.avcompris.commons3.dao.impl.CorrelationDaoInMemory;
import net.avcompris.commons3.notifier.impl.ErrorNotifierInMemory;
import net.avcompris.commons3.utils.ClockImpl;
import net.avcompris.commons3.web.AbstractApplication;
import net.avcompris.commons3.web.AbstractConfig;
import net.avcompris.commons3.web.ApplicationUtils;
import net.avcompris.examples.shared3.core.impl.CorrelationServiceImpl;
import net.avcompris.examples.shared3.dao.impl.CorrelationDaoInRDS;
import net.avcompris.examples.users3.api.Credentials;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserUpdate;
import net.avcompris.examples.users3.core.impl.AuthServiceImpl;
import net.avcompris.examples.users3.core.impl.UsersServiceImpl;
import net.avcompris.examples.users3.dao.AuthDao;
import net.avcompris.examples.users3.dao.UsersDao;
import net.avcompris.examples.users3.dao.impl.AuthDaoInMemory;
import net.avcompris.examples.users3.dao.impl.AuthDaoInRDS;
import net.avcompris.examples.users3.dao.impl.UsersDaoInMemory;
import net.avcompris.examples.users3.dao.impl.UsersDaoInRDS;

/**
 * Our Spring Boot applications.
 *
 * @author dandriana
 */
@EnableAutoConfiguration(exclude = { DataSourceAutoConfiguration.class })
@SpringBootApplication
public class Application extends AbstractApplication {

	public static final String NAME = "examples-users3";

	public static final String API = "/api/v1";

	/**
	 * Of the form "net.avcompris.commons3.examples.users".
	 */
	public static final String PACKAGE_PREFIX = substringBeforeLast(Application.class.getPackage().getName(), ".web");

	private static boolean secure = true;

	private static final class Config extends AbstractConfig {

		private Config(final ApplicationArgs myargs) {

			super(myargs);
		}

		static Config load(final ApplicationArgs myargs) {

			return new Config(myargs);
		}
	}

	public static void main(final String... args) throws Exception {

		// 1. APPLICATION ARGS

		final ApplicationArgs myargs = new ApplicationArgs();

		JCommander.newBuilder().addObject(myargs).build().parse(args);

		secure = !myargs.unsecure;

		final Config config = Config.load(myargs);

		// 2. SPRING BEANS

		final ApplicationContext context = SpringApplication.run(new Class<?>[] {

				Application.class,

				ClockImpl.class,

				CorrelationServiceImpl.class,

				ErrorNotifierInMemory.class, // ErrorNotifierInRabbitMQ.class,

				AuthServiceImpl.class,

				PermissionsImpl.class,

				SessionPropagator.class,

				UsersServiceImpl.class,

				dao(AuthDao.class, //
						config.inMemory, AuthDaoInMemory.class, //
						config.rds, AuthDaoInRDS.class),

				dao(CorrelationDao.class, //
						config.inMemory, CorrelationDaoInMemory.class, //
						config.rds, CorrelationDaoInRDS.class),

				dao(UsersDao.class, //
						config.inMemory, UsersDaoInMemory.class, //
						config.rds, UsersDaoInRDS.class),

		}, args);

		ApplicationUtils.dumpBeans(context, System.out, "net.avcompris");
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {

		return new WebMvcConfigurerAdapter() {

			@Override
			public void addCorsMappings(final CorsRegistry registry) {

				registry.addMapping("/**").allowedMethods(

						"HEAD", "GET", "POST", "PUT", "DELETE" //
				);
			}
		};
	}

	public static boolean isSecure() {

		final boolean secure = Application.secure;

		if (!secure) {
			System.err.println("*** WARNING *** Application.secure = " + secure
					+ ", which means HTTP Cookies may be sent on non-SSL HTTP connections");
		}

		return secure;
	}

	public static boolean isHttpOnly() {

		return true;
	}

	/**
	 * Because we use interfaces for our DataBean classes (via our DataBeans utility), we
	 * must specify custom JSON deserializers here.
	 */
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer addCustomDeserialization() {

		return new Jackson2ObjectMapperBuilderCustomizer() {

			@Override
			public void customize(final Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder) {

				for (final Class<?> clazz : new Class<?>[] {

						Credentials.class,

						EntitiesQueryRaw.class,

						UserCreate.class, UserUpdate.class,

				}) {

					jackson2ObjectMapperBuilder.deserializerByType(clazz,
							DataBeanDeserializer.createDeserializer(clazz));
				}
			}
		};
	}
}
