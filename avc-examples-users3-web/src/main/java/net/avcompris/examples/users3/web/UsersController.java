package net.avcompris.examples.users3.web;

import static com.google.common.base.Preconditions.checkNotNull;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static net.avcompris.examples.users3.web.Application.API;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.avcompris.commons3.api.EntitiesQueryRaw;
import net.avcompris.commons3.api.exception.ServiceException;
import net.avcompris.commons3.client.SessionPropagator;
import net.avcompris.commons3.core.AuthService;
import net.avcompris.commons3.core.CorrelationService;
import net.avcompris.commons3.utils.Clock;
import net.avcompris.commons3.utils.LogFactory;
import net.avcompris.examples.users3.api.PreferredLang;
import net.avcompris.examples.users3.api.PreferredTimeZone;
import net.avcompris.examples.users3.api.UserCreate;
import net.avcompris.examples.users3.api.UserInfo;
import net.avcompris.examples.users3.api.UserUpdate;
import net.avcompris.examples.users3.api.UsersInfo;
import net.avcompris.examples.users3.api.UsersQuery;
import net.avcompris.examples.users3.core.api.UsersService;
import net.avcompris.examples.users3.query.UserFiltering;

@RestController
public final class UsersController extends MyAbstractController {

	private static final Log logger = LogFactory.getLog(UsersController.class);

	private final AuthService authService;
	private final UsersService usersService;

	@Autowired
	public UsersController(final CorrelationService correlationService, final AuthService authService,
			final UsersService usersService, //
			final SessionPropagator sessionPropagator, //
			final Clock clock) {

		super(correlationService, sessionPropagator, clock);

		this.authService = checkNotNull(authService, "authService");
		this.usersService = checkNotNull(usersService, "usersService");
	}

	@RequestMapping(value = API + "/users", method = GET)
	public ResponseEntity<UsersInfo> getUsers(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestParam(name = "q", required = false) final String q, //
			@RequestParam(name = "sort", required = false) final String sort, //
			@RequestParam(name = "start", required = false) final Integer start, //
			@RequestParam(name = "limit", required = false) final Integer limit, //
			@RequestParam(name = "expand", required = false) final String expand //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UsersQuery query = usersService.validateUsersQuery(correlationId, user, q, sort, start, limit,
					expand);

			final UsersInfo users = usersService.getUsers(correlationId, user, query);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(users);
		});
	}

	@RequestMapping(value = API + "/users", method = POST)
	public ResponseEntity<UsersInfo> getUsers(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestBody(required = true) final EntitiesQueryRaw<UserFiltering, UserFiltering.Field> raw //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UsersQuery query = usersService.validateUsersQuery(correlationId, user, raw.getQ(), raw.getSort(),
					raw.getStart(), raw.getLimit(), raw.getExpand());

			final UsersInfo users = usersService.getUsers(correlationId, user, query);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(users);
		});
	}

	@RequestMapping(value = API + "/users/{username}", method = POST)
	public ResponseEntity<UserInfo> createUser(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username, //
			@RequestBody(required = true) final UserCreate create //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo = usersService.createUser(correlationId, user, username, create);

			return ResponseEntity.status(HttpStatus.CREATED) //
					.body(userInfo);
		});
	}

	@RequestMapping(value = API + "/users/me", method = GET)
	public ResponseEntity<UserInfo> getUserMe(final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo = usersService.getUserMe(correlationId, user);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userInfo);
		});
	}

	@RequestMapping(value = API + "/users/{username}", method = GET)
	public ResponseEntity<UserInfo> getUser(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo;

			if (username.contentEquals(user.getUsername())) {

				userInfo = usersService.getUserMe(correlationId, user);

			} else {

				userInfo = usersService.getUser(correlationId, user, username);
			}

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userInfo);
		});
	}

	@RequestMapping(value = API + "/users/me/preferredLang", method = GET)
	public ResponseEntity<String> getUserMePreferredLang(final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo = usersService.getUserMe(correlationId, user);

			final String lang = userInfo.getPreferredLang();

			return ResponseEntity.status(lang != null //
					? HttpStatus.OK //
					: HttpStatus.NO_CONTENT) //
					.body(lang);
		});
	}

	@RequestMapping(value = API + "/users/me/preferredLang", method = { PUT, POST })
	public ResponseEntity<String> setUserMePreferredLang(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestBody(required = false) final PreferredLang preferredLang, //
			@RequestParam(name = "preferredLang", required = false) final String preferredLangParam //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			if (logger.isDebugEnabled()) {
				logger.debug("setUserMePreferredLang(), user: " + user.getUsername() + ", preferredLang: " + preferredLang
						+ ", preferredLangParam: " + preferredLangParam);
			}

			final String lang;

			if (preferredLangParam != null) {

				lang = preferredLangParam;

			} else if (preferredLang == null) {

				throw new IllegalArgumentException("lang should be set");

			} else {

				lang = preferredLang.getLang();
			}

			final int fromRevision = usersService.getUserMe(correlationId, user).getRevision();

			usersService.updateUserMe(correlationId, user, instantiate(UserUpdate.class) //
					.setPreferredLang(lang) //
					.setFromRevision(fromRevision));

			final UserInfo userInfo = usersService.getUserMe(correlationId, user);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userInfo.getPreferredLang());
		});
	}

	@RequestMapping(value = API + "/users/{username}/preferredLang", method = GET)
	public ResponseEntity<String> getUserPreferredLang(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo;

			if (username.contentEquals(user.getUsername())) {

				userInfo = usersService.getUserMe(correlationId, user);

			} else {

				userInfo = usersService.getUser(correlationId, user, username);
			}

			final String lang = userInfo.getPreferredLang();

			return ResponseEntity.status(lang != null //
					? HttpStatus.OK //
					: HttpStatus.NO_CONTENT) //
					.body(lang);
		});
	}

	@RequestMapping(value = API + "/users/{username}/preferredLang", method = { POST, PUT })
	public ResponseEntity<String> setUserPreferredLang(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username, //
			@RequestBody(required = false) final PreferredLang preferredLang, //
			@RequestParam(name = "preferredLang", required = false) final String preferredLangParam //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final String lang;

			if (preferredLangParam != null) {

				lang = preferredLangParam;

			} else if (preferredLang == null) {

				throw new IllegalArgumentException("lang should be set");

			} else {

				lang = preferredLang.getLang();
			}

			final UserInfo userInfo;

			if (username.contentEquals(user.getUsername())) {

				final int fromRevision = usersService.getUserMe(correlationId, user).getRevision();

				usersService.updateUserMe(correlationId, user, instantiate(UserUpdate.class) //
						.setPreferredLang(lang) //
						.setFromRevision(fromRevision));

				userInfo = usersService.getUserMe(correlationId, user);

			} else {

				final int fromRevision = usersService.getUser(correlationId, user, username).getRevision();

				usersService.updateUser(correlationId, user, username, instantiate(UserUpdate.class) //
						.setPreferredLang(lang) //
						.setFromRevision(fromRevision));

				userInfo = usersService.getUser(correlationId, user, username);
			}

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userInfo.getPreferredLang());
		});
	}

	@RequestMapping(value = API + "/users/me/preferredTimeZone", method = GET)
	public ResponseEntity<String> getUserMePreferredTimeZone(final HttpServletRequest request, //
			final HttpServletResponse response //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo = usersService.getUserMe(correlationId, user);

			final String timeZone = userInfo.getPreferredTimeZone();

			return ResponseEntity.status(timeZone != null //
					? HttpStatus.OK //
					: HttpStatus.NO_CONTENT) //
					.body(timeZone);
		});
	}

	@RequestMapping(value = API + "/users/me/preferredTimeZone", method = { PUT, POST })
	public ResponseEntity<String> setUserMePreferredTimeZone(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@RequestBody(required = false) final PreferredTimeZone preferredTimeZone, //
			@RequestParam(required = false) final String timeZoneParam //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final String timeZone;

			if (timeZoneParam != null) {

				timeZone = timeZoneParam;

			} else if (preferredTimeZone == null) {

				throw new IllegalArgumentException("timeZone should be set");

			} else {

				timeZone = preferredTimeZone.getTimeZone();
			}

			final int fromRevision = usersService.getUserMe(correlationId, user).getRevision();

			usersService.updateUserMe(correlationId, user, instantiate(UserUpdate.class) //
					.setPreferredTimeZone(timeZone) //
					.setFromRevision(fromRevision));

			final UserInfo userInfo = usersService.getUserMe(correlationId, user);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userInfo.getPreferredTimeZone());
		});
	}

	@RequestMapping(value = API + "/users/{username}", method = PUT)
	public ResponseEntity<UserInfo> updateUser(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username, //
			@RequestBody(required = true) final UserUpdate update //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo = usersService.updateUser(correlationId, user, username, update);

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userInfo);
		});
	}

	@RequestMapping(value = API + "/users/{username}", method = DELETE)
	public ResponseEntity<String> deleteUser(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			usersService.deleteUser(correlationId, user, username);

			return ResponseEntity.status(HttpStatus.NO_CONTENT) //
					.body(null);
		});
	}

	@RequestMapping(value = API + "/users/{username}/preferredTimeZone", method = GET)
	public ResponseEntity<String> getUserPreferredTimeZone(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final UserInfo userInfo;

			if (username.contentEquals(user.getUsername())) {

				userInfo = usersService.getUserMe(correlationId, user);

			} else {

				userInfo = usersService.getUser(correlationId, user, username);
			}

			final String timeZone = userInfo.getPreferredTimeZone();

			return ResponseEntity.status(timeZone != null //
					? HttpStatus.OK //
					: HttpStatus.NO_CONTENT) //
					.body(timeZone);
		});
	}

	@RequestMapping(value = API + "/users/{username}/preferredTimeZone", method = { POST, PUT })
	public ResponseEntity<String> setUserPreferredTimeZone(final HttpServletRequest request, //
			final HttpServletResponse response, //
			@PathVariable(name = "username", required = true) final String username, //
			@RequestBody(required = false) final PreferredTimeZone preferredTimeZone, //
			@RequestParam(name = "preferredTimeZone", required = false) final String preferredTimeZoneParam //
	) throws ServiceException {

		return wrapAuthenticated(request, response, authService, (correlationId, user) -> {

			final String timeZone;

			if (preferredTimeZoneParam != null) {

				timeZone = preferredTimeZoneParam;

			} else if (preferredTimeZone == null) {

				throw new IllegalArgumentException("timeZone should be set");

			} else {

				timeZone = preferredTimeZone.getTimeZone();
			}

			final UserInfo userInfo;

			if (username.contentEquals(user.getUsername())) {

				final int fromRevision = usersService.getUserMe(correlationId, user).getRevision();

				usersService.updateUserMe(correlationId, user, instantiate(UserUpdate.class) //
						.setPreferredTimeZone(timeZone) //
						.setFromRevision(fromRevision));

				userInfo = usersService.getUserMe(correlationId, user);

			} else {

				final int fromRevision = usersService.getUser(correlationId, user, username).getRevision();

				usersService.updateUser(correlationId, user, username, instantiate(UserUpdate.class) //
						.setPreferredTimeZone(timeZone) //
						.setFromRevision(fromRevision));

				userInfo = usersService.getUser(correlationId, user, username);
			}

			return ResponseEntity.status(HttpStatus.OK) //
					.body(userInfo.getPreferredTimeZone());
		});
	}
}
